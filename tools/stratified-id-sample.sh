#!/bin/bash
# run this to create a random sample of ids with ad text
# generally this is used to determine how many ids are associated with valid ads
# use tools/classify.pl to check each post and record 
db=$1
seed=2014
size=2000
echo "select i.id, count(distinct a.fid) adcount, max(words),min(words) from id2file f join ads a on f.fid=a.fid join ids i on i.id=f.id where i.include=1 and a.keep > 0 and (a.section='craigslist' or a.relevant in (1,3)) group by id order by rand($seed) limit $size" | $db --skip-column-names --table > $db-relevant.txt
