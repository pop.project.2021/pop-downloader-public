#!/usr/bin/env python
"""
simple takes a file as input and runs the perceptual hash algorithm
on the image path then prints out an update command.
assumes: iid,path
path should be valid where the script is run"""
import imagehash
from PIL import Image
import sys, re, os

def phash(line):
    g = re.match(r'(\d+)[,\t](.*)', line.strip())
    if g is None:
        g = re.match(r'([^,\t]+)[,\t](.*)', line.strip())
        if g is None:
            path = line.strip()
            where = "where path='{0}'".format(path.replace("'","''"))
        else:
            where = g.group(1)
            path = g.group(2)
    else:
        path = g.group(2)
        where = "where iid={0}".format(g.group(1))
    if not os.path.isfile(path):
        print(f"-- cannot find {path} {where}")
        return
    try:
        phash = imagehash.phash(Image.open(path))
        print(f"update images set phash='{phash}' {where}; -- {path} ")
    except Exception as e:
        print(f"-- error creating phash for {path}: {e}")

if __name__ == "__main__":
    if len(sys.argv) > 1:
        for filelist in sys.argv[1:]:
            with open(filelist) as listh:
                for line in listh:
                    phash(line)
    else:
        for line in sys.stdin:
            phash(line)

