Miscellaneous tools

Probably the most used tool was classify.pl which can be used on 
mariadb output to manually classify a database row. The 
row should be formatted in the mysql table format for this script
to work. Use the --table --skip-column-headers options when creating
an output file to classify.

The second most used was boxplot.py which simplified getting basic
stats out of mariadb. There are likely many ways to do this in other
languages. This tool is used by some of the id-vars scripts.

The simple-phash.py and simple-facetest.py were used to classify 
photos. When running simple-facetest.py its not a good idea to use
the phash value as some pictures can have the face obscured and 
still be matched with ones that don't.

