#!/usr/bin/perl
$pop = 169473;
$e = 0.05;
$z = 1.96;
$z2 = $z**2;
$e2 = $e**2;
$prop = 0.84;
$num = $z2 * $prop * (1-$prop)/$e2;

print "sample size calculator\n";
# from the js sample size calculator
#       ss = ((zVal *zVal) * 0.25) / ((conInt / 100) *(conInt / 100));
#       ss=ss/(1+(ss-1)/pop)
# where zVal is z below and conInt is e below
$ss = $num;
$ss /= (1+($ss-1)/$pop);
printf "sample size %d for pop %d for 5%% conf interval at 95%% certainty\n", 
    $ss, $pop;

print "wall street mojo\n";
# wall street mojo https://www.wallstreetmojo.com/sample-size-formula/
$ss = $pop * ($num) /($pop - 1 + $num);
printf "sample size %d for pop %d for 5%% conf interval at 95%% certainty\n", 
    $ss, $pop;

