#!/usr/bin/perl -n
# write a log line including first field in a set of sql output in mysql format
if (not %seen) {
    # note this will only check the first log file if its successful
    if (open LOG, "$ARGV.classify.log") {
        while (my $l = <LOG>) {
            if ($l =~ m/^(\S+|\S+ \S+|\S+ \S+ \S+) (?:VALID|invalid)/) {
                $seen{$1} = 1;
            }
        }
        close LOG;
    }
}

chomp;
(undef, $id, @fields) = split m/\s*\|\s*/, $_;

next unless $id ne "" and scalar(@fields) > 0;
$count++;
print STDERR "[$count] already saw $id\n" and next if $seen{$id};
$seen{$id} = 1;

print STDERR "-----------------------------------------------------\n\n";
print STDERR "[$count]\nID $id => ";
foreach $f (@fields) {
    $f =~ s/[^[:print:]]+//g;
    print STDERR "? $f\n";
}
print STDERR "\nvalid? [Yn] ";
$yn = <STDIN>;
chomp $yn;
if ($yn =~ m/^[nN]/) {
    $yn =~ s/^[nN]//;
    print "$id invalid $yn\n";
    open LOG, ">> $ARGV.classify.log" or die "failed to open $ARGV.classify.log to append: $!";
    print LOG "$id invalid $yn\n";
    close LOG;
} else {
    print "$id VALID $yn\n";
    open LOG, ">> $ARGV.classify.log" or die "failed to open $ARGV.classify.log to append: $!";
    print LOG "$id VALID $yn\n";
    close LOG;
}

