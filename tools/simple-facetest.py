#!/usr/bin/env python
"""
from machinelearningmastery.com
https://machinelearningmastery.com/how-to-perform-face-detection-with-classical-and-deep-learning-methods-in-python-with-keras/
"""
from matplotlib import pyplot
from mtcnn.mtcnn import MTCNN
import sys, os, re

seen = dict()
def facecheck(detector, line):
    """
    check an image with tensorflow and make an sql statement
    """
    global seen
    g = re.match(r'^(where .*)[,\t](.*)', line.strip())
    if g is None:
        g = re.match(r'^(\d+)[,\t](.*)', line.strip())
        if g is None:
            filename = line.strip()
            where = "where path='{0}'".format(filename.replace("'","''"))
        else:
            filename = g.group(2)
            where = "where iid={0}".format(g.group(1))
    else:
        filename = g.group(2)
        where = g.group(1)

    if filename in seen:
        return
    seen[filename] = True
    if not os.path.isfile(filename):
        print("-- skipping {0}".format(filename))
        return
    try:
        # load image from file
        pixels = pyplot.imread(filename)
        # detect faces in the image
        faces = detector.detect_faces(pixels)
        if len(faces) == 0:
            print(f"update images set faces='0' {where};")
        else:
            maxconf = 0.0
            for face in faces:
                if face['confidence'] > maxconf:
                    maxconf = face['confidence']
            print(f"update images set faces={maxconf} {where};")
    except:
        print("-- image error {0}".format(filename))

if __name__ == "__main__":
    # create the detector, using default weights
    detector = MTCNN()

    if len(sys.argv) > 1:
        for listname in sys.argv[1:]:
            with open(listname,"r") as imgh:
                for line in imgh:
                    facecheck(detector, line)
    else:
        for line in sys.stdin:
            facecheck(detector, line)

