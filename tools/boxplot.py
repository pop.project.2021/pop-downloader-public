#!/usr/bin/env python
"""
run a query and do min, avg, max, count 
then get the box plot
PPDBUSER and PPDBPW must be defined in the environment
"""
import argparse, os, logging

parser = argparse.ArgumentParser(description="take a field and table and output various frequency stats")
parser.add_argument("db",help="database name")
parser.add_argument("table",help="table to query")
parser.add_argument("field",help="field to query, for --count this can be a comma separated list")
parser.add_argument("--where",help="optional where conditions")
parser.add_argument("--count", action='store_true', help="do boxplot on freq of field")
parser.add_argument("--raw", action='store_true', help="do not print titles")
parser.add_argument("--pg", action='store_true', help="use local postgres db instead of mysql")
args = parser.parse_args()

if args.pg:
    from psycopg2 import connect, Error
else:
    from mysql.connector import connect, Error

try:
    if args.pg:
        connection = connect(database=args.db)
    else:
        connection = connect(database=args.db, user=os.environ["PPDBUSER"], password=os.environ["PPDBPW"], host="localhost")

    with connection.cursor() as cursor:
        if args.count:
            querystr = "select {0}, count(*) c from {1} {2} group by {0} order by c".format(
                        args.field, args.table, "" if args.where is None else args.where)
            cursor.execute(
                    "select count(*), min(c), avg(c), max(c), stddev(c) "
                "from ({0}) a".format(querystr))
            count, minc, avgc, maxc, stddev = cursor.fetchone()
        else:
            querystr = "select {0} c from {1} {2} order by {0}".format(
                        args.field, args.table, "" if args.where is None else args.where)
            cursor.execute("""select count(*), min({0}), avg({0}), max({0}), 
                            stddev({0}) from {1} {2}""".format(
                        args.field, args.table, "" if args.where is None else args.where))
            count, minc, avgc, maxc, stddev  = cursor.fetchone()
        print(querystr)
        
        for title, stat in {"N":count,"min":minc,"average":avgc,"max":maxc,"stddev":stddev}.items():
            if args.raw:
                print(stat)
            else:
                print(title,",",stat)

        fractions = {"1st quartile": int(count/4), "median": int(count/2), "3rd quartile": int(3*count/4), 
                "90th percentile": int(9*count/10), "95th percentile": int(95*count/100), 
                "99th percentile": int(99*count/100)}
        for title, frac in fractions.items():
            cursor.execute("{0} limit 1 offset {1}".format(querystr, frac))
            result = cursor.fetchone()
            if args.raw:
                print(result[-1])
            else:
                print(title,",",result[-1])

    connection.close()

except Error as e:
    logging.error("db error {0}".format(e))
