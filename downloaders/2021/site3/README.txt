Example downloader code for 2021 data set

See crontab.txt for how this code is used in 
the crontab. 

Because of the way downloadable data is represented
via the sitemap xml files, the download can take
quite a long time. 

The way this site works is advertisers pay to "bump" 
ads. Therefore ads can live for quite a long time.
Generally these can be processed daily to get a 
sense of who is advertising.

The lack of discrete time vs ad mapping getting
accurate counts of ad volumes per day could be
error prone. However, overall insight into how
many people advertise is probably accurate.

