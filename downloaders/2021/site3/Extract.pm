package Extract;
use strict;
use URL::Encode qw/url_encode url_decode/;
use Postedon;
our $posts;

sub scrape {
	my ($lines, $path) = @_;
	my ($name, $chatid, $chatname,$chatregion,$price,$avail,$views,$postedon,
        $title, $post, $postingid, $postingdate, $age, $ethnicity, $city, $phone, $email, %get);
	my @mt = localtime((stat("$ENV{HOME}/dl/$path"))[9]);
	my $postingdate = sprintf '%04d-%02d-%02d %02d:%02d:%02d', $mt[5]+1900, $mt[4]+1, @mt[3,2,1,0];
	if ($path =~ m#.*?(\d+)$#) {
		$postingid = $1;
	}
    my $notactive = 0;
	foreach (@$lines) {
        if (/Sorry, this ad is no longer active/) {
            $notactive = 1;
            last;
        }
        if ($get{ethnicity}) {
            $get{ethnicity} = 0;
            if (m#^\s*(.*\S)\s*</div>#) {
                $ethnicity = $1;
            }
        }
        if ($get{avail}) {
            $get{avail} = 0;
            if (m#^\s*(.*\S)\s*</div>#) {
                my @avail = split /\s*\&\s*/, $1;
                my %avail; @avail{@avail} = 1;
                $avail = join ",", (sort keys %avail);
            }
        }
        if (m#id="preview-name"[^>]*>([^<]*)#) {
            $name = $1;
        } elsif (m#id="preview-age"[^>]*>(\d+)#) {
            $age = $1;
        } elsif (m#id="preview-availability"#) {
            $get{avail} = 1;
            next;
        } elsif (m#id="preview-ethnicity"[^>]*>#) {
            $get{ethnicity} = 1;
            next;
        } elsif (m#<span itemprop="addressLocality">([^<]+?)</span>#) {
            $city = $1;
        } elsif (m#<meta itemprop="priceRange" content="(\d+)"/>#) {
            $price = $1;
        } elsif (m#<meta itemprop="telephone" content="([^"]*-\d\d\d\d)"/>#) {
            # these are now cut off like this -NNNN
            $phone = $1;
        } elsif (m#data-chat-id="(\d+)"#) { 
            $chatid = $1;
        } elsif (m#data-chat-name="([^"]+)"#) { 
            $chatname = $1;
        } elsif (m#data-region-id="(\d+)"#) {
            $chatregion = $1;
		} elsif (m#itemprop="telephone">.*?(\d[^<>]*)#) {
			$phone = $1;
		} elsif (m#"mailto:([^"]*)[^>]*itemprop="email"#) {
			$email = $1;
		} elsif (m#<h1[^>]*class="head__title"[^>]*>([^<]*)#) {
			$title = $1;
		} elsif (m#itemprop="description"#..m#</div>#) {
			s/<[^>]*(?:>|$)//g;
			s/&(?:#\d+|\w+);/ /g;
			$post .= $_ if m/\w/;
		} else {
            my $postedondata = Postedon::get($_, $postingdate);
            if (defined $postedondata) {
                $postedon = $postedondata->{postedon};
                $views = $postedondata->{views};
            }
        }
	}
	&trim($title);
	&trim($post);
    if ($notactive) {
        return { post => undef };
    }
	return { title=>$title, post=>$post, age=>$age, city=>$city, 
		 ethnicity=>$ethnicity, postingdate=>$postingdate, phone=>$phone, 
         email=>$email, postingid=>$postingid, name=>$name, availability=>$avail,
         chatid=>$chatid, chatname=>$chatname, chatregion=>$chatregion, price=>$price, 
         views=>$views, postedon=>$postedon, _op_=>'insert'};
}

sub images {
	my ($lines) = @_;
	my @images;
    return \@images;
    # this stuff is happening during the download now ...
	foreach (@$lines) {
		if (m#accountGallery#..m#<script>#) {
			if (m#<img([^>]*)#) {
				my $i = $1;
				my %img;
                # foreach my $t (qw/src alt title/) {
                # strange characters in the alt text may be causing this to fail, also no title afaik
				foreach my $t (qw/src/) {
					if ($i =~ m#$t="([^"]*)#) {
						$img{$t} = $1;
					}
				}
                $img{src} =~ s#/304x304/#/main/#;
				$img{src} =~ s#/ths/#/main/#;
                $img{src} = url_decode($img{src});
				push @images, \%img;
			}
		}
	}
	\@images;
}

sub trim {
	chomp $_[0];
	$_[0] =~ s/\r//gs;
	$_[0] =~ s/^\s*//s;
	$_[0] =~ s/\s*$//s;
}

1;
