#!/usr/bin/perl
## do a naive download of posts and images
## for some reason the php sitemap using xml isn't finding the images
use lib ( "$ENV{HOME}/dl/phones" );
use PPDB;
use PPIMG;
use Getopt::Std;
use URI::Escape;
my %opt;
getopts('rv',\%opt);
my $refresh = $opt{r} || 0;
my $verbose = $opt{v} ? 1: 0;

while (<>) {
	if (m#<loc>(.*)</loc>#) { 
		$url = $1;
		$seen = $keep = 0;
		next unless $url =~ m#-\d+$#;
		@ts = localtime;
		$ts = sprintf '%04d-%02d-%02d %02d:%02d:%02d', $ts[5]+1900,$ts[4]+1,@ts[3,2,1,0];
		print "not adult: $url\n" and next 
			unless $url =~ m#/adult/|/personals/|/jobs/salon-spa-fitness/|/services/therapeutic/#;
		($file = $url) =~ s#^https?://##;
		$keep = 1;
		if (my $fid = PPDB::seen("site3/$file") and -f $file) { 
			$seen = 1;
			print "already in db: $url\n";
		} else {
			print "$ts found: $url\n";
			system "/usr/bin/wget --timeout=1 -nc -nv -x '$url'";
		}
        PPDB::insfile("site3/$file") unless $seen;
	} elsif ($keep) {
		if (m#<lastmod>(.*)</lastmod>#) {
			next unless -f $file;
			$lastmod = $1;
			next unless $lastmod =~ m/^(\d\d\d\d-\d\d-\d\d)T(\d\d:\d\d:\d\d)[-\+]\d\d:\d\d$/;
			$lastmod = "$1 $2";
            @mtime = localtime((stat $file)[9]);
            $mtime = sprintf("%04d-%02d-%02d %02d:%02d:%02d", 
                $mtime[5]+1900, $mtime[4]+1, @mtime[3,2,1,0]);
			print "setting date to $lastmod for $file\n";
			system "/bin/touch -d '$lastmod' '$file'";

            if ($refresh and $seen and $mtime ne $lastmod) {
                print "$ts refresh (mtime $mtime vs $lastmod): $url\n";
                system "/usr/bin/wget --timeout=1 -nc -nv -x '$url'";
                PPDB::updatefile("site3/$file", {ignored => -1});
            }
            print "updating lastseen to $lastmod for site3/$file\n"; 
            PPDB::lastseen("site3/$file", $lastmod);
		} elsif (!$ENV{NOERSLISTIMG} and m#<image:loc>(.*)</image:loc>#) {
			my $imgurl = $1;
			(my $imgfile = $imgurl) =~ s#^https?://##;
            my $fid = PPDB::seen("site3/$file");
            die "site3/$file not found!" and next unless $fid > 0;
            my $iid = PPDB::imgseen("site3/$imgfile");
            if ($iid) {
                print "already have img for $fid: ($iid) $imgurl\n" if $iid and $verbose;
            } else {
                $ts = localtime;
                print "$ts image: $imgurl\n";
                system "/usr/bin/wget --timeout=1 --restrict-file-names=nocontrol -nc -nv -x '$imgurl'";
                my ($path, $sha1, $size) = PPIMG::get_path_hash("site3", "$ENV{HOME}/dl/site3", {src=>$imgurl});
                # $path, $imgurl, $sha1, $size
                PPIMG::insert($path,$imgurl,$sha1,$size);
                $iid = PPDB::imgseen("site3/".uri_unescape($imgfile));
                print "$ts fid $fid, new iid $iid for site3/$imgfile\n";
            }
            PPIMG::map($fid,$iid);
		}
	}
}

PPDB::fixsite3dates();

