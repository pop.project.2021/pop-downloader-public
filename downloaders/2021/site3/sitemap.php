#!/usr/bin/php
<?php
date_default_timezone_set('America/Vancouver');
$verbose = false;
if (!is_file($argv[1])) die("no valid file supplied");

$passthru = "";
if (sizeof($argv) > 2 and preg_match('#^-r$#', $argv[2])) {
    $passthru = "-r";
}

$mom_data = file_get_contents($argv[1]);
$mom = new SimpleXMLElement($mom_data);
foreach ($mom->sitemap as $sitemap) {
    read_map($sitemap->loc, $passthru);
}

function read_map($url, $passthru) {
    $file = preg_replace('#.*/#','',$url);
    print("file $file\n");
    if (false and file_exists($file)) {
        return;
    } 
    print("downloading");
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $data = curl_exec ($ch);
    curl_close ($ch);

    if ($fh = fopen($file,'w')) {
        print "saving map to $file\n";
        fwrite($fh, $data);
        fclose($fh);
    } 
    system("./sitemap.pl $passthru $file");
}
?>
