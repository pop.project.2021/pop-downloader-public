package Postedon;
our $verbose = 1;
our %months = (Jan=>1, Feb=>2, Mar=>3, Apr=>4, May=>5, Jun=>6, Jul=>7, Aug=>8, Sep=>9, Oct=>10, Nov=>11, Dec=>12);

sub get {
    my ($line, $today) = @_;
    my ($views, $postedon);

    if ($line =~ m/="Posted on (\d\d\d\d-\d\d-\d\d) \d\d:\d\d:\d\d".*Viewed (\d+) times/) {
        $postedon = $1;
        $views = $2;
    } elsif ($line =~ m/Posted on (Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\w*, (\d+) (\d\d\d\d),.*Viewed (\d+) times/) {
        $views = $4;
        if (defined $months{$1} and $3 >= 2021) {
            my $rpmon = $months{$1};
            my $rpday = $2;
            my $rpyear = $3;
            $postedon = sprintf("%04d-%02d-%02d", $rpyear, $rpmon, $rpday);
        }
    } elsif ($line =~ m/Posted (?:Today|Yesterday),.*Viewed (\d+) times/) {
        $postedon = $today;
        $views = $1;
    } elsif ($line =~ m#Posted(?: on|) (?:(?:Mon|Tue|Wed|Thu|Fri|Sat|Sun)\w*,\s*)(.*?(?:AM|PM)) .*?Viewed (\d+) time#) {
        $views = $2;
        my $rawpostedon = $1;
        print "raw $rawpostedon\n" if $verbose;
        if ($rawpostedon =~ /(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\w* (\d+)/) {
            if (defined $months{$1}) {
                my $rpmon = $months{$1};
                my $rpday = $2;
                my $rpyear = ($today =~ /^(\d\d\d\d)/)[0];
                $postedon = sprintf("%04d-%02d-%02d", $rpyear, $rpmon, $rpday);
                if ($postedon gt $today) {
                    $postedon = sprintf("%04d-%02d-%02d", $rpyear-1, $rpmon, $rpday);
                }
            }
        }
    } else {
        return;
    }
    print "$postedon, $views views\n" if $verbose;
    return {postedon=>$postedon, views=>$views};
}

1;

