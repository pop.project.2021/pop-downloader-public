Population project scripts

These tools download pages from various online advertising
services. As each service is different the tools differ for
each. This particular code base shows what was used for
the download and processing during 2021.

Because site3 changed between 2017 and 2021 additional 
fields were added for some items that did not exist in
the earlier version. As a result the db schema in phones
will likely be out of date (esp the ads table). However, 
this should give a fairly clear idea of how data was 
arranged in the database.

DEPENDENCIES

perl 5, php 5 or 7, bash/sh and wget.

These should now be considered non-working examples.
Mostly these tools should run as is except that the IP address
in the ip.php check in purevpn/pvpn-start.sh should be changed
to reflect the main external IP for the machine running these tools.

