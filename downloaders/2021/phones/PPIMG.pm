package PPIMG;
use Data::Dumper;
use Digest::SHA qw/sha1_hex/;
use URI::Encode qw/uri_decode/;
use PPDB;
use strict;
our $verbose = 0;

# this is used as part of the download process to create a new image record
sub insert {
    my ($path, $imgurl, $sha1, $size) = @_;
    my $dbh = PPDB::conn();
    return unless defined $dbh;
    my $ignored = defined $sha1 ? 0: 1;
    my $ins = $dbh->prepare(
        "insert ignore into images (image, path, sha1, size, ignored) ".
        "values (?,?,?,?,?)"
    );
    print "inserting $imgurl, $path, $sha1, $size, $ignored\n";
    $ins->execute($imgurl, $path, $sha1, $size, $ignored) or die $ins->errstr;
    my $iid = $dbh->selectrow_arrayref(
        "select LAST_INSERT_ID()"
    );
    return $iid->[0];
}

# also used during the download process
# this will have to have the sha1 and size filled in later - these aren't absolutely required
sub map {
    my ($fid, $iid) = @_;
    my $dbh = PPDB::conn();
    return unless defined $dbh;
    my $ins = $dbh->prepare("insert ignore into img2file (fid,iid) values (?,?)");
    print "mapping fid $fid with iid $iid\n" if $verbose;
    $ins->execute($fid,$iid) or die $ins->errstr;
}

# original code that does the insert/mapping during the analysis process
# this uses data from the post itself gleaned by the local Extract.pm
sub save {
	my ($path, $fid, $dbh, $post, $basedir, $pagesection) = @_;
    $pagesection = 0 unless defined $pagesection;
	$dbh = PPDB::conn() unless defined $dbh;
	return unless defined $dbh;

	my $canonical = PPDB::fullpath($basedir,$path);
	my $section = $path;
	if (-f $canonical) { 
		local $/; $/ = undef;
		open FILE, $canonical or warn "can't open $canonical: $!" and return;
		my $file = <FILE>;
		close FILE;
		$post = [split "\n", $file];
		$section = ($path=~m#^(\w+?)/#)[0];
		$canonical = "$ENV{HOME}/dl/$section";
	} elsif (-d $canonical) {
		$section = $path;
	} else {
		warn "don't know what $canonical is!";
		return;
	}
	eval "use lib qw( $canonical ); use Extract;";
	warn $@ and return if $@;
	my $imgdata = [];
	$imgdata = Extract::images($post);

	my $getiid = $dbh->prepare(
		"select iid from images where image=?"
	);

	my @newimages;
	if (scalar @$imgdata) {
		print Dumper($imgdata) if $verbose;
		foreach my $img (@$imgdata) {
			my ($path, $sha1, $size) = &get_path_hash($section, $canonical, $img);
			my $src = $dbh->quote($img->{src});
			my $qpath = $dbh->quote($path);
			unless (defined $sha1) {
				push @newimages, "($src,$qpath,null,null,1)";
			} else {
				push @newimages, "($src,$qpath,'$sha1',$size,0)";
			}
		}
		# doing this this way saves time as doing multiple inserts per file is costly
		if (scalar @newimages) {
			$dbh->do("insert ignore into images (image, path, sha1, size, ignored) values ".
					(join ",", @newimages)) or die $dbh->errstr;
			my @values;
			foreach my $img (@$imgdata) {
				$getiid->execute($img->{src}) or warn $getiid->errstr and next;
				if ($getiid->rows) {
					my $row = $getiid->fetch;
					push @values, "($fid, $pagesection, $row->[0])";
				}
				$getiid->finish;
			}
			if (scalar @values) {
				$dbh->do("insert ignore into img2file (fid, pagesection, iid) values ".(join ",", @values)) 
					or die $dbh->errstr;
			}
		}
	}
	$imgdata;
}

sub get_path_hash {
	my ($dir, $canonical, $img) = @_;
	(my $path = uri_decode($img->{src})) =~ s#^https?://##;
	my ($sha1,$size);
	if (-f "$canonical/$path") {
		if (open IMG, "$canonical/$path") {
			binmode IMG;
			local $/; $/ = undef;
			my $idata = <IMG>;
			die "missing data for $path!" if (length $idata) != ((stat("$canonical/$path"))[7]);
			close IMG;
			$sha1 = sha1_hex($idata);
			$size = length($idata);
		}
		print "hash '$sha1' ($size) for $path\n";
	}
	("$dir/$path", $sha1, $size);
}

1;
