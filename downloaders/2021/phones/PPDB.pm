package PPDB;
use DBI;
use strict;

our $db = $ENV{PPDB};
our $host = $ENV{DBHOST};
our $port = $ENV{DBPORT};
our $dsn = "DBI:mysql:database=$db;host=$host;port=$port";
our $dbuser = $ENV{PPDBUSER};
our $dbpw = $ENV{PPDBPW}; 

sub setdsn {
	my ($newdb, $newhost, $newport) = @_;
	$newdb = $db unless defined $newdb;
	$newhost = $host unless defined $newhost;
	$newport = $port unless defined $newport;
	my $newdsn = "DBI:mysql:database=$newdb;host=$newhost;port=$newport";
	$newdsn;
}

sub conn {
	my ($condsn,$user,$pw) = @_;
	$condsn = $dsn unless defined $condsn;
	$user = $dbuser unless defined $user;
	$pw = $dbpw unless defined $pw;
	my $dbh = DBI->connect($condsn, $user, $pw, {AutoCommit=>1,RaiseError=>1})
		or die "can't connect to $dsn as $user: $!";
	return $dbh;
}

sub fullpath {
	my ($basedir, $path) = @_;
	if (-d $basedir) {
		return "$basedir/$path";
	}
	return "$ENV{HOME}/dl/$path";
}

sub seen {
	my $dbh = PPDB::conn();
	my ($file) = @_;
    my $get = $dbh->prepare("select fid from files where path=?");
    $get->execute($file) or die "$file: ".$get->errstr;
    my $fid = 0;
    if ($get->rows) {
        my $row = $get->fetch();
        $fid = $row->[0];
    }
    $get->finish;
	return $fid;
}

sub imgseen {
	my $dbh = PPDB::conn();
	my ($file) = @_;
	$file = $dbh->quote($file);
	my $row = $dbh->selectrow_arrayref("select iid from images where path=$file");
	if ('ARRAY' eq ref $row) { return $row->[0]; }
	return 0;
}

sub lastseen {
	my $dbh = PPDB::conn();
	my ($file, $mtime) = @_;
	if (defined $mtime) {
		$mtime = $dbh->quote($mtime);
	} else {
		$mtime = 'now()';
	}
	$file = $dbh->quote($file);
	$dbh->do("update files set lastseen=$mtime where path=$file") 
		or die $dbh->errstr;
}

sub updatefile {
    my $dbh = PPDB::conn();
    my ($file, $fields) = @_;
    my (@fieldstrs);
    foreach my $field (keys %$fields) {
        push @fieldstrs, "$field=?";
    }
    my $fieldstr = join ",", @fieldstrs;
    my $query = "update files set $fieldstr where path=?";
    my $upd = $dbh->prepare($query);
    $upd->execute(values %$fields, $file) or die $upd->errstr;
}

sub insfile {
	my $dbh = PPDB::conn();
	my ($file, $mtime) = @_;
	if (defined $mtime) {
		$mtime = $dbh->quote($mtime);
	} else {
		$mtime = 'now()';
	}
	$file = $dbh->quote($file);
	$dbh->do("insert ignore into files (seen,path,lastseen) values ($mtime,$file,$mtime)") 
		or die $dbh->errstr;
}

sub fixsite3dates {
    my $dbh = PPDB::conn();
    $dbh->do("insert ignore into files_20220410 select * from files") and
    $dbh->do("update files set seen = lastseen where seen > lastseen") or warn $dbh->errstr;
}

1;
