package PPURI;
# gather info from ad urls

# list of location identifying url parts from PPAC
our %locales = (
	'site3' => {
		'calgary' => 'AB',
		'central-ontario' => 'ON',
		'edmonton' => 'AB',
		'greater-toronto' => 'ON',
		'hamilton-niagara' => 'ON',
		'interior' => 'BC',
		'metro-vancouver' => 'BC',
		'n_alberta' => 'AB',
		'new-brunswick' => 'NB',
		'newfoundland' => 'NF',
		'northern-bc' => 'BC',
		'northern-ontario' => 'ON',
		'nova-scotia' => 'NS',
		'nw_territories' => 'YT',
		'quebec' => 'QC',
		'regina' => 'SK',
		'saskatoon' => 'SK',
		'south-eastern-ontario' => 'ON',
		'south-western-ontario' => 'ON',
		'vancouver-island' => 'BC',
		'winnipeg' => 'MB',
		'yukon' => 'YT',
	},
);

our $catpat = qr/-(female-escorts|fetish-domination|erotic-massage|erotic-model|male-escorts|massage-by-men|travesti-transexual)-/;

# PROVINCE,LOCATION,CATEGORY_PARENT,CATEGORY_CHILD
sub parse {
	my ($path) = @_;
	my @parts = split '/', $path;
	my %parse;
	if ($parts[0] eq 'site3') {
		if ($parts[1] eq 'www.site3.tld') {
			%parse = (
				prov => $locales{$parts[0]}{$parts[3]},
				location => $parts[3],
				category_parent => 'adult',
				category_child => $parts[2],
			);
		} 
	} 
	\%parse;
}

1;

