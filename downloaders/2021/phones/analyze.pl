#!/usr/bin/perl
our ($dir, $canonical, $section, %opt);
BEGIN {
	use Getopt::Std;
	getopts('ui',\%opt);
    $ENV{PPDB} = 'pp2021';
    print "db $ENV{PPDB}";
	$opt{i} = ($opt{u} ? 0 : 1) unless $opt{i};
	$section = $dir = $ARGV[0];
	die "need a directory! (did you add a / at the end?)" unless $dir =~ /^\w+$/;
	$canonical = "$ENV{HOME}/dl/$dir";
	die "$canonical does not exist!" unless -d $canonical;
	chdir $canonical or die "can't change to $canonical!";
}

use lib ( $canonical, "$ENV{HOME}/dl/phones" );
use PPDB;
use PPID;
use PPIMG;
use Extract;
use Data::Dumper;
use strict;
 
my $dbh = PPDB::conn();
exit 1 unless defined $dbh;
my $get = $dbh->prepare(
	"select fid, path, basedir, ignored from files ".
    "where path like '$dir/%' ".
    "and ignored=-1 or (ignored=0 and fid not in (select fid from ads)) ".
	"order by fid"
);
my $getad = $dbh->prepare(
	"select fid,postingid from ads where postingid=?"
);
my $ins = $dbh->prepare(
	"replace into ads (".
    "fid,section,postingdate,title,postingid,post,age,ethnicity,city,phone,email,".
    "name,chatid,chatname,chatregion,availability,price,views,postedon) ".
	"values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
);
my $insstub = $dbh->prepare(
	"insert into ads (fid,section,postingid,post) ".
	"values (?,?,?,?)"
);
my $ignore = $dbh->prepare(
	"update files set ignored=? where fid=?"
);

$get->execute or die $get->errstr;
while (my $row = $get->fetch) {
	my ($fid, $path, $basedir, $ignored) = @$row;
	my $fullpath = PPDB::fullpath($basedir,$path);
    print "checking $ENV{PPDB} $fullpath\n";
	if (open POST, $fullpath) {
		print "fid $ENV{PPDB} $fid extracting from $path\n";
		my @lines;
		while (<POST>) {
			push @lines, $_;
		}
		close POST;
		my $ad = Extract::scrape(\@lines, $path);
		if ($ad->{post} =~ /\w/) {
			$ad->{fid} = $fid;
			$ad->{section} = $section;
			$ad->{path} = $path;
			print Dumper($ad);
			if ($ad->{_op_} eq 'update') {
				warn "need a postingid!" and next unless $ad->{postingid} =~ /^\d+$/;
				$getad->execute($ad->{postingid}) or warn $getad->errstr and next;
				unless ($getad->rows) {
					$insstub->execute(@$ad{qw/fid section postingid post/}) 
						or die $insstub->errstr;
				}
				foreach (qw/postingdate title post age ethnicity city phone email/) {
					next unless length $ad->{$_};
					my $val = $dbh->quote($ad->{$_});
					$dbh->do("update ads set $_=$val where postingid=$ad->{postingid}")
						or warn $dbh->errstr and last;
				}
				# indicate that this fid has been seen
				if ($opt{i}) { $ignore->execute(3,$fid) or warn $ignore->errstr; }
			} elsif ($ad->{_op_} eq 'ignore') {
				next;
			} else {
				$ins->execute(@$ad{qw/
						fid section postingdate title postingid post age ethnicity 
                        city phone email name chatid chatname chatregion availability price
                        views postedon
					/}) 
					or die $ins->errstr;
			}
			PPID::extract($ad,$dbh,1);
            if ($ignored == -1) {
                print "resetting ignore\n"; 
                $ignore->execute(0,$fid) or warn $ignore->errstr;
            }
		} else {
			print "no data\n";
			if ($opt{i}) { $ignore->execute(2,$fid) or warn $ignore->errstr; }
		}
		# find images in posts
		PPIMG::save($section, $fid, $dbh, \@lines, $basedir);
 	} else {
		warn "can't open $path: $!";
		if ($opt{i}) { $ignore->execute(1,$fid) or warn $ignore->errstr; }
		next;
	}
}
$get->finish;

