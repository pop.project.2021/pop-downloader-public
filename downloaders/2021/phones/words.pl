#!/usr/bin/perl
# find words in posts - remove any punctuation but not the unicode
use HTML::Entities;
use Encode qw/decode/;
use PPDB;
use Getopt::Std;
use Data::Dumper;
use feature 'unicode_strings';
use strict;
my %opt;
# l - limit, p - print commands only, q - quiet, v - verbose
getopts('l:pqv',\%opt);
my $dbh = PPDB::conn();
my $limit = $opt{l} || 10000;
my $limitstmt = (($limit eq 'all') ? "" : "limit $limit");
print "using batch limit of $limit\n" unless $opt{q};
my $get = $dbh->prepare(
	"select fid,title,post,locale,lang from ads ".
    # "where words is null ".
	"order by fid desc ".
	$limitstmt
);
# my $updw = $dbh->prepare("update ads set words3=? where fid=?");
my $updw = $dbh->prepare("update ads set words=?, strippedtitle=?, strippedpost=? where fid=?");
$get->execute or die $get->errstr;
exit(1) unless $get->rows;
my %wordcounts = ();
my %bigrams = ();
my %trigrams = ();
while (my $row = $get->fetch) {
	my ($fid,$title,$post,$locale,$lang) = @$row;
	$title =~ s/[^\x00-\x7f]+/ /g;
	$title =~ s/(?:\&#?\w+;)+/ /g;
	print "$fid $lang $title\n" unless $opt{q};
	$lang = s/.*?([a-z]{2}) .*/$1/;

	# squash words into two langs
	$lang = 'fr' if $lang !~ /\w\w/ and $locale eq 'QC';
	$lang = 'en' unless $lang =~ /\w\w/;

    my $strippedtitle = &stripchars($title);
    my $strippedpost = &stripchars($post);
	my $all = $strippedtitle." ".$strippedpost;
	my @words = split " ", $all;
	my @ngram = ();
	my @cleaned = ();
    my %seen = ();
	foreach my $word (@words) {
		next unless $word =~ /\w/;
		print "found $word\n" if $opt{v};
		# as this is for the individual post keep the numbers
		push @cleaned, $word;
        # shrink overly long words
        $word = substr($word, 0, 128);
		# squash numbers but keep length of number for the general counts
		if ($word =~ /\d+/) {
			$word =~ s/(\d+)/sprintf "_%s_", 'N' x length($1)/eg;
		}
        $wordcounts{$word}{$lang}{docs}++ unless $seen{$word};
        $seen{$word} = 1;
        $wordcounts{$word}{$lang}{num}++;
        push @ngram, $word;
        print "ngram @ngram\n" if $opt{v};
        my $ns = scalar @ngram;
        if ($ns == 2) {
            my $bigram = join " ", @ngram;
            $bigrams{$bigram}{$lang}{docs}++ unless $seen{$bigram};
            $seen{$bigram} = 1;
            $bigrams{$bigram}{$lang}{num}++;
        } elsif ($ns == 3) {
            my $trigram = join " ", @ngram;
            $trigrams{$trigram}{$lang}{docs}++ unless $seen{$trigram};
            $seen{$trigram} = 1;
            $trigrams{$trigram}{$lang}{num}++;
            my $bigram = join " ", @ngram[1,2];
            $bigrams{$bigram}{$lang}{docs}++ unless $seen{$bigram};
            $seen{$bigram} = 1;
            $bigrams{$bigram}{$lang}{num}++;
            shift @ngram;
        } elsif ($ns > 3) {
            die "ngram @ngram got too big!";
        }
	}
    $updw->execute("@cleaned",$strippedtitle,$strippedpost,$fid) or die $updw->errstr;
}
$get->finish;
&docounts('words',\%wordcounts);
&docounts('bigrams',\%bigrams);
&docounts('trigrams',\%trigrams);

# print "building indexes\n";
# $dbh->do("create index words_word_idx on words (word)") or warn $dbh->errstr;
# $dbh->do("create index bigrams_word_idx on bigrams (word)") or warn $dbh->errstr;
# $dbh->do("create index trigrams_word_idx on trigrams (word)") or warn $dbh->errstr;

sub stripchars {
    my ($all) = @_;
	$all =~ s/\s+/ /g;
	$all =~ s/<[^>]*>/ /sg;
	$all = decode_entities($all);
	$all =~ s/[\+\=\`\~!@#\$\%^\&\*\(\)\[\]\{\}\|\\\:\;\"\,\.\/<>\?_\-]+/ /g;
    $all =~ s/[^\x00-\x7f]+/ /g;
	$all =~ s/ +/ /g;
	$all =~ s/^'+|'+$//;
	$all =~ s/'+/'/g;
	$all = lc($all);
    return $all;
}

sub docounts {
	my ($table, $counts) = @_;
	print "adding ",(scalar keys %$counts)," items to $table\n" unless $opt{q};
=filter out
	my $getnums = $dbh->prepare("select word,lang,num,name from $table");
	$getnums->execute or die $getnums->errstr;
	my $isname = {};
	while (my $row = $getnums->fetch) {
		my ($word,$lang,$num,$name) = @$row;
		if (defined $counts->{$word}{$lang}) {
			$counts->{$word}{$lang} += $num;
			$isname->{$word}{$lang} = $name;
		}
	}
	$getnums->finish;
=cut
	my (@keys);
	my $getnumskey = $dbh->prepare("select word,lang,num,name from $table where word=? and lang=?");
	foreach my $key (keys %$counts) {
		foreach my $lang (keys %{$counts->{$key}}) {
			$getnumskey->execute($key, $lang) or die "$table: $getnumskey->errstr";
			my $count = $counts->{$key}{$lang}{num};
            my $docs = $counts->{$key}{$lang}{docs};
			my $name = 0;
			if ($getnumskey->rows) {
				my $row = $getnumskey->fetchrow_hashref;
				$count += $row->{num};
				$name = $row->{name};
			}
			next unless $lang =~ /en|fr/;
			push @keys, [$key,$lang,$count,$docs,$name];
		}
		if (scalar @keys >= 100) {
			&dumpvals($table,\@keys);
			@keys = ();
		}
	}
	&dumpvals($table,\@keys);
}

sub dumpvals {
	my ($table, $keys) = @_;
    my (@vals);
    foreach my $key (@$keys) {
        my ($first,$second,$last) = map { $dbh->quote(substr($_,0,128)) } (split " ", $key->[0]);
        $first = 'null' unless defined $first;
        $second = 'null' unless defined $second;
        $last = 'null' unless defined $last;
        push @vals, "(".$dbh->quote(substr($key->[0],0,128)).",".
                $dbh->quote($key->[1]).",$key->[2],$key->[3],$key->[4],$first,$second,$last)"; 
    }
    my $vals = join ",", @vals;
	my $stmt = "replace into $table (word,lang,num,docs,name,first,second,last) values $vals";
	if ($opt{p}) {
		print "$stmt;\n";
	} else {
		$dbh->do($stmt) or die $dbh->errstr;
	}
}

