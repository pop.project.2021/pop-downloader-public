package PPID;
# extract identifiers from posts
use PPDB;
use PPAC;
use PPExtract;
use Data::Dumper;
use strict;

our $verbose = 0;
our $threshold = 0;

sub extract {
    my ($ad, $dbh, $passt, $keep, $nolinks) = @_;
    $threshold = $passt if $passt =~ /^\d$/;
    if (!defined $dbh) {
        $dbh = PPDB::conn();
    }
    return unless defined $dbh;
    my $chk = $dbh->prepare(
        "update ads set checked=1 where fid=? and pagesection=?"
    );
    # do a basic check for phone/email info
    my $content = "$ad->{title}\n\n$ad->{post}\n\n$ad->{phone}\n\n$ad->{email}";
    $content =~ s/&(?:#\d+|\w+);//g;
    $content =~ s/<[^>]*>//g;
    # remove dates
    $content =~ s/\d+ \w+ 201\d \d\d:\d\d:\d\d//g;
    $content =~ s/201\d\D\d\d\D\d\d[T ]\d\d:\d\d:\d\d//g;
    unless ($nolinks) {
        # expose uris that may have been inadvertently removed
        my $uripat = qr{<a[^>]*href=["']?(https?://[^"']*)};
        while ($ad->{post} =~ /$uripat/g) {
            $content .= "\n$1";
        }
    } else {
        $content =~ s#https?://\S*##g;
    }
    my @lines = split "\n", $content;
    my $count = &scan($ad, $dbh, \@lines, 1,$keep);

    # remove extraneous characters between digits
    unless ($count) {
        # print $content;
        my @test;
        foreach my $line (@lines) {
            my $test = $line;
            $test =~ s/(\d)\D+(\d)/$1$2/gm;
            $test =~ s/(\d)\D+(\d)/$1$2/gm;
            push @test, { test=>$test, line=>$line };
        }
        # print "count $count\n$content\n";
        # print join "\n", @lines, "\n";
        $count = &scan($ad, $dbh, \@test, 2, $keep);
        # print "count $count\n",(join "\n", map { $_{test} } @test),"\n";
        unless ($count) {
            foreach my $l (split "\n", $content) {
                $count += &squeeze($l, $ad, $dbh,$keep);
            }
            &squeeze($content, $ad, $dbh,$keep) unless $count;
        }
    }
    $chk->execute(@{$ad}{qw/fid pagesection/}) or die $chk->errstr;
}

sub squeeze {
    my ($content,$ad,$dbh,$keep) = @_;
    my $ex = PPExtract->new('content'=>$content);
    $ex->process;
    my $prov = PPAC::get_prov($ad);
    my ($count, @pass3phs);
    if (defined $prov) {
        foreach my $ph (sort keys %{$ex->{phones}}) {
            push @pass3phs,$ph if $PPAC::canada{substr($ph,0,3)} =~ /^(?:$prov)$/;
        }
        $count = &scan($ad, $dbh, \@pass3phs, 3);
    }
    print $content,$ex->tostring if $verbose;
    $count;
}

sub scan {
    my ($ad, $dbh, $lines, $pass, $noclr) = @_;
    # for ids found in posts
    my $insids = $dbh->prepare(
        "insert ignore into ids (id,pass,type,confirmed) values (?,?,?,1)"
    );
    my $insid2file = $dbh->prepare(
        "insert ignore into id2file (id,fid,pagesection,pass,type,line,capture,confirmed) values (?,?,?,?,?,?,?,1)"
    );
    my $clr = $dbh->prepare(
        "delete from id2file where fid=? and (pass is null or pass=?)"
    );

    # this is not included in the count
    if ($ad->{chatname} and not $ad->{chatnameadded}) { 
        print "adding chatname $ad->{chatname} for $ad->{fid}.$ad->{pagesection}\n";
        $insids->execute($ad->{chatname}, 0, 'chatname') or die $insids->errstr;
        $insid2file->execute($ad->{chatname}, $ad->{fid}, $ad->{pagesection}, 0, 'chatname', undef, undef)
            or die $insid2file->errstr;
        $ad->{chatnameadded} = 1;
    }

    # adapted from the patterns from findid.pl
    my %pats = (
        phone => qr/(($PPAC::acpat)\D{0,10}([2-9]\D?\d\D?\d)\D{0,10}(\d\D?\d\D?\d\D?\d))/,
        email => qr/(((?:\w+\.)*\w+)\@((?:[\-\w]+\.)*\w+)\.([a-z]{2,4})(?:$|\W))/,
        uri => qr{((https?://)([\w\-\.]*)([/\w\-\&\=\+\%\;]*))},
    );

    my (%keep, %seen);
    foreach my $pattype (keys %pats) {
        my $pat = $pats{$pattype};
        foreach my $l (@$lines) {
            my ($line, $original);
            if (ref $l eq 'HASH') {
                $line = $l->{test};
                $original = $l->{line};
            } else {
                $original = $line = $l;
            }
            next unless $line =~ /\w/;
            while ($line =~ /$pat/g) {
                my $capture = $1;
                my $id = "$2 $3 $4";
                if ($pattype eq 'uri') {
                    $id .= "/" unless $4;
                    next if $id =~ / (?:www\.|)(?:youtube|youtu\.be)/;
                }
                my $captpat = $id;
                if ($pattype eq 'phone') { 
                    my $repos = pos($line) - (length($capture) - 1);
                    pos($line) = $repos;
                    $id =~ s/\D//g; 
                    $id =~ s/(...)(...)(....)/$1 $2 $3/; 
                    next if $1 eq $2;
                    $captpat = $id;
                    $captpat =~ s/ //g;
                    my @pat = split //, $captpat;
                    $captpat = join "\\D*", @pat;
                    $captpat = qr/\S*($captpat)\S*/;
                } else {
                    $captpat = quotemeta($captpat);
                    $captpat =~ s/ /\.\*/g;
                    $captpat = qr{$captpat} 
                        or warn "error with pattern $captpat: $!";
                }
                next if $seen{$id};
                $seen{$id} = 1;
                next if $threshold > 0 and $pass != $threshold;
                if ($original =~ /($captpat)/) { $capture = $1; }
                next unless &valid($id, $capture, $ad);
                $keep{$id} = { line=>$original, capture=>$capture, type=>$pattype };
            }
        }
    }
    $clr->execute($ad->{fid},$pass) unless $noclr;
    foreach my $id (keys %keep) {
        print "pass $pass: inserting $id for $ad->{fid}\n";
        $insids->execute($id, $pass, $keep{$id}{type}) or die $insids->errstr;
        $insid2file->execute($id,@{$ad}{qw/fid pagesection/},$pass,@{$keep{$id}}{qw/type line capture/}) 
            or die $insid2file->errstr;
    }
    scalar keys %keep;
}

sub valid {
    my ($id, $capture, $ad) = @_;
    my $captpat = $id;
    $captpat =~ s/ //g;
    warn "not checking $id, $capture" and return 2
        unless $captpat =~ /^\d{10}$/;
    my $valid = 1;
    if (&is_url($capture)) {
        return 0;
    } elsif ($capture =~ /Lic\s*#/i) {
        return 0;
    } else {
        my @pat = split //, $captpat;
        $captpat = join "(\\D*)", @pat;
        $captpat = qr/($captpat)/;
        my @noise = $capture =~ /$captpat/;
        if (scalar @noise) {
            my $noise = join "", @noise;
            # these characters show up in things like 120$/h etc
            $noise =~ s#[/\\\$]##g;
            my $all = length($noise);
            return 1 if $all == 0;
            # this simple rule seems to be valid almost all of the time
            my @words = grep /^[a-zA-Z]+\S*$/, split / /, join " ", @noise;
            return 0 if scalar(@words) > 0;
            $noise =~ s/[a-zA-Z]//g;
            my $nonalpha = length($noise);
            my $alpha = $all - $nonalpha;
            # may need to tweak this
            return 0 if $alpha/$nonalpha > 0.5;
        }
        # check if we are in the middle of a phone #
        # if so check the area code
        my $prov = PPAC::get_prov($ad) if defined $ad;
        if (defined $prov and $capture =~ /[02-9]$captpat/) {
            print "$PPAC::canada{substr($id,0,3)} eq $prov?\n";
            return 0 unless $PPAC::canada{substr($id,0,3)} eq $prov;
        }
    }
    $valid;
}

sub is_url {
    my ($capture) = @_;
    return $capture =~ m#^https?://\S*$#;
}

1;

