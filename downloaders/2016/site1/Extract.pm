package Extract;
# this is only for site1
use JSON;
use strict;
our $posts;

sub scrape {
	my ($lines, $path) = @_;
	my $title;
	my $post;
	my $postingid;
	my $postingdate;
	my $age;
	if ($path =~ m#.*/(\d+)$#) {
		$postingid = $1;
		$post = join '', @$lines;
		&trim($post);
		$post =~ s/<br>//g;
		return {postingid=>$postingid, post=>$post, _op_=> 'insert'};
	}
	foreach (@$lines) {
		if (m#<title>(.*)</title>#) {
			$title = $1;
		} elsif (m#(?:affiché|Posted): <time datetime="(\S*)"#) {
			$postingdate = $1; 
		} elsif (m#updated: <time datetime="(\S*)"#) {
			$postingdate = $1; 
		} elsif (m#<section id="postingbody">#..m#</section>#) {
			$_ =~ s/<br>//g;
			unless (m#^\s*$#) {
				$post .= $_;
			}
		} elsif (m#age: (\d+)#) {
			$age = $1;
		} elsif (m#(?:IDannonce|post id): (\d+)#) {
			$postingid = $1;
		}
	}
	&trim($title);
	&dateconv($postingdate);
	&trim($post);
	return { title=>$title, postingdate=>$postingdate, post=>$post, age=>$age, 
		 postingid=>$postingid , _op_=>'insert'};
}

sub images {
	my ($lines) = @_;
	my ($imgblock, @images);
	foreach (@$lines) {
		if (/imgList = (.*);/) { 
			eval { $imgblock = decode_json($1); };
			warn $@ if $@;
			last;
		}
	}
	foreach my $img (@$imgblock) {
		$img->{src} = $img->{url};
		push @images, $img;
	}
	\@images;
}

sub dateconv {
	&trim($_[0]);
	use Date::Parse;
	my @t = localtime(str2time($_[0])); 
	$_[0] = sprintf "%04d-%02d-%02d %02d:%02d:%02d", 
			$t[5]+1900, $t[4]+1, $t[3], $t[2], $t[1], $t[0];
}

sub trim {
	chomp $_[0];
	$_[0] =~ s/^\s*//;
	$_[0] =~ s/\s*$//;
}

1;
