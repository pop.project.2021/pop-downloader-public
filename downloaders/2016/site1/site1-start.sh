#!/bin/sh
# site1, now even more annoying ...
cd $HOME/dl/site1
echo checking $1 count "$2" retry "$3"

# from http://www.linuxjournal.com/content/sigalrm-timers-and-stdin-analysis
function allow_time
{
	(
		echo will timeout after $1 seconds
		sleep $1
		kill -ALRM $$
	)&
}

function timeout_handler 
{
	echo taking too long
	exit 1
}

trap timeout_handler SIGALRM
allow_time 2400

res=0
if /usr/bin/pgrep openvpn >/dev/null 2>&1
then
	echo openvpn running IP is `../purevpn/ip.sh`
else
	../purevpn/start.sh
	res=$?
	count=1
	while [ $res -ne 0 ]
	do
		../purevpn/start.sh
		res=$?
		count=`perl -e 'sleep 30; print $ARGV[0]++;' $count`
		if [ $count -ge 10 ]; then exit 1 ; fi
	done
fi
if [ $res -eq 0 ]
then
	finish=`perl -e 'print (time + 180)'`
	rm cookies
	touch cookies
	./site1-downloader.pl -c "$2" -r "$3" $1
	res=$?
	# if the downloader fails make sure we return that code so we can retry
	if [ $res -ne 0 ]; then exit $res; fi
else
	echo openvpn failed 
fi
