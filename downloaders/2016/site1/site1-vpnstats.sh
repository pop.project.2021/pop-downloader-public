#!/bin/sh
cd $HOME/dl/site1
gzip -dc site1.out.*.gz | \
perl -ne 'print' site1.out | \
perl -ne '$conn = $1 and $freq{$conn}++ if /openvpn connecting to (.*)/; 
	$codes{$conn}{$1}++ if /awaiting response.*?(\d\d\d)/; 
	END { 
		foreach $f (keys %codes) { $rates{$f} = $codes{$f}{200}/($codes{$f}{403}||1); } 
		foreach (sort { $rates{$b} <=> $rates{$a} } keys %rates) { 
			printf "replace into vpn (success,fail,freq,server) values (%d,%d,%d,\"%s\");\n", 
				@{$codes{$_}}{qw/200 403/}, $freq{$_}, $_; 
		} 
	}'
