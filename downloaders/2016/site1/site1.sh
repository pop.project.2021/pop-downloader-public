#!/bin/sh
cd $HOME/dl/site1
rm *-start

# from http://www.linuxjournal.com/content/sigalrm-timers-and-stdin-analysis
function allow_time
{
        (
                echo $0 will timeout after $1 seconds
                sleep $1
                kill -ALRM $$
        )&
}

function timeout_handler
{
        echo $0 taking too long
        exit 1
}

trap timeout_handler SIGALRM
# fail after 2.5 hours
# allow_time 9000
# fail after 4 hours
allow_time 14400

function wait_it_out
{
	finish=`perl -e 'print time+$ARGV[0]' $1`
	# for single devices hma expects an uptime of 2.5 min
	while [ `perl -e 'print time'` -lt $finish ]; do sleep 1; done 
}

function stop_sequence 
{
	wait_it_out 180
	../purevpn/pvpn-stop.sh
	sleep 30
}

# with the approx 3 min minimum delay for each this will take 168 min or 2 hours 48 min
# and longer for longer delays of course 
list=`./shuffle.pl toronto vancouver montreal abbotsford barrie belleville brantford calgary cariboo chatham comoxvalley cornwall cranbrook edmonton ftmcmurray guelph halifax hamilton hat kamloops kelowna kingston kitchener lethbridge londonon nanaimo newfoundland niagara ottawa owensound pei peterborough princegeorge quebec reddeer regina saguenay sarnia saskatoon sherbrooke skeena soo sudbury sunshine thunderbay troisrivieres victoria whistler whitehorse windsor winnipeg yellowknife`
echo $list
for l in $list
do
	count=`perl -e 'print (++$ARGV[0])' $count`
	retry=1
	echo $l $count out of 53 attempt $retry
	./site1-start.sh $l "$count" "$retry"
	while [ $? -ne 0 ] && [ $retry -lt 10 ]
	do
		retry=`perl -e 'print $ARGV[0]+1;' $retry`
		echo $l $count out of 53 attempt $retry
		stop_sequence
		./site1-start.sh $l "$count" "$retry"
	done
done
stop_sequence
$HOME/dl/phones/analyze.pl site1
