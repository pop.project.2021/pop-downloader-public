#!/usr/bin/perl
use lib ( "$ENV{HOME}/dl/phones" );
use PPDB;
use JSON;
use Getopt::Std;
my %opt;
getopts('c:r:ft',\%opt);
my $testing = $opt{t} || 0;
my @locations = qw{ 
	abbotsford barrie belleville brantford calgary cariboo 
	chatham comoxvalley cornwall cranbrook edmonton ftmcmurray 
	guelph halifax hamilton hat kamloops kelowna kingston 
	kitchener lethbridge londonon montreal nanaimo newfoundland 
	niagara ottawa owensound peace pei peterborough princegeorge 
	quebec reddeer regina saguenay sarnia saskatoon sherbrooke 
	skeena soo sudbury sunshine thunderbay toronto troisrivieres 
	vancouver victoria whistler whitehorse windsor winnipeg 
	yellowknife 
}; 
my @categories = qw/
    ths
/;

# for wget - impossible to switch pages w/o this
my $useragent = "--user-agent='Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:33.0) Gecko/20100101 Firefox/33.0'";
my $cookieopts = "--keep-session-cookies --load-cookies=cookies --save-cookies=cookies $useragent -T 60 -t 5";

my $ml = "/home/pop/mod_ml";
my $mluri = 'http://0.0.0.0:80';

# foreach my $loc (@locations) {
foreach my $loc (@ARGV) {
	my $dir = "$loc.site1.ca";
	my $pre = ($loc=~/^(...)/)[0];
	my $url = "http://$dir";
	my $retrieved = 0;
	my $emptypages = 0;
	my $pages = 5;
	foreach my $cat (@categories) {
		my $startpage = 0;
		my $startpagefile = "$loc-$cat-start";
		if (-f $startpagefile) {
			if (open START, $startpagefile) {
				$startpage = <START>;
				close START;
			}
		}
		sleep 2;
		$emptypages = 0;
		foreach my $pagen ($startpage..$pages) {
			if (open START, ">$startpagefile") {
				print START $pagen;
				close START;
			}
			my $cpage = "$loc-$cat-$pagen.html";
			unless ($testing) {
				my $out = `/usr/bin/wget $cookieopts -O '$cpage' '$url/search/$cat' 2>&1`;
				print $out;
				die "can't download" unless $out =~ /200 OK/;
			}
			open LOC, "$cpage" or warn "can't open $cpage: $!" and next;
			$retrieved = 0;
			while (<LOC>) {
				if (m#Zero LOCAL results found\. Here are some from NEARBY areas\.#i) {
					last;
				}
				while (m#href="([^"]*/$cat/(\d+)\.html)"#g) {
					my ($u, $n) = ($1, $2);
					next unless $u =~ m#^/\w+/\w+#;
					$u = "$url$u";
					(my $fb = $u) =~ s#(https?://[^/]*?)/(.*?)\.html#$1/fb/$2#;
					if ($u =~ m#^/#) { 
						$u = "$url$u";
						$fb = "$url/fb/$pre/$cat/$n";
					} 
					my $f = ($u=~m#^https?://(.*)#)[0];
					# NOTE: cannot use -O with nonexistent directories
					# get the main page
					my $fseen = PPDB::seen("site1/$f");
					unless (-f $f or $fseen) {
						sleep 2 + int(rand()*5);
						my $out = `/usr/bin/wget -p -H -nc -x $cookieopts '$u' 2>&1`;
						print $out;
						$retrieved++ if $out =~ /200 OK/;
						&report($loc,$cat,$startpage,$retrieved);
						die "can't download!" if $out =~ /ERROR 403/;
						PPDB::insfile("site1/$f");
					}
					# the syntax of the contact info url is not that predictable
					# so see if we can't find it to get it right
					# 2015-06-17: these are blocked with a captcha now
					if ($opt{f}) {
						my $fbf = ($fb=~m#^https?://(.*)#)[0]; # the default
						unless (-f $fbf or PPDB::seen("site1/$fbf")) {
							sleep int(rand()*5);
							my $out = `/usr/bin/wget -nc -x $cookieopts '$fb' 2>&1`;
							print $out;
							$retrieved++ if $out =~ /200 OK/;
							&report($loc,$cat,$startpage,$retrieved);
							die "can't download!" if $out =~ /ERROR 403/;
							PPDB::insfile("site1/$fbf");
						}
					}
					&images($f) unless $fseen;
				}
			}
			close LOC;
			unlink $startpagefile;
			# check one after the last page in case ads were pushed past last seen page during delay
			$emptypages++ if $retrieved == 0;
			last if $emptypages == 2;
		}
	}
}

sub images {
	my ($f) = @_;
	# print "checking $f for images\n";
	open HTML, $f or warn "can't open $f: $!" and return;
	my $images;
	# page images are now in a json array called imgList
	while (my $line = <HTML>) {
		if ($line =~ /imgList = (.*);/) { 
			$images = decode_json($1); 
			last;
		}
	}
	my $retrieved = 0;
	foreach my $i (@$images) {
		my $if = ($i->{url} =~ m#^https?://(.*)#)[0];
		next if -f $if;
		sleep int(rand()*2);
		my $out = `/usr/bin/wget -nc -x $cookieopts '$i->{url}' 2>&1`;
		print $out;
		die "can't download!" if $out =~ /ERROR 403/;
		$retrieved++;
	}
	$retrieved;
}

sub report {
	my ($loc,$cat,$startpage,$retrieved) = @_;
	print "$loc $cat (count $opt{c}, retry $opt{r}) page $startpage download $retrieved\n";
}
__END__
