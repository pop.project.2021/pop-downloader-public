#!/bin/sh
echo number of complete download runs per day
grep -B2 '53 out of 53' site1.out | grep '^201[0-9]' | perl -pe 's/(201\d-\d\d-\d\d).*/$1/' | perl -ne 'chomp; $c{$_}++; END { foreach my $d (sort keys %c) { print "$d $c{$d}\n"; }}'
echo http results per day
perl -ne '$d=$1 if /^--(201\d-\d\d-\d\d)/; $res{$d}{$1.$2}++ if /(?:(200) OK|(403) Forbidden)/i; END { foreach (sort keys %res) { $perc = sprintf "%.2f", 100*$res{$_}{200}/($res{$_}{200}+$res{$_}{403}); print "$_ 200=$res{$_}{200} 403=$res{$_}{403} $perc\% success\n" } }' site1.out
