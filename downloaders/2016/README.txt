Population project scripts

These tools download pages from various online advertising
services. As each service is different the tools differ for
each. 

DEPENDENCIES

perl, php, sh and wget

INSTALLING

Mostly these tools should run as is except that the IP address
in the ip.php check in purevpn/pvpn-start.sh should be changed
to reflect the main external IP for the machine running these tools.

RUNNING

The best approach is to run the scripts in sequence.
Use the runall.sh script to run these tools via cron.

This is example code with site identifying information
redacted. However, it should give a good sense of how
the downloading system  worked.

In terms of timing some sites were better served by doing
a larger number of downloads and some were better served
by doing fewer. For 2015 downloads were done approximately
every 15 minutes. For 2016 downloads were done approximately
every 5 minutes.

As some download processes could get stuck, there was code
to limit the time a process on a job. In particular analysis
scripts could take a long time to finish. These scripts
could be run multiple times and stopped as needed.

