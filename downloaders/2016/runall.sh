#!/bin/sh
echo starting `/bin/date` "in $HOME"
. $HOME/.bashrc
. $HOME/dl/runlib.sh
trap timeout_handler SIGALRM
allow_time 14400
$HOME/dl/site4/site4.sh >> $HOME/dl/site4/site4.out 2>&1 &
$HOME/dl/site3/site3.sh >> $HOME/dl/site3/site3.out 2>&1 &
$HOME/dl/site2/site2.sh >> $HOME/dl/site2/site2.out 2>&1
$HOME/dl/site1/site1.sh >> $HOME/dl/site1/site1.out 2>&1
echo doing mergeback `/bin/date`
$HOME/dl/phones/check.pl
$HOME/dl/phones/analyze.sh
echo doing nlp stuff `/bin/date`
$HOME/dl/phones/get_prov.pl
$HOME/dl/phones/get_lang.pl
$HOME/dl/phones/words.pl -l 1000 
echo doing rsync `/bin/date`
killall /usr/bin/rsync
nice /usr/bin/rsync -rzlpt --delete $HOME/dl/ $DLBACKUP/dl
echo finished `/bin/date`
