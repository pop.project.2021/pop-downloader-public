#!/usr/bin/perl
# download current page for a set of representative categories in site2s
use strict;
use lib ( "$ENV{HOME}/dl/phones" );
use PPDB;

my @locations = qw{
	britishcolumbia
	alberta
	saskatchewan
	winnipeg
	ontario
	quebec
	newbrunswick
	halifax
	stjohns
	whitehorse
	yellowknife
};

my @categories = qw/
	FemaleEscorts
	BodyRubs
	Domination
	TranssexualEscorts
	MaleEscorts
	Strippers
	Datelines
	AdultJobs
	TherapeuticMassage
/;

foreach my $loc (@locations) {
	my $dir = "$loc.site2.com";
	my $url = "http://$dir";
	foreach my $cat (@categories) {
		sleep 2;
		my $retrieved = 0;
		foreach my $pagen (1..10) {
			my $cfile = "$loc-$cat-$pagen.html";
			my $curl = "$url/$cat?page=$pagen";
			system "/usr/bin/wget -O '$cfile' '$curl'" and next;
			open LOC, "< $cfile" or warn "can't open $cfile: $!" and next;
			$retrieved = 0;
			while (<LOC>) {
				while (m#href="((?:https?://\w+\.site2\.com|)/$cat[^"]*?/\d{3,})"#g) {
					my $u = $1;
					my $f;
					if ($u =~ m#^http#) {
						($f = $u) =~ s#^https?://(.*)#$1#;
					} else {
						$u = "/$u" unless $u =~ m#^/#;
						$f = "$dir$u";
						$u = "$url$u";
					}
					next if -f $f or PPDB::seen("site2/$f");
					PPDB::insfile("site2/$f");
					sleep 2;
				 	system "/usr/bin/wget -p -H -nv -nc -x '$u'";
					$retrieved++;
				}
			}
			close LOC;
			last if $retrieved == 0;
		}
	}
}

__END__
4443 escorts ab,bc,on,pq,yukon,nunavut
517483 escorts sask,man,nb,ns,nfld

108649 body rubs ab,bc,on,yukon,nunavut
833324 body rubs sask,man,nb,ns,nfld
735658 body rubs pq

2605140 dom & fetish ab
2154217 dom & fetish bc
1193105 dom & fetish sask
1079477 dom & fetish man
158301 dom & fetish on
1718367 dom & fetish pq
1061916 dom & fetish nb
1052889 dom & fetish ns
1447123 dom & fetish nfld
2208114 dom & fetish yukon,nunavut

2028937 ts ab
1543458 ts bc
990073 ts sask
937314 ts man
158300 ts on
1436349 ts pq
945886 ts nb
938337 ts ns
1018206 ts nfld
1819761 ts yukon,nunavut

2603953 male escorts ab
1780599 male escorts bc
1194110 male escorts sask
1080056 male escorts man
65999 male escorts on
1730901 male escorts pq
1062434 male escorts nb
1930491 male escorts ns
1449647 male escorts nfld

