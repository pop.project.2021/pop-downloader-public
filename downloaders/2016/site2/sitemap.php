#!/usr/bin/php
<?php
# site2 seems to have malformed sitemaps so not using xml 
if (!is_file($argv[1])) die("no valid file supplied");
$accept = '/./';
@include('categories.php');

$mom_data = file_get_contents($argv[1]);
foreach (explode("\n", $mom_data) as $line) {
    if (preg_match('#(http://\S*)#',$line,$m)) {
        if (preg_match($accept, $m[1])) read_map($m[1]);
    }
}

function read_map($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $data = curl_exec ($ch);
    # print $data."\n";
    curl_close ($ch);
    foreach (explode("\n", $data) as $line) {
        if (preg_match('#(http://\S*)#',$line,$m)) {
            exec("/usr/bin/wget -nc -x '{$m[1]}'");
        }
    }
}
?>
