package Extract;
use strict;

sub scrape {
	my ($lines) = @_;
	my $title;
	my $post;
	my $postingid;
	my $postingdate;
	my $age;
	my $city;
	foreach (@$lines) {
		if (m#<div id="postingTitle">#..m#</div>#) {
			if (m#<h1>(.*)</h1#) {
				$title = $1;
			}
		} elsif (m#<div class="adInfo">#..m#</div>#) {
			unless (m#Posted:# or m#</?div#) {
				$postingdate .= $_; 
			}
		} elsif (m#<div class="postingBody">#..m#</div>#) {
			$_ =~ s/<br>//g;
			unless (m#^\s*$# or m#</?div#) {
				$post .= $_;
			}
		} elsif (m#Poster's age: (\d+)#) {
			$age = $1;
		} elsif (m#Post ID: (\d+)#) {
			$postingid = $1;
		} elsif (m#&bull; Location:#..m#</div>#) {
			if (m#^\s*(\w[^<>]+?)\s*$#) {
				$city = $1;
			}
		}
	}
	&trim($title);
	&dateconv($postingdate);
	&trim($post);
	$city = undef if $city =~ 
		/^(?:Monday|Tuesday|Wednesday|Thursday|Friday|Saturday|Sunday|(?:Out|In)calls|Independent)$/i;
	return { title=>$title, postingdate=>$postingdate, post=>$post, age=>$age, postingid=>$postingid, city=>$city };
	
}

sub images {
	my ($lines) = @_;
	my @images;
	foreach (@$lines) {
		if (m#<ul id="viewAdPhotoLayout"#..m#</ul>#) {
			if (m#<img([^>]*)#) {
				my $i = $1;
				my %img;
				if ($i =~ m#src="([^"]*)#) {
					$img{src} = $1;
				}
				if ($i =~ m#alt="([^"]*)#) {
					$img{alt} = $1;
				}
				if ($i =~ m#title="([^"]*)#) {
					$img{title} = $1;
				}
				push @images, \%img;
			}
		}
	}
	\@images;
}

sub dateconv {
	&trim($_[0]);
	use Date::Parse;
	my @t = localtime(str2time($_[0])); 
	$_[0] = sprintf "%04d-%02d-%02d %02d:%02d:%02d\n", 
			$t[5]+1900, $t[4]+1, $t[3], $t[2], $t[1], $t[0];
}

sub trim {
	$_[0] =~ s/^\s*//;
	$_[0] =~ s/\s*$//;
}

1;
