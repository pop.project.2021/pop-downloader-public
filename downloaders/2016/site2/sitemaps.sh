#!/bin/sh
cd $HOME/dl/site2
wget -O sitemap-britishcolumbia.xml http://britishcolumbia.site2.com/sitemap.xml
./sitemap.php sitemap-britishcolumbia.xml
wget -O sitemap-alberta.xml http://alberta.site2.com/sitemap.xml
./sitemap.php sitemap-alberta.xml
wget -O sitemap-saskatchewan.xml http://saskatchewan.site2.com/sitemap.xml
./sitemap.php sitemap-saskatchewan.xml
wget -O sitemap-winnipeg.xml http://winnipeg.site2.com/sitemap.xml
./sitemap.php sitemap-winnipeg.xml
wget -O sitemap-ontario.xml http://ontario.site2.com/sitemap.xml
./sitemap.php sitemap-ontario.xml
wget -O sitemap-quebec.xml http://quebec.site2.com/sitemap.xml
./sitemap.php sitemap-quebec.xml
wget -O sitemap-newbrunswick.xml http://newbrunswick.site2.com/sitemap.xml
./sitemap.php sitemap-newbrunswick.xml
wget -O sitemap-halifax.xml http://halifax.site2.com/sitemap.xml
./sitemap.php sitemap-halifax.xml
wget -O sitemap-stjohns.xml http://stjohns.site2.com/sitemap.xml
./sitemap.php sitemap-stjohns.xml
wget -O sitemap-whitehorse.xml http://whitehorse.site2.com/sitemap.xml
./sitemap.php sitemap-whitehorse.xml
wget -O sitemap-yellowknife.xml http://yellowknife.site2.com/sitemap.xml
./sitemap.php sitemap-yellowknife.xml
sudo rsync -rct ./ /media/passport/site2
