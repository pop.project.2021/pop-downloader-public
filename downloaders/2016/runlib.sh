# from http://www.linuxjournal.com/content/sigalrm-timers-and-stdin-analysis
function allow_time
{
        (
                echo $0 will timeout after $1 seconds
                sleep $1
                kill -ALRM $$
        )&
}

function timeout_handler
{
        echo $0 taking too long
        exit 1
}

## usage
## trap timeout_handler SIGALRM
## # fail after 2.5 hours
## allow_time 9000

