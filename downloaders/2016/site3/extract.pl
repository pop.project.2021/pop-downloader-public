#!/usr/bin/perl -n
$n=($ARGV=~/.*-(\d+)/)[0]; 
while (m/(\w(?:[\w\.\-]+\w|)@\w(?:[\w\.\-]+\w|)\.[a-zA-Z]+)/g) { 
	$key = "$n email $1"; 
	print "$key\n" and $keys{$key}++ unless defined $keys{$key}; 
}
while (m/>[^<>]*([2-9]\d\d)\D?([2-9]\d\d)\D?(\d\d\d\d)[^<>]*</g) { 
	$key = "$n phone $1 $2 $3"; 
	print "$key\n" and $keys{$key}++ unless defined $keys{$key}; 
}
while (m#<img.*src="([^"]+/$n/[^"]+)"#g) {
	$key = "$n id $1";
	print "$key\n" and $keys{$key}++ unless defined $keys{$key};
}

