package Extract;
# this is only for site3 
use strict;
our $posts;

sub scrape {
	my ($lines, $path) = @_;
	my ($title, $post, $postingid, $postingdate, $age, $ethnicity, $city, $phone, $email, %get);
	my @mt = localtime((stat("$ENV{HOME}/dl/$path"))[9]);
	my $postingdate = sprintf '%04d-%02d-%02d %02d:%02d:%02d', $mt[5]+1900, $mt[4]+1, @mt[3,2,1,0];
	if ($path =~ m#.*?(\d+)$#) {
		$postingid = $1;
	}
	foreach (@$lines) {
		if ($get{eth}) {
			$get{eth} = 0;
			$ethnicity = $1 and next if m#"value"[^>]*>([^<]+)#;
		} 
		if ($get{age}) {
			$get{age} = 0;
			$age = $1 and next if m#"value"[^>]*>([^<]+)#;
		} 
		if ($get{city}) {
			$get{city} = 0;
			$city = $1 and next if m#"addressLocality"[^>]*>([^<]*)#;
		}
		if (m#"user-label".*Ethnicity#) {
			$get{eth} = 1;
			next;
		} elsif (m#"user-label".*Age#) {
			$get{age} = 1;
			next;
		} elsif (m#"user-label".*City#) {
			$get{city} = 1;
			next;
		} elsif (m#itemprop="telephone">.*?(\d[^<>]*)#) {
			$phone = $1;
		} elsif (m#"mailto:([^"]*)[^>]*itemprop="email"#) {
			$email = $1;
		} elsif (m#<h2 itemprop="name">([^<]*)#) {
			$title = $1;
		} elsif (m#itemprop="description"#..m#</div>#) {
			s/<[^>]*(?:>|$)//g;
			s/&(?:#\d+|\w+);/ /g;
			$post .= $_ if m/\w/;
		}
	}
	&trim($title);
	&trim($post);
	return { title=>$title, post=>$post, age=>$age, city=>$city, 
		 ethnicity=>$ethnicity, postingdate=>$postingdate,
		 phone=>$phone, email=>$email, postingid=>$postingid , _op_=>'insert'};
}

sub images {
	my ($lines) = @_;
	my @images;
	foreach (@$lines) {
		if (m#<ul class="slides">#..m#</ul>#) {
			if (m#<img([^>]*)#) {
				my $i = $1;
				my %img;
				foreach my $t (qw/src alt title/) {
					if ($i =~ m#$t="([^"]*)#) {
						$img{$t} = $1;
					}
				}
				$img{src} =~ s#/ths/#/main/#;
				push @images, \%img;
			}
		}
	}
	\@images;
}

sub trim {
	chomp $_[0];
	$_[0] =~ s/\r//gs;
	$_[0] =~ s/^\s*//s;
	$_[0] =~ s/\s*$//s;
}

1;
