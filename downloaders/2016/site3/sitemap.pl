#!/usr/bin/perl
## do a naive download of posts and images
## for some reason the php sitemap using xml isn't finding the images
use lib ( "$ENV{HOME}/dl/phones" );
use PPDB;

while (<>) {
	if (m#<loc>(.*)</loc>#) { 
		$url = $1;
		$seen = $keep = 0;
		next unless $url =~ m#-\d+$#;
		@ts = localtime;
		$ts = sprintf '%04d-%02d-%02d %02d:%02d:%02d', $ts[5]+1900,$ts[4]+1,@ts[3,2,1,0];
		print "not adult: $url\n" and next 
			unless $url =~ m#/adult/|/personals/|/jobs/salon-spa-fitness/|/services/therapeutic/#;
		($file = $url) =~ s#^https?://##;
		$keep = 1;
		if (PPDB::seen("site3/$file")) { 
			$seen = 1;
			print "already in db: $url\n";
		} else {
			print "$ts found: $url\n";
			system "/usr/bin/wget -nc -nv -x '$url'";
		}
		PPDB::lastseen("site3/$file", $ts) if -f $file;
	} elsif ($keep) {
		if (m#<lastmod>(.*)</lastmod>#) {
			next unless -f $file;
			$lastmod = $1;
			next unless $lastmod =~ m/^(\d\d\d\d-\d\d-\d\d)T(\d\d:\d\d:\d\d)[-\+]\d\d:\d\d$/;
			$lastmod = "$1 $2";
			print "setting date to $lastmod for $file\n";
			system "/bin/touch -d '$lastmod' '$file'";
			PPDB::insfile("site3/$file", $lastmod) unless $seen;
		} elsif (!$ENV{NOERSLISTIMG} and m#<image:loc>(.*)</image:loc>#) {
			$url = $1;
			(my $file = $url) =~ s#^https?://##;
			print "already have img: $url\n" and next if PPDB::imgseen("site3/$file");
			$ts = localtime;
			print "$ts image: $url\n";
			system "/usr/bin/wget --restrict-file-names=nocontrol -nc -nv -x '$url'";
		}
	}
}
