#!/usr/bin/php
<?php
date_default_timezone_set('America/Vancouver');
$verbose = false;
if (!is_file($argv[1])) die("no valid file supplied");

$mom_data = file_get_contents($argv[1]);
$mom = new SimpleXMLElement($mom_data);
foreach ($mom->sitemap as $sitemap) {
    read_map($sitemap->loc);
}

function read_map($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $data = curl_exec ($ch);
    curl_close ($ch);

    $file = preg_replace('#.*/#','',$url);
    if ($fh = fopen($file,'w')) {
        print "saving map to $file\n";
        fwrite($fh, $data);
        fclose($fh);
    } 
    system("./sitemap.pl $file");
/*
    # this is not reliably finding images
    # print $data."\n";

    $xml = new SimpleXMLElement($data);
    foreach ($xml->url as $page_url) {
        if (!preg_match('#-\d+$#', $page_url->loc)) continue;
	if (!preg_match('#/adult/|/personals/|/jobs/salon-spa-fitness/|/services/therapeutic/#', $page_url->loc)) {
		print "not adult: ".$page_url->loc."\n";
		continue;
	}
        print date('Y-m-d H:i:s')." found: ".$page_url->loc."\n";
        exec("/usr/bin/wget -nc -nv -x '{$page_url->loc}'");
	if (isset($_ENV['NOERSLISTIMG']) && $_ENV['NOERSLISTIMG'] == "1") continue;
        foreach ($page_url->children('image') as $images) {
            foreach ($images as $image_links) {
                # var_export($image_links);
                $a = array($image_links); 
                foreach ($a as $image_url) { 
                    print date('Y-m-d H:i:s')." image: ".$image_url."\n"; 
                    exec("/usr/bin/wget -x -nc -nv '{$image_url}'");
                }
            }
        }
    }
*/
}
?>
