package PPDB;
use DBI;
use strict;

our $db = $ENV{PPDB};
our $host = $ENV{DBHOST};
our $port = $ENV{DBPORT};
our $dsn = "DBI:mysql:database=$db;host=$host;port=$port";
our $dbuser = $ENV{PPDBUSER};
our $dbpw = $ENV{PPDBPW}; 

sub setdsn {
	my ($newdb, $newhost, $newport) = @_;
	$newdb = $db unless defined $newdb;
	$newhost = $host unless defined $newhost;
	$newport = $port unless defined $newport;
	my $newdsn = "DBI:mysql:database=$newdb;host=$newhost;port=$newport";
	$newdsn;
}

sub conn {
	my ($condsn,$user,$pw) = @_;
	$condsn = $dsn unless defined $condsn;
	$user = $dbuser unless defined $user;
	$pw = $dbpw unless defined $pw;
	my $dbh = DBI->connect($condsn, $user, $pw, {AutoCommit=>1,RaiseError=>1})
		or die "can't connect to $dsn as $user: $!";
	return $dbh;
}

sub fullpath {
	my ($basedir, $path) = @_;
	if (-d $basedir) {
		return "$basedir/$path";
	}
	return "$ENV{HOME}/dl/$path";
}

sub seen {
	my $dbh = PPDB::conn();
	my ($file) = @_;
	$file = $dbh->quote($file);
	my $row = $dbh->selectrow_arrayref("select fid from files where path=$file");
	if ('ARRAY' eq ref $row) { return $row->[0]; }
	return 0;
}

sub imgseen {
	my $dbh = PPDB::conn();
	my ($file) = @_;
	$file = $dbh->quote($file);
	my $row = $dbh->selectrow_arrayref("select iid from images where path=$file");
	if ('ARRAY' eq ref $row) { return $row->[0]; }
	return 0;
}

sub lastseen {
	my $dbh = PPDB::conn();
	my ($file, $mtime) = @_;
	if (defined $mtime) {
		$mtime = $dbh->quote($mtime);
	} else {
		$mtime = 'now()';
	}
	$file = $dbh->quote($file);
	$dbh->do("update files set lastseen=$mtime where path=$file") 
		or die $dbh->errstr;
}

sub insfile {
	my $dbh = PPDB::conn();
	my ($file, $mtime) = @_;
	if (defined $mtime) {
		$mtime = $dbh->quote($mtime);
	} else {
		$mtime = 'now()';
	}
	$file = $dbh->quote($file);
	$dbh->do("insert ignore into files (seen,path,lastseen) values ($mtime,$file,$mtime)") 
		or die $dbh->errstr;
}

1;
