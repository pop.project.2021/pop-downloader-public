Common code for 2007-2009 and 2015-2016 processing

The *.mysql files are the db schemas originally used.
The db has evolved to include more variables as well as 
tables used to track checking contact ids etc. However,
this should act as a guide to how it is structured.

Because of the nature of the data a copy of the database
is not publicly available currently.

