#!/usr/bin/perl
our (%opt, $dir, $dl, $canonical, $section, $maxfails);
BEGIN {
	use Getopt::Std;
	# n - no download, d - delay seconds
	getopts('nf:d:',\%opt);
	$section = $dir = $ARGV[0];
	$maxfails = (defined $opt{f} ? $opt{f}: 3);
	die "need a directory! (did you add a / at the end?)" unless $dir =~ /^\w+$/;
	$dl = "$ENV{HOME}/dl";
	$canonical = "$dl/$dir";
	die "$canonical does not exist!" unless -d $canonical;
	chdir $canonical or die "can't change to $canonical!";
}

use lib ( $canonical, "$ENV{HOME}/dl/phones" );
use PPDB;
use Extract;
use Data::Dumper;
use Digest::SHA qw/sha1_hex/;
use URI::Encode qw/uri_decode/;
use strict;
 
my $dbh = PPDB::conn();
exit 1 unless defined $dbh;
my $get = $dbh->prepare(
	"select iid, image as src, imgpath as path from imagefiles ".
	"where path like '$dir/%' ".
	"and sha1 is null and imgignored=1 ".
	"and seen > '2015-01-26' ".
	"order by iid desc "#limit 20 "
);
my $upd = $dbh->prepare(
	"update images set sha1=?,size=?,path=?,ignored=0 where iid=?"
);
my $ign = $dbh->prepare(
	"update images set ignored=? where iid=?"
);
$get->execute or die $get->errstr;
my ($failures);
while (my $row = $get->fetchrow_hashref) {
	unless (-f "$dl/$row->{path}") {
		$row->{path} = uri_decode($row->{path});
		
	}
	print "getting iid $row->{iid} $row->{path}\n";
	unless ($opt{n} or -f "$dl/$row->{path}") {
		my $output = `wget --restrict-file-names=nocontrol -x -nc '$row->{src}' 2>&1`;
		if ($?) {
			$failures++;
			print $output;
			$ign->execute(2,$row->{iid}) or die $ign->errstr;
			die "too many failures!" if $maxfails > 0 and $failures > $maxfails;
			next;
		} else {
			$failures = 0;
		}
		if ($output =~ m#File ‘(.*)’ already there;#) {
			$row->{path} = "$dir/$1";
			print "$row->{path}\n";
		} else {
			sleep (rand()*$opt{d}) if $opt{d};
		}
	}
	my ($path,$sha1,$size) = &get_path_hash($row);
	if (defined $sha1) {
		$upd->execute($sha1,$size,$path,$row->{iid}) or die $upd->errstr;
	} else {
		$ign->execute(1,$row->{iid}) or die $ign->errstr;
	}
}

sub get_path_hash {
	my ($img) = @_;
	my $path;
	if (defined $img->{path}) {
		$path = $img->{path};
	} else {
		($path = $img->{src}) =~ s#^https?://#$dir/#;
	}
	my ($sha1,$size);
	if (-f "$dl/$path") {
		if (open IMG, "$dl/$path") {
			binmode IMG;
			local $/; $/ = undef;
			my $idata = <IMG>;
			die "missing data for $dl/$path!" if (length $idata) != ((stat("$dl/$path"))[7]);
			close IMG;
			$sha1 = sha1_hex($idata);
			$size = length($idata);
		}
		print "hash '$sha1' ($size) for $path\n";
	}
	($path, $sha1, $size);
}

