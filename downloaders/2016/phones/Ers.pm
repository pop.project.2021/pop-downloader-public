package CL::Extract::Ers;
use Exporter;
use Names::Girls;
use CL::DB;
use LWP::Simple;
use Digest::MD5 qw/md5_hex/;
use strict;
our @ISA = qw/Exporter/;
our @EXPORT_OK = qw/%acodes %paths $numpat $tenpat $digitpat $spampat %nummap %tenmap %excludepats grabacronyms/;
our $AUTOLOAD;

=pod

=h1 CL::Extract::Ers 

Module to process and extract phone numbers and other salient data from 
site1's original adult section.

=cut
our %paths = (
	vancouver => {
		url => 'http://vancouver.site1.tld/ers/',
		dir => '/home/site1/vancouver.site1.tld/ers',
	}
);
our %acodes = (
	# vancouver => [ qw/604 778 250/ ],
	vancouver => [ qw/604 778/ ],
);
# use these to exclude posts you aren't interested in based on w4m etc in the title
our %excludepats = (
	het => qr#[tm]4[mwt]{1,2}\s*(?:-\s*\d+|)\s*(?:\([^\)]*\)|)\s*$#,
	gay => qr#w4m\s*(?:-\s*\d+|)\s*(?:\([^\)]*\)|)\s*$#,
);
# put any city related wierdness here
sub add_odd_phones {
	my ($extracted,$content) = @_;
	if ($extracted->{city} eq 'vancouver') {
		$extracted->{phones}->{spam}++ if $content =~ m#imageshack.us/img380/2129/#;
		$extracted->{phones}->{'7788937132'}++
			if $content =~ m#onmensmind.com/files/i57oa22t8jnqru3ni2e2#;
		if ($content =~ m#photobucket\.com/albums/b366/baybeeface#) { 
			$extracted->{phones}->{'7783199825'}++; 
			$extracted->{phones}->{'7783191205'}++; 
		}
		$extracted->{phones}->{'elizabethmassage'}++ 
			if $content =~ m#flickr.com/(?:
					2408/2433048772_728846df05|
					3154/2605724229_09311747c5|
					2118/2252144970_f60decf115|
					2121/2406637222_a7f4ce73a9|
					2143/2402033606_d1846268b5)#x;
		$extracted->{phones}->{'6046832369'}++ if $content =~ m#foxworthyhall#;
		$extracted->{phones}->{'7783892568'}++ if $content =~ m#flickr.com/3110/2345371755_1f7dda51a8_o.jpg#;
		$extracted->{phones}->{'6047264101'}++ if $content =~ m#604\D{0,4}PAM\D{0,4}4101#i;
		$extracted->{phones}->{'6043756113'}++ if $content =~ m#604\D{0,4}DPJ\D{0,4}6113#i;
		$extracted->{phones}->{'6047293595'}++ if $content =~ m#604\D{0,4}729\D{0,4}DJWJ#i;
		$extracted->{phones}->{'scarlettshouse'}++ if $content =~ /scarlett/i;
		$extracted->{phones}->{'scarlettshouse'}++ if $content =~ /please leave the posting information/i;
		$extracted->{phones}->{'pixie'}++ if $content =~ m#pixie1983#;
		$extracted->{phones}->{'6043136545'}++ if $content =~ m#imagecave.com/arianna#;
		$extracted->{phones}->{'nikkiamore'}++ if $content =~ m#nikki-amore#;
		$extracted->{phones}->{'6049990440'}++
			if $content =~ m#icontact.com/icp/loadimage.php/mogile/231917/cb5834a3f03c0aaf773ed9798469bf15#;
		$extracted->{phones}->{'6049990440'}++
			if $content =~ m#photobucket\.com/albums/jj176/DaviesInternational/V-11\.jpg#i;
		$extracted->{phones}->{'7783892568'}++ 
			if $content =~ m#flickr.com/(?:2144|2210)#;
		$extracted->{phones}->{'7783892568'}++ 
			if $content =~ m#flickr.com/2210/#;
		$extracted->{phones}->{'7783892568'}++ 
			if $content =~ m#imikimi.com/image/images_full/48388885r48390277.jpg#;
		$extracted->{phones}->{'7783892568'}++ 
			if $content =~ m#http://pic.xpeeps.com/xpeeps/images/usergallerypics/620399/#;
		$extracted->{phones}->{'7783892568'}++ if $content =~ m#imikimi.com/image/images_full/47076412.gif#;
		$extracted->{phones}->{'6043141297'}++ 
			if $content =~ /imagedeposit.com.*UserID_(?:94214|93772)/i;
		$extracted->{phones}->{'7788937132'}++ 
			if $content =~ m#freeimagehosting.net/uploads/(?:c7be677979.jpg|e9a22ba652.jpg)#;
		$extracted->{phones}->{'7788937132'}++ 
			if $content =~ m#onmensmind\.com/files/3ju8cdsm8to5k1c1uajo\.gif#i;
		$extracted->{phones}->{'7787377529'}++ if $content =~ /737\D{0,3}PLAY/i;
		$extracted->{phones}->{"${1}${2}7399"}++ if $content =~ /(604|778)\D{0,3}(\d{3})\D{0,3}SEXY/i;
		$extracted->{phones}->{'6047337393'}++ if $content =~ /SEE\W*SEXE\b/i;
		if ($content =~ /vanessa\W*kelly/i or $content =~ /srhd/i) {
			$extracted->{phones}->{'vanessa'}++;
		}
		$extracted->{phones}->{'mistress_t'}++ if $content =~ /\bmis(?:tres)s\W*t\b/i;
		$extracted->{phones}->{'6044638845'}++ if $content =~ /463\D{0,5}8845/;
		$extracted->{phones}->{'7789883466'}++ if $content =~ /988\D{0,5}DION/i;
		$extracted->{phones}->{'7788888538'}++ if $content =~ /billionairess/i;
		$extracted->{phones}->{'7787377529'}++ if $content =~ /778\D{0,1}737\D{0,1}PLAY/i;
		$extracted->{phones}->{'4169385834'}++ if $content =~ /416\s*938\s*5834/;
		$extracted->{phones}->{'6045239912'}++ if $content =~ /523\D{0,10}9912/;
		$extracted->{phones}->{'6045239918'}++ if $content =~ /523\D{0,10}9918/;
		$extracted->{phones}->{'6048591313'}++ if $content =~ m#859\D{0,5}1313#;
		$extracted->{phones}->{'dasha'}++ if $content =~ m#fubar.com/72/34/1114327/#;
		$extracted->{phones}->{'dasha'}++ if $content =~ m#fubar.com/90/47/757409#;
		$extracted->{phones}->{'dasha'}++ if $content =~ m#webshots.com/image/38445/#;
		$extracted->{phones}->{'dasha'}++ if $content =~ m#webshots.com/image/42994/#;
		$extracted->{phones}->{'7788293673'}++ if $content =~ m#778\D{0,3}829\D{0,3}DOPE#i;
		$extracted->{phones}->{'6049169120'}++ if $content =~ m#6[o0]4\D{0,3}9[i1]6\D{0,3}9[i1]2[o0]#i;
		$extracted->{phones}->{'6046823269x7012'}++ 
			if $content =~ m#604\D{0,3}682\D{0,3}3269\s*(?:ext|box)\s*7012#i;
		$extracted->{phones}->{'6048162354'}++ if $content =~ m#pimpandhost.com/media/simple/13397/#;
		$extracted->{phones}->{'celticviolet'}++ if $content =~ 
					m#flickr.com/(?:
					2006/2231063429_44ebfc0a50|
					2366/2231872080_406c6cf62d|
					2362/2231089077_756ef114e0|
					2385/2231855966_ae0f49021d
					)\.jpg#x;
		$extracted->{phones}->{'celticviolet'}++ if $content =~ 
			m#http://img395.imageshack.us/img395/2502/bondage1webjn2.jpg|
			  http://img235.imageshack.us/img235/7172/legsaddhh7.jpg#x;
	}
	$extracted;
}

our $spampat = qr{
\b(?:web)cam\b|
\bm[\W_]*y[\W_]*r[\W_]*e[\W_]*d[\W_]*p[\W_]*a[\W_]*g[\W_]*e[\W_]*s[\W_]*|
\bm\W*r\W*p\W*|
<font [^>]*><small>|
<font [^>]*size=(?:"|)[01](?:"|) [^>]*>|
<font color="ivory"|
font-size:\s*[012](?:px|%)|
lonelyjasmin|
angel\W*review|
playangels|
meetsomeoneforsex|
jamsite|
9p0\.net|
adultflirtonline|
absoluteamateurgirls|
asymmetricalmedia|
chatmasters|
flingnsa|
flirtpicsonline|
flirtyimages|
geocities|
hosturfiles|
hundredk|
maturelustnow|
meetflingdate|
online-flirtdates|
profiles\.yahoo|
purensa|
radioinsert|
ratemysexyflirt|
searchflingdate|
sfducktours|
sftourreviews|
sweetpicsonline|
thekyj|
(?:Yahoo|AIM|ICQ|MSN\/WL):\s*\w+|
Town.*?Friends.*?com|
WWW.*?weekend.*?friends.*?COM|
Holiday.*?wives.*?com|
hotgirlangelina|
kinkykelly778|
camaholic\.net|
camsprite\.com|
cerf\.ca|
fundaters\.com|
myglamgirls\.com
}ix;
# note that the order of the patterns being matched in the "or" affects what gets matched even if match is shorter
our $acronympat = qr#[^a-zA-Z0-9]{0,3}(\d{3}|[a-zA-Z]{3})[^a-zA-Z0-9]{0,3}(\d{4}|[a-zA-Z]{4})\b#;
our $numpat = qr/
(fourteen|sixteen|seventeen|eighteen|nine?teen|
good|honest|oo*hh*|zero|owe|one|won|on|two|too*|three*|fourty|forty|fou*r|five|
six(?:ty|)|seven(?:ty|)|sev|eight(?:ty|)|eihgt|eieght|ate|nine(?:ty|)|
ten|eleven|twelve|thirteen|fifteen|twenty|thirty|fifty)
/x;
our $tenstr = "twenty|thirty|forty|fifty|sixty|seventy|eighty|ninety";
our $tenpat = qr/($tenstr)/;
our $digitpat = qr/($tenstr|[1-9]|one|two|thre*|fou*r|five|six|se*ve*n|eight|nine|)/;
our %acronymmap = (
	a => 2,
	b => 2,
	c => 2,
	d => 3,
	e => 3,
	f => 3,
	g => 4,
	h => 4,
	i => 4,
	j => 5,
	k => 5,
	l => 5,
	m => 6, 
	n => 6,
	o => 6,
	p => 7,
	q => 7,
	r => 7,
	s => 7,
	t => 8,
	u => 8,
	v => 8,
	w => 9,
	x => 9,
	y => 9,
	z => 9,
);

our %nummap = (
	'honest' => '',
	'good' => '',
	'oh' => 0,
	'ohh' => 0,
	'ooh' => 0,
	'zero' => 0,
	'owe' => 0,
	'one' => 1,
	'on' => 1,
	'won' => 1,
	'two' => 2,
	'too' => 2,
	'tooo' => 2,
	'to' => 2,
	'threee' => 3,
	'three' => 3,
	'thre' => 3,
	'four' => 4,
	'for' => 4,
	'five' => 5,
	'six' => 6,
	'seven' => 7,
	'sven' => 7,
	'sevn' => 7,
	'svn' => 7,
	'sev' => 7,
	'eight' => 8,
	'eieght' => 8,
	'eihgt' => 8,
	'ate' => 8,
	'nine' => 9,
	'ten' => 10,
	'eleven' => 11,
	'twelve' => 12,
	'thirteen' => 13,
	'fourteen' => 14,
	'fifteen' => 15,
	'sixteen' => 16,
	'seventeen' => 17,
	'eighteen' => 18,
	'ninteen' => 19,
	'nineteen' => 19,
	'twenty' => 20,
	'thirty' => 30,
	'forty' => 40,
	'fourty' => 40,
	'fifty' => 50,
	'sixty' => 60,
	'seventy' => 70,
	'eighty' => 80,
	'eihgty' => 8,
	'ninety' => 90,
);
our %tenmap = (
	'twenty' => 2,
	'thirty' => 3,
	'forty' => 4,
	'fifty' => 5,
	'sixty' => 6,
	'seventy' => 7,
	'eighty' => 8,
	'eihgty' => 8,
	'ninety' => 9,
);

sub new {
	my $class = shift;
	my $extracted = {};
	bless $extracted, $class;
	$extracted->{city} = shift;
	$extracted->{orientation} = shift;
	my %params = @_;
	$extracted->{no_image_save} = $params{no_image_save};
	$extracted->{phones} = {};
	$extracted;
}

sub open_post {
	my ($extracted,$fname) = @_;
	open POST, $fname or die "couldn't open $fname: $!";
	local $/; undef $/;
	$extracted->{whole_post} = <POST>;
	$extracted->{whole_post} =~ s/\r//g;
	close POST;
	$extracted;
}

sub extract {
	my ($extracted) = @_;
	my $getcontent = my $getimages = 0;
	my($location,@names,%images,$title,$date,$content,%imgstubs,$postingid);
	my $raw = $extracted->{whole_post};
	foreach (split "\n", $raw) {
		if (m#<h2>(.*)</h2>#i) {
			$title = $1 unless defined $title;
		} elsif (m#^Date: (\d{4})-(\d{2})-(\d{2}), +(\d{1,2}):(\d{2})(AM|PM) [A-Z]+<br>$#) {
			my ($year,$mon,$day,$hour,$min,$ampm) = ($1,$2,$3,$4,$5,$6);
			if ($ampm eq 'PM') { 
				$hour += 12 if $hour != 12;
			} else { 
				$hour = 0 if $hour == 12;
			}
			$date = sprintf('%04d-%02d-%02d %02d:%02d:00',$year,$mon,$day,$hour,$min);
			$getcontent = 1;
		} elsif ($getcontent) {
			if (s#<table##) {
				$getimages = 1;
				$getcontent = 0;
			}
			$content .= $_;
		} elsif ($getimages) {
			if (m#<td[^>]*><img src="([^"]+)"></td>#) {
				my $img = $1;
				$images{$img} = {};
				my $stub = ($img=~m#.*/(.*)#)[0];
				$imgstubs{$stub}++;
			}
			$getimages = 0 and last if m#</table>#;
		} elsif (/PostingID: (\d+)/) {
			$postingid = $1;
		} elsif (/Location: ([\w\s]+)/) {
			$location = $1;
		}
	}
	@{$extracted}{qw/images imgstubs title postingid date raw_post location/} = 
		(\%images, \%imgstubs, $title, $postingid, $date, $content, $location);
	$extracted;
}

# now process $content and look for phone numbers etc
sub process {
	my ($extracted,$pregrab) = @_;
	my $title = $extracted->{title};
	$extracted->{ignore} = 1 and return $extracted 
		if $title =~ $excludepats{$extracted->{orientation}};
	my $lctitle = lc($title);
	my $spam = 0;
	$spam = 1 if $lctitle =~ m/(?:$spampat)/;
	my $name;
	if ($lctitle =~ m#(?:\b|\W)($girl_name_pat)(?:\b|\W)#) {
		$name = $1 if $1 ne '';
	}
	$extracted->{age} = $lctitle =~ /(\d{2})\s*\(.*\)\s*$/s ? $1 : undef;
	my $content = $extracted->{raw_post};
	$content .= ' ';
	$content = lc($content);
	$content =~ s/\r//g;
	$content =~ s/\s/ /sg;
	$extracted->add_odd_phones($content);
	$content =~ s/#\d+;//g;
	# do a basic check for phone numbers in the title
	&grabacronyms($extracted,$lctitle) unless $extracted->hasphone;
	&grabphones($extracted,$lctitle) unless $extracted->hasphone;
	unless ($extracted->hasphone) {
		$spam = 1 if $content =~ m/(?:$spampat)/; 
		(my $spamcheck = $content) =~ s#<[^>]*>+##sg;
		$spam = 1 if $spamcheck =~ m/(?:$spampat)/; 
	}
	$content =~ s/<[^>]*>+/ /g;
	$content =~ s/&\w+;/ /g;
	$spam = 1 if $content =~ m/(?:$spampat)/;
	$content =~ s/(?:star|\*)\s*(?:eight|8)\s*(?:two|2)\b//gi;
	while ($content =~ /(?:\b|\W)($girl_name_pat)(?:\b|\W)/g) {
		my $n = $1;
		$name = $n if length($name) < length($n);
	}
	$content =~ s/[456]\s*(?:foot|feet)\s*\d{1,2}\s*(?:inch|"|)\s*(?:tall|)/DELETED/;
	$content =~ s/[234]\d\s*[a-f]+[\W\s]*[234]\d[\W\s]*[234]\d/DELETED/;
	$content =~ s/[abcde]+\s*[234]\d\s*[\W\s]*[234]\d[\W\s]*[234]\d/DELETED/;
	$content =~ s/[abcde]*\s*[234]\d\s*[a-f]*(?:"|)\s*(?:waist|bust|chest|breast|hips)/DELETED/g;
	$content =~ s/\b(?:[12]\d\d|9\d)\s*[il]b/DELETED/;
	$content =~ s/[\W_]/ /g;
	$content =~ s/  +/ /g;
	# this early grab before converting number words is resulting in errors
	# probably we should do a run with it and a run without it.
	if ($pregrab) {
		&grabphones($extracted,$content) unless $extracted->hasphone;
	}
	my $testcontent = $content; # see below for where this is used
	$content =~ s/$tenpat\s*$digitpat/exists $tenmap{$2} ? $nummap{$1}.$nummap{$2} : $tenmap{$1}.($2 || '0')/eg;
	$content =~ s/$numpat/$nummap{$1}/eg;
	$content =~ s/(\d) /$1/g;
	$content =~ s/double\s*(\d)/$1$1/g;
	$content =~ s/triple\s*(\d)/$1$1$1/g;
	$content =~ s/thousand/000/gi;
	# test without converting o to 0
	&grabacronyms($extracted,$content) unless $extracted->hasphone;
	&grabphones($extracted,$content) unless $extracted->hasphone;
	# test again with simpler mapping
	unless ($extracted->hasphone) {
		$testcontent =~ s/$tenpat/$tenmap{$1}0/g;
		$testcontent =~ s/$numpat/$nummap{$1}/eg;
		$testcontent =~ s/(\d) /$1/g;
		$testcontent =~ s/double\s*(\d)/$1$1/g;
		$testcontent =~ s/triple\s*(\d)/$1$1$1/g;
		$testcontent =~ s/thousand/000/gi;
		&grabacronyms($extracted,$testcontent);
		&grabphones($extracted,$testcontent) unless $extracted->hasphone;
	}
	$content =~ s/o/0/g;
	$content =~ s/[il]/1/g;
	$content =~ s/(\d)\D+(\d)/$1$2/g;
	$extracted->{ignore} = 1 and return $extracted 
		unless length($content) >= 7 and $content =~ /\w/;
	&grabphones($extracted,$content) unless $extracted->hasphone;
	$extracted->{phones}->{spam}++ if $spam and !$extracted->hasphone;
#	$extracted->getimages;
	$extracted->img2phone unless $extracted->hasphone;
	$name = undef if $name =~ /\d/;
	if (!$extracted->{phones}->{spam} and $name =~ /\w/ and !$extracted->hasphone) {
		$extracted->{phones}->{$name}++ if $name =~ /\w/;
	}
	my $phonelist = join ",",sort keys %{$extracted->{phones}};
	@{$extracted}{qw/processed_post name spam phonelist/} = ($content,$name,$spam,$phonelist);
	$extracted;
}

# taken from cl.pl - ties in the db related stuff
sub insertdb {
	my ($post) = @_;
	return $post if $post->ignore;
	# add stuff to the db
	my $upd_img = $cldb->prepare("insert into img (md5,size,img,postingid) values (?,?,?,?)");
	foreach my $img (keys %{$post->images}) {
		$upd_img->execute(@{$post->{images}->{$img}}{qw/md5 size/},$img,$post->postingid) 
			or warn $upd_img->errstr and $cldb->rollback;
	}
	foreach my $phone (keys %{$post->phones}) {
		$get_phone->execute($phone) or die $get_phone->errstr;
		unless ($get_phone->rows) {
			$upd_phone->execute($phone,$post->name) or warn $upd_phone->errstr and $cldb->rollback;
		}
		$get_phone->finish;
		$upd_phone2posts->execute($phone,$post->postingid) 
			or warn $upd_phone2posts->errstr and $cldb->rollback;
	}
	$upd_posts->execute(@{$post}{qw/age location raw_post title date phonelist postingid/}) 
		or warn $upd_posts->errstr and $cldb->rollback;
	$cldb->commit;
	$post;
}

sub tostring {
	my ($extracted,$sep,$all) = @_;
	# default to single line short form
	$sep = "; " unless defined $sep;
	$all = 0 unless defined $all;
	my ($postingid,$title,$date,$name,$age,$unprocessed,$content) = 
		@{$extracted}{qw/postingid title date name age raw_post processed_post/};
	my $phones = join ",",sort keys %{$extracted->{phones}};
	my $images = join ",",keys %{$extracted->{images}};
	my @string = ( 
		"$postingid",
		"title: $title",
		"date: $date",
		"name: $name",
		"age: $age",
		"phone: $phones",
		"images: $images",
	);
	if ($all) {
		push @string, ("unprocessed content: $unprocessed","processed content: $content$sep");
	}
	join "$sep", @string;
}

sub hasphone {
	my $extracted = shift;
	return 1 if scalar(%{$extracted->{phones}});
	0;
}

sub grabacronyms {
	my ($extracted,$firsttest) = @_;
	foreach (@{$acodes{$extracted->{city}}}) {
		my $pat = qr/($_$acronympat)/;
		while ($firsttest =~ m#$pat#g) {
			my @bits = ($2,$3);
			my $pn;
			foreach (@bits) {	
				if (/[a-z]+/) {
					foreach (split '', $_) {
						$pn .= $acronymmap{$_};
					}
				} else {
					$pn .= $_;
				}
			}
			&addphone($extracted,"$_$pn");
		}
	}
}

sub grabphones {
	my ($extracted,$firsttest,$pattern) = @_;
	$firsttest =~ s/(\d)\D*/$1/g;
	my @as = @{$acodes{$extracted->{city}}};
	foreach (@as) {
		# clean up 604604 type things to avoid some probably invalid phone numbers
		(my $ft = $firsttest) =~ s/$_$_/$_/g;
		my $pat = defined $pattern ? $pattern : qr/$_[2-9]\d{6}/;
		while ($ft =~ m#($pat)#g) {
			&addphone($extracted,$1);
		}
	}
}

sub addphone {
	my ($extracted,$num) = @_;
	my $phone;
	my @as = @{$acodes{$extracted->{city}}};
	my $acodepat = join '|', @as;
	$acodepat .= '[2-9]\d{6}';
	return unless $num =~ /$acodepat/;
	if (length($num) == 10) {
		$phone = $num;
	}
	if ($phone) {
		$extracted->{phones}->{$phone}++;
		return;
	}
	foreach (@as) {
		my $pat = $_.'[2-9]\d{6}';
		while ($num =~ /($pat)/g) {
			$phone = $1;
			$extracted->{phones}->{$phone}++;
		}
	}
	$extracted;
}

# image handling functions
sub getimages {
	my $extracted = shift;
	my $check = $cldb->prepare("select img from img where md5=? and size=?");
	my $postingid = $extracted->{postingid};
	foreach my $img (keys %{$extracted->{images}}) {
		sleep rand;
		(my $fname = $img) =~ s#.*/##;
		# my $data = get($img);
		my $data = `/usr/bin/lynx -source $img`;
		next unless defined $data;
		my $md5 = md5_hex($data);
		my $size = length($data);
		@{$extracted->{images}->{$img}}{qw/md5 size/} = ($md5,$size);
		$check->execute($md5,$size) or die $check->errstr;
		unless ($check->rows) {
			unless ($extracted->no_image_save) {
				my $squashed = sprintf('%s/%s-%06d.jpg',$climgdir,$md5,$size);
				open IMG, "> $squashed" or warn "can't open $squashed: $!\n$data\n" and next;
				binmode(IMG);
				print IMG $data;
				close IMG;
			}
			$extracted->{images}->{$img}->{'new'} = 1;
		}
		$check->finish;
	}
	$extracted;
}

sub img2phone {
	my $extracted = shift;
	my $getquery = "select distinct phone2posts.phone,date(posted) from phone2posts,img,posts ".
		"where img.postingid=posts.postingid and posts.postingid=phone2posts.postingid ".
		"and img.hide <> 1 and img.md5=? order by date(posted) desc";
	my $get = $cldb->prepare($getquery);
	foreach my $img (keys %{$extracted->{images}}) {
		my $i = $extracted->{images}->{$img};
		next if $i->{'new'};
		$get->execute($i->{md5}) or die $get->errstr;
		my $foundphone;
		while (my $row = $get->fetch) {
			if ($row->[0] =~ /^(?:spam|notad|male|t4m)$/) {
				delete $extracted->{phones};
				$extracted->{phones}->{$row->[0]}++;
			} else {
				next if $foundphone;
				$extracted->{phones}->{$row->[0]}++;
				$foundphone = 1;
			}
		}
		$get->finish;
	}
	$extracted;
}

sub AUTOLOAD {
	my $extracted = shift;
	(my $param = $AUTOLOAD) =~ s/.*:://;
	return $extracted->{$param};
}

1;

__END__
