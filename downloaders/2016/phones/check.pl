#!/usr/bin/perl
# find out if an id grabbed by findid.pl is actually part of the ad grabbed by ads.pl
# use lib ( "$ENV{HOME}/dl/phones" );
use PPDB;
print "db $ENV{PPDB}\n";
use PPID;
use PPIMG;
use Getopt::Std;
# use Data::Dumper;
use strict;
my %opt;
# n - no images, N - ignore links in posts, k - keep existing ids always
getopts('eknNvp:s:',\%opt);
my $check_existing = $opt{e};
$PPID::verbose = 1 if $opt{v};
my $pass = ($opt{p} =~ /^[1-3]$/ ? $opt{p}: undef);
my $section = $opt{s};
my $secquery = " section='$section' and " if $section =~ m/^\w+$/;

my $dbh = PPDB::conn();
exit 1 unless defined $dbh;
my $getads;
if ($ARGV[0] eq 'nopass') {
	$getads = $dbh->prepare(
		"select distinct fid,title,post,phone,email,section,path,basedir from adidfiles ".
		"where $secquery pass is null order by fid"
	);
} elsif ($ARGV[0] =~ /^\d\d\d\d-\d\d-\d\d/) {
	my $start = $dbh->quote($ARGV[0]);
	if ($ARGV[1] =~ /^\d\d\d\d-\d\d-\d\d/) {
		my $finish = $dbh->quote($ARGV[1]);
		($finish,$start) = ($start,$finish) if $start gt $finish;
		$getads = $dbh->prepare(
			"select fid,pagesection,title,post,phone,email,section,path,basedir from adfiles ".
			"where $secquery seen between $start and $finish order by fid"
		);
	} else {
		$getads = $dbh->prepare(
			"select fid,pagesection,title,post,phone,email,section,path,basedir from adfiles ".
			"where $secquery seen >= $start order by fid"
		);
	}
} elsif ($ARGV[0] =~ /^\d+$/) {
	$getads = $dbh->prepare(
		"select fid,pagesection,title,post,phone,email,section,path,basedir from adfiles where $secquery fid=$ARGV[0]"
	);
} elsif ($ARGV[0] =~ /\w+/) {
	my $path = $dbh->quote($ARGV[0]);
	$getads = $dbh->prepare(
		"select fid,pagesection,title,post,phone,email,section,path,basedir from adfiles where $secquery path=$path"
	);
} else {
	$getads = $dbh->prepare(
		"select fid,pagesection,title,post,phone,email,section,path,basedir from adfiles where $secquery checked=0 order by fid"
	);
}
my $getadids = $dbh->prepare(
	"select id from id2file where fid=? and pagesection=?"
);
my $conf = $dbh->prepare(
	"update id2file set capture=?,valid=?,confirmed=1,ignored=0 where id=? and fid=? and pagesection=?"
);
my $ign = $dbh->prepare(
	"update id2file set confirmed=0,ignored=2 where id=?"
);
my $chk = $dbh->prepare(
	"update ads set checked=1 where fid=? and pagesection=?"
);
my $insids = $dbh->prepare(
	"insert ignore into ids (id,confirmed) values (?,1)"
);
my $insid2file = $dbh->prepare(
	"insert ignore into id2file (id,fid,pagesection,confirmed) values (?,?,?,1)"
);
# adapted from the patterns from findid.pl
my @pats = (
	qr/([2-9]\d\d)\D{0,5}([2-9]\d\d)\D{0,5}(\d\d\d\d)/,
	qr/((?:\w+\.)*\w+)(?:\@|\W*at\W*)((?:[\-\w]+\.)*\w+)(?:\.|\W*dot\W*)([a-z]{2,4})(?:$|\W)/,
	qr{<a[^>]*href=(["']?)(https?://[^"]*)(["']?)},
);

my $div = ("=" x 80)."\n";

$getads->execute or die $getads->errstr;
while (my $adrow = $getads->fetchrow_hashref) {
	my ($fid, $pagesection, $title, $post, $phone, $email, $section, $path, $basedir) = 
		@{$adrow}{qw/fid pagesection title post phone email section path basedir/};
	my $content = "$title\n\n$post\n\n$phone\n\n$email";
	$content =~ s/&(?:#\d+|\w+);/ /g;
	$content =~ s/<[^>]*>//g;

	print "\n\nad $fid $section $title $path\n";
	my %seen;
	if ($check_existing) {
		$getadids->execute($fid) or die $getadids->errstr;
		if ($getadids->rows) {
			while (my $idrow = $getadids->fetch) {
				my ($id) = @$idrow;
				$seen{$id} = 1;
				my $pat = $id;
				if ($id =~ /^[\d ]*$/) {
					$pat =~ s/ //g;
					my @pat = split //, $pat;
					$pat = join "\\D*", @pat;
					$pat = qr/\S*($pat)\S*/;
				} else {
					$pat =~ s/ /\.\*/g;
					$pat = qr/$pat/;
				}
				print "id $id => $pat ";
				if ($content =~ /($pat)/) {
					my $capture = $1;
					my $valid = PPID::valid($id,$capture,$adrow);
					print " valid=$valid, found ",(length($capture))." $capture\n";
					$conf->execute($capture, $valid, $id, $fid, $pagesection) or die $conf->errstr;
				} else {
					print " missing\n";
					print "$div$content\n$div\n";
					# $ign->execute($id) or die $ign->errstr;
				}
			}
		} else {
			print "no ids\n";
		}
		$getadids->finish;
	} else {
		PPID::extract($adrow, $dbh, $pass, $opt{k}, $opt{N});
	}
	PPIMG::save($path, $fid, $dbh, undef, $basedir, $pagesection) unless $opt{n};
}
$getads->finish;
