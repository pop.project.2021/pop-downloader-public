package PPURI;
# gather info from ad urls

# list of location identifying url parts from PPAC
our %locales = (
	'site2' => {
		'alberta.site2.com' => 'AB',
		'britishcolumbia.site2.com' => 'BC',
		'halifax.site2.com' => 'NS',
		'newbrunswick.site2.com' => 'NB',
		'ontario.site2.com' => 'ON',
		'quebec.site2.com' => 'QC',
		'saskatchewan.site2.com' => 'SK',
		'stjohns.site2.com' => 'NB',
		'whitehorse.site2.com' => 'YT',
		'winnipeg.site2.com' => 'MB',
		'yellowknife.site2.com' => 'YT',
	},
	'site1' => {
		'abbotsford.site1.ca' => 'BC',
		'barrie.site1.ca' => 'ON',
		'belleville.site1.ca' => 'ON',
		'brantford.site1.ca' => 'ON',
		'calgary.site1.ca' => 'AB',
		'cariboo.site1.ca' => 'BC',
		'chatham.site1.ca' => 'ON',
		'comoxvalley.site1.ca' => 'BC',
		'cornwall.site1.ca' => 'ON',
		'cranbrook.site1.ca' => 'BC',
		'edmonton.site1.ca' => 'AB',
		'ftmcmurray.site1.ca' => 'AB',
		'guelph.site1.ca' => 'ON',
		'halifax.site1.ca' => 'NS',
		'hamilton.site1.ca' => 'ON',
		'hat.site1.ca' => 'AB',
		'kamloops.site1.ca' => 'BC',
		'kelowna.site1.ca' => 'BC',
		'kingston.site1.ca' => 'ON',
		'kitchener.site1.ca' => 'ON',
		'lethbridge.site1.ca' => 'ON',
		'londonon.site1.ca' => 'ON',
		'montreal.site1.ca' => 'QC',
		'nanaimo.site1.ca' => 'BC',
		'newbrunswick.site1.ca' => 'NB',
		'newfoundland.site1.ca' => 'NF',
		'niagara.site1.ca' => 'ON',
		'ottawa.site1.ca' => 'ON|QC',
		'owensound.site1.ca' => 'ON',
		'peace.site1.ca' => 'AB',
		'pei.site1.ca' => 'PE',
		'peterborough.site1.ca' => 'ON',
		'princegeorge.site1.ca' => 'BC',
		'quebec.site1.ca' => 'QC',
		'reddeer.site1.ca' => 'AB',
		'regina.site1.ca' => 'SK',
		'saguenay.site1.ca' => 'QC',
		'sarnia.site1.ca' => 'ON',
		'saskatoon.site1.ca' => 'AB',
		'sherbrooke.site1.ca' => 'QC',
		'skeena.site1.ca' => 'BC',
		'soo.site1.ca' => 'ON',
		'sudbury.site1.ca' => 'ON',
		'sunshine.site1.ca' => 'BC',
		'territories.site1.ca' => 'YT',
		'thunderbay.site1.ca' => 'ON',
		'toronto.site1.ca' => 'ON',
		'troisrivieres.site1.ca' => 'QC',
		'vancouver.site1.ca' => 'BC',
		'victoria.site1.ca' => 'BC',
		'whistler.site1.ca' => 'BC',
		'whitehorse.site1.ca' => 'YT',
		'windsor.site1.ca' => 'ON',
		'winnipeg.site1.ca' => 'MB',
		'yellowknife.site1.ca' => 'YT',
	},
	'site4' => {
		'calgary.site4.com' => 'AB',
		'centre-du-quebec.site4.com' => 'QC',
		'edmonton.site4.com' => 'AB',
		'estrie.site4.com' => 'QC',
		'fredericton.site4.com' => 'NB',
		'gatineau-hull.site4.com' => 'QC',
		'halifax.site4.com' => 'NS',
		'hamilton.site4.com' => 'ON',
		'london.site4.com' => 'ON',
		'moncton.site4.com' => 'NB',
		'montreal.site4.com' => 'QC',
		'montreal-rive-nord.site4.com' => 'QC',
		'montreal-rive-sud.site4.com' => 'QC',
		'ottawa.site4.com' => 'ON|QC',
		'quebec.site4.com' => 'QC',
		'quebec-rive-sud.site4.com' => 'QC',
		'regina.site4.com' => 'SK',
		'saguenay-lac-st-jean.site4.com' => 'QC',
		'saint-john.site4.com' => 'NB',
		'saskatoon.site4.com' => 'SK',
		'terrebonne.site4.com' => 'QC',
		'toronto.site4.com' => 'ON',
		'vancouver.site4.com' => 'BC',
		'victoria.site4.com' => 'BC',
		'windsor.site4.com' => 'ON',
		'winnipeg.site4.com' => 'MB',
	},
	'site3' => {
		'calgary' => 'AB',
		'central-ontario' => 'ON',
		'edmonton' => 'AB',
		'greater-toronto' => 'ON',
		'hamilton-niagara' => 'ON',
		'interior' => 'BC',
		'metro-vancouver' => 'BC',
		'n_alberta' => 'AB',
		'new-brunswick' => 'NB',
		'newfoundland' => 'NF',
		'northern-bc' => 'BC',
		'northern-ontario' => 'ON',
		'nova-scotia' => 'NS',
		'nw_territories' => 'YT',
		'quebec' => 'QC',
		'regina' => 'SK',
		'saskatoon' => 'SK',
		'south-eastern-ontario' => 'ON',
		'south-western-ontario' => 'ON',
		'vancouver-island' => 'BC',
		'winnipeg' => 'MB',
		'yukon' => 'YT',
	},
);

our $catpat = qr/-(female-escorts|fetish-domination|erotic-massage|erotic-model|male-escorts|massage-by-men|travesti-transexual)-/;

# PROVINCE,LOCATION,CATEGORY_PARENT,CATEGORY_CHILD
sub parse {
	my ($path) = @_;
	my @parts = split '/', $path;
	my %parse;
	if ($parts[0] eq 'site2') {
		%parse = (
			prov => $locales{$parts[0]}{$parts[1]},
			location => ($parts[1]=~/^([\w\-]+)\./)[0],
			category_parent => 'adult entertainment',
			category_child => lc $parts[2],
		);

	} elsif ($parts[0] eq 'site1') {
		%parse = (
			prov => $locales{$parts[0]}{$parts[1]},
			location => ($parts[1]=~/^([\w\-]+)\./)[0],
			category_parent => 'services',
			category_child => 'therapeutic',
		);
		
	} elsif ($parts[0] eq 'site3') {
		if ($parts[1] eq 'www.site3.com') {
			%parse = (
				prov => $locales{$parts[0]}{$parts[3]},
				location => $parts[3],
				category_parent => 'adult',
				category_child => $parts[2],
			);
		} elsif ($parts[1] eq 'www.site3.com' or $parts[1] eq 'www.site3.cc') {
			%parse = (
				prov => $locales{$parts[0]}{$parts[4]},
				location => $parts[4],
				category_parent => $parts[2],
				category_child => $parts[3],
			);
		}

	} elsif ($parts[0] eq 'site4') {
		my $city = ($parts[1]=~/^([\w\-]+)\./)[0];
		my ($category) = ($parts[2]=~/$catpat/);
		%parse = (
			prov => $locales{$parts[0]}{$parts[1]},
			location => $city,
			category_parent => 'adult entertainment',
			category_child => $category,
		);
	}
	\%parse;
}

1;

