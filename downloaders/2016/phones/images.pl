#!/usr/bin/perl
our ($dir, $canonical, $section);
BEGIN {
	$section = $dir = $ARGV[0];
	die "need a directory! (did you add a / at the end?)" unless $dir =~ /^\w+$/;
	$canonical = "$ENV{HOME}/dl/$dir";
	die "$canonical does not exist!" unless -d $canonical;
	chdir $canonical or die "can't change to $canonical!";
}

use lib ( $canonical, "$ENV{HOME}/dl/phones" );
use PPDB;
use Extract;
use Data::Dumper;
use Digest::SHA qw/sha1_hex/;
use strict;
 
my $dbh = PPDB::conn();
exit 1 unless defined $dbh;
my $get = $dbh->prepare(
	"select fid, path from files ".
	"where path like '$dir/%' ".
	"and ignored=0 and fid not in (select distinct fid from img2file) ".
	"and seen > '2015-01-31' ".
	"order by fid desc " #limit 200 "
);
my $getiid = $dbh->prepare(
	"select iid from images where image=?"
);
my $insimg = $dbh->prepare(
	"insert ignore into images (image, path, sha1, size, ignored) ".
	"values (?,?,?,?,?)"
);
my $insimg2file = $dbh->prepare(
	"insert ignore into img2file (fid, iid) ".
	"values (?,?)"
);
$get->execute or die $get->errstr;
my (@values,@newimages,%images);
while (my $row = $get->fetch) {
	my ($fid, $path) = @$row;
	if (open POST, "$ENV{HOME}/dl/$path") {
		# print "fid $fid extracting from $path\n";
		my @lines;
		while (<POST>) {
			push @lines, $_;
		}
		close POST;
		my $data = Extract::images(\@lines);
		$images{$fid} = $data;
		if (scalar @$data) {
			print Dumper($data);
			foreach my $img (@$data) {
				my ($path, $sha1, $size) = &get_path_hash($img);
				unless (defined $sha1) {
					push @newimages, "('$img->{src}','$path',null,null,1)";
				} else {
					push @newimages, "('$img->{src}','$path','$sha1',$size,0)";
				}
			}
		}
 	} else {
		warn "can't open $path: $!";
		next;
	}
	if (scalar @newimages >= 512) {
		$dbh->do("insert ignore into images (image, path, sha1, size, ignored) values ".(join ",", @newimages))
			or die $dbh->errstr;
		@newimages = ();
	}
}
$get->finish;
if (scalar @newimages) {
	$dbh->do("insert ignore into images (image, path, sha1, size, ignored) values ".(join ",", @newimages))
		or die $dbh->errstr;
}

foreach my $fid (sort keys %images) {
	# print "fid $fid\n";
	foreach my $img (@{$images{$fid}}) {
		$getiid->execute($img->{src}) or die $getiid->errstr;
		my $iid;
		if ($getiid->rows) {
			my $row = $getiid->fetch;
			$iid = $row->[0];
		} else {
			my ($path, $sha1) = &get_path_hash($img);
			$insimg->execute($img->{src}, $path, $sha1) or die $insimg->errstr;
			my $row = $dbh->selectrow_arrayref("select last_insert_id()");
			$iid = $row->[0];
		}
		$getiid->finish;
		push @values, "($fid, $iid)";
	}
	if (scalar @values >= 4096) {
		# print Dumper(\@values);
		$dbh->do("insert ignore into img2file (fid, iid) values ".(join ",", @values)) 
			or die $dbh->errstr;
		@values = ();
	}
}
if (scalar @values) {
	# print Dumper(\@values);
	$dbh->do("insert ignore into img2file (fid, iid) values ".(join ",", @values)) 
		or die $dbh->errstr;
}

sub get_path_hash {
	my ($img) = @_;
	(my $path = $img->{src}) =~ s#^https?://##;
	my ($sha1,$size);
	if (-f "$canonical/$path") {
		if (open IMG, "$canonical/$path") {
			binmode IMG;
			local $/; $/ = undef;
			my $idata = <IMG>;
			die "missing data for $path!" if (length $idata) != ((stat($path))[7]);
			close IMG;
			$sha1 = sha1_hex($idata);
			$size = length($idata);
		}
		print "hash '$sha1' ($size) for $path\n";
	}
	("$dir/$path", $sha1, $size);
}

