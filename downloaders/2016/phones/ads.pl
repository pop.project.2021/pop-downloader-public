#!/usr/bin/perl
our ($dir, $canonical, $section);
BEGIN {
	$section = $dir = $ARGV[0];
	die "need a directory! (did you add a / at the end?)" unless $dir =~ /^\w+$/;
	$canonical = "$ENV{HOME}/dl/$dir";
	die "$canonical does not exist!" unless -d $canonical;
	chdir $canonical or die "can't change to $canonical!";
}

use lib ( $canonical, "$ENV{HOME}/dl/phones" );
use PPDB;
use Extract;
use Data::Dumper;
use strict;
 
my $dbh = PPDB::conn();
exit 1 unless defined $dbh;
my $get = $dbh->prepare(
	"select fid, path from files ".
	"where path like '$dir/%' ".
	"and ignored=0 and fid not in (select fid from ads)"
);
my $getad = $dbh->prepare(
	"select fid,postingid from ads where postingid=?"
);
my $ins = $dbh->prepare(
	"insert into ads (fid,section,postingdate,title,postingid,post,age,ethnicity,city,phone,email) ".
	"values (?,?,?,?,?,?,?,?,?,?,?)"
);
my $insstub = $dbh->prepare(
	"insert into ads (fid,section,postingid,post) ".
	"values (?,?,?,?)"
);
my $ignore = $dbh->prepare(
	"update files set ignored=? where fid=?"
);
$get->execute or die $get->errstr;
while (my $row = $get->fetch) {
	my ($fid, $path) = @$row;
	if (open POST, "$ENV{HOME}/dl/$path") {
		# print "fid $fid extracting from $path\n";
		my @lines;
		while (<POST>) {
			push @lines, $_;
		}
		close POST;
		my $data = Extract::scrape(\@lines, $path);
		if ($data->{post} =~ /\w/) {
			$data->{fid} = $fid;
			$data->{section} = $section;
			print Dumper($data);
			if ($data->{_op_} eq 'update') {
				warn "need a postingid!" and next unless $data->{postingid} =~ /^\d+$/;
				$getad->execute($data->{postingid}) or warn $getad->errstr and next;
				unless ($getad->rows) {
					$insstub->execute(@$data{qw/fid section postingid post/}) 
						or die $insstub->errstr;
				}
				foreach (qw/postingdate title post age ethnicity city phone email/) {
					next unless length $data->{$_};
					my $val = $dbh->quote($data->{$_});
					$dbh->do("update ads set $_=$val where postingid=$data->{postingid}")
						or warn $dbh->errstr and last;
				}
				# indicate that this fid has been seen
				$ignore->execute(3,$fid) or warn $ignore->errstr;
			} elsif ($data->{_op_} eq 'ignore') {
				next;
			} else {
				$ins->execute(@$data{qw/
						fid section postingdate title postingid post age 
						ethnicity city phone email
					/}) 
					or die $ins->errstr;
			}
		} else {
			print "no data\n";
			$ignore->execute(2,$fid) or warn $ignore->errstr;
		}
 	} else {
		warn "can't open $path: $!";
		$ignore->execute(1,$fid) or warn $ignore->errstr;
		next;
	}
}
$get->finish;

