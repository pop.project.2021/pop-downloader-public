#!/usr/bin/perl
use strict;
use lib ( "$ENV{HOME}/dl/phones" );
use PPDB;

=filter out
for the view parameter:
the first number is the location, 
the second is the category and 
the third is the page#
everything else is ignored
using the host is not necessary
=cut

# numbers are count of ads as of 2015-03-10
my @locord = (
	'montreal', # 60167
	'montreal-rive-nord', # 11055
	'montreal-rive-sud', # 6049
	'quebec', # 1862
	'terrebonne', # 932
	'quebec-rive-sud', # 760
	'ottawa', # 494
	'gatineau-hull', # 373
	'centre-du-quebec', # 314
	'estrie', # 195
	'saguenay-lac-st-jean', # 150
	'toronto', # 146
	'moncton', # 91
	'calgary', # 81
	'edmonton', # 79
	'vancouver', # 33
	'hamilton', # 32
	'london', # 23
	'saskatoon', # 20
	'halifax', # 18
	'fredericton', # 16
	'winnipeg', # 16
	'windsor', # 14
	'victoria', # 14
	'saint-john', # 14
	'regina', # 13
);

my %locnum = (
	'gatineau-hull' => 1,
	'montreal-rive-nord' => 2,
	'quebec-rive-sud' => 3,
	'montreal-rive-sud' => 4,
	'montreal' => 5,
	'quebec' => 6,
	'terrebonne' => 7,
	'estrie' => 8,
	'centre-du-quebec' => 10,
	'windsor' => 20,
	'ottawa' => 21,
	'toronto' => 22,
	'edmonton' => 23,
	'winnipeg' => 24,
	'saskatoon' => 25,
	'moncton' => 26,
	'fredericton' => 27,
	'regina' => 28,
	'hamilton' => 29,
	'vancouver' => 30,
	'calgary' => 31,
	'saint-john' => 32,
	'halifax' => 33,
	'victoria' => 34,
	'london' => 35,
	'saguenay-lac-st-jean' => 36,
);

my %locations = (
	1 => 'gatineau-hull',
	2 => 'montreal-rive-nord',
	3 => 'quebec-rive-sud',
	4 => 'montreal-rive-sud',
	5 => 'montreal',
	6 => 'quebec',
	7 => 'terrebonne',
	8 => 'estrie',
	10 => 'centre-du-quebec',
	20 => 'windsor',
	21 => 'ottawa',
	22 => 'toronto',
	23 => 'edmonton',
	24 => 'winnipeg',
	25 => 'saskatoon',
	26 => 'moncton',
	27 => 'fredericton',
	28 => 'regina',
	29 => 'hamilton',
	30 => 'vancouver',
	31 => 'calgary',
	32 => 'saint-john',
	33 => 'halifax',
	34 => 'victoria',
	35 => 'london',
	36 => 'saguenay-lac-st-jean',
);

my %categories = (
	1 => 'female-escorts',
	2 => 'fetish-domination',
	3 => 'erotic-massage',
	6 => 'erotic-model',
	13 => 'male-escorts',
	15 => 'massage-by-men',
	14 => 'travesti-transexual',
);

# timeout to wait in minutes
# this doesn't work because the time reported by stat is off by 15-20 min (?)
my %timeout = (
	5 => { 1 => 10, 3 => 10 },
	2 => { 1 => 20, 3 => 20 },
	4 => { 1 => 20, 3 => 20 },
);

# how many pages to get based on city and category
# this doesn't work out of the box as we keep getting the first page
# requires cookies
my %pages = (
	5 => { 1 => 20, 3 => 20 },
	2 => { 1 => 10, 3 => 10 },
	4 => { 1 => 6, 3 => 6 },
);

# for wget - impossible to switch pages w/o this
my $cookieopts = "--keep-session-cookies --load-cookies=cookies --save-cookies=cookies";
my $uagentopts = "--user-agent='Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:35.0) Gecko/20100101 Firefox/35.0'";
my $opts = "$cookieopts $uagentopts";
system "/usr/bin/wget $opts 'http://www.site4.com/confirm18.php?accept=y'";

foreach my $loc (@locord) {
	my $locn = $locnum{$loc};
	my $dir = "$loc.site4.com";
	my $url = "http://$dir";
	foreach my $catn (sort {$a <=> $b} keys %categories) {
		my $cat = $categories{$catn};
		my $pages = 1;
		$pages = $pages{$locn}{$catn} if defined $pages{$locn}{$catn};
		my $retrieved = 0;
		foreach my $pagen (1..$pages) {
			# sleep 1;
			my $pfile = "$loc-$cat-$pagen.html";
			my $purl = "$url/list.php?view=$locn-$catn-$pagen-$loc-$cat";
			system "/usr/bin/wget $opts -O $pfile '$purl'" and next;
			open PFILE, "< $pfile" or warn "can't open $pfile: $!" and next;
			$retrieved = 0;
			while (<PFILE>) {
				while ($_ =~ m#href="/?(detail\.php?[^"]*)#ig) {
					my $dfile = "$dir/$1";
					my $durl = "$url/$1";
					next if -f $dfile or PPDB::seen("site4/$dfile");
					sleep 1;
					system "/usr/bin/wget -nv -p -H -nc -x $opts '$durl'";
					PPDB::insfile("site4/$dfile");
					$retrieved++;
				}
			}
			close PFILE;
			last if $retrieved == 0;
		}
	}
}

