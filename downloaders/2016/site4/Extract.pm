package Extract;
# this is only for site4
use strict;
use JSON;
our $posts;

sub scrape {
	my ($lines, $path) = @_;
	my $title;
	my $post;
	my $postingid;
	my $postingdate;
	my $age;
	if ($path =~ m#.*\?view=(\d+)#) {
		$postingid = $1;
	}
	my $isdate = 0;
	foreach (@$lines) {
		if (m#<meta name="description"[^>]*?content="([^"]*)"[^>]*/?>#) {
			$post = $1;
		} elsif (m#<meta name="description"#i...m#/?>#) {
			if (m#content="(.*)# and !defined $post) {
				$post .= $1;
			} else { 
				unless (m#^\s*$#) {
					$post .= $_;
				}
			}
		} elsif ($title !~ /\w/ and m{<div class="titlelisttop"[^>]*>([^<]*)(?:<|$)}) {
			$title = $1;
		} elsif (m{<span style=".*?">Date posted </span>}) {
			$isdate = 1;
			next;
		} elsif (m{<span style=".*?">\s*(\d\d\d\d-\d\d-\d\d)}) {
			$isdate = 0;
			$postingdate = $1;
		}
	}
	&trim($title);
	&trim($post);
	$post =~ s/&(?:#\d+|[a-zA-Z]+);/ /g;
	return { title=>$title, postingdate=>$postingdate, post=>$post,
		 postingid=>$postingid , _op_=>'insert'};
}

sub images {
	my ($lines) = @_;
	my @images;
	my %seen;
	foreach (@$lines) {
		if (m#<div [^>]*backgroundImage='url\(([^\)]*)\)'#) {
			next if $seen{$1};
			$seen{$1}++;
			push @images, {src=>$1};
		} elsif (m#var aImage = (\[[^\]]*\]);#) {
			my $jstr = $1;
			$jstr =~ s/'/"/g;
			eval {
				my $jimages = from_json($jstr);
				foreach my $img (@$jimages) {
					push @images, {src=>$img};
				}
			};
			warn $@ if $@;
		}
	}
	\@images;
}
sub trim {
	chomp $_[0];
	$_[0] =~ s/\r//gs;
	$_[0] =~ s/^\s*//s;
	$_[0] =~ s/\s*(?:"\s*\/?>\s*|)$//s;
}

1;
