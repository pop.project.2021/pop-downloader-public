#!/bin/sh
cd $HOME/dl/misc

date > lastrun.txt
find > snapshot.txt

perl -pe 's/.*?, //' site5-schedules.txt | wget -x -r -l1 -N --no-check-certificate -i -

# in relative order of flakiness
wget -r -l1 -E -N http://www.site6.com/view/View-Current-Ads.html
wget -T1 -nc -r -l2 -E --max-redirect=0 -D www.site5.com http://www.site5.com/classifieds/

# since we have to overwrite files we've already seen keep a snapshot of what was found
find -type f -exec ./printnew.pl '{}' snapshot.txt \; > snapshots/`date +%Y%m%d-%H%M%S`.txt

# archive of commands to get archive or sitemaps
wget -O site5.html http://www.site5.com/vbulletin/archive/index.php


