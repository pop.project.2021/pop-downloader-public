#!/usr/bin/perl
# try and figure out the language of a post
use Lingua::Identify qw(:language_identification);
use PPDB;
use Getopt::Std;
use strict;
my %opt;
getopts('v',\%opt);
my $dbh = PPDB::conn();
my $get = $dbh->prepare("select fid,title,post,locale from ads where lang is null");
my $upd = $dbh->prepare("update ads set lang=? where fid=?");
$get->execute or die $get->errstr;
my ($total,$updated,$wrong);
my $lopts = {prefixes2 => 5, suffixes3 => 2, ngrams3 => 10};
while (my $row = $get->fetch) {
	my ($fid,$title,$post,$locale) = @$row;
	my $all = $title."\n".$post;
	$all =~ s/<[^>]*>/ /g;
	$all =~ s/&\w+\;/ /g;
	$all =~ s/[\`\~!@#\$\%^\&\*\(\)\[\]\{\}\|\\\:\;\"\'\,\.\/<>\?_\-]/ /g;
	$all =~ s/ +/ /g;
	$all = lc($all);
	my $lang = langof($lopts, $all);
	my %lang = langof($lopts, $all);
	$total++;
	print "$total: $lang at locale $locale '$title'\n" if $opt{v};
	my $err = $lang !~ /en|fr/ ? 1 : 0;
	if ($err) {
		$wrong++;
		print "en $lang{en}\nfr $lang{fr}\n$post\n-------------------\n" if $opt{v};
	}
	$upd->execute("",$fid) and next unless $lang =~ /en|fr/;
	$updated++;
	print "updating\n" if $opt{v};
	$upd->execute((sprintf "%s %.04f",$lang,$lang{$lang}),$fid);
}
$get->finish;
if ($total > 0) {
	printf "found lang for %.02f %% ads (%d/%d) wrong %.02f (%d/%d)\n", 
		100*$updated/$total,$updated,$total,
		100*$wrong/$total,$wrong,$total;
} else {
	print "no ads missing lang found\n";
}

