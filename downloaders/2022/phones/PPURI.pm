package PPURI;
# gather info from ad urls

# list of location identifying url parts from PPAC
our %locales = (
	'backpage' => {
		'alberta.backpage.com' => 'AB',
		'britishcolumbia.backpage.com' => 'BC',
		'halifax.backpage.com' => 'NS',
		'newbrunswick.backpage.com' => 'NB',
		'ontario.backpage.com' => 'ON',
		'quebec.backpage.com' => 'QC',
		'saskatchewan.backpage.com' => 'SK',
		'stjohns.backpage.com' => 'NB',
		'whitehorse.backpage.com' => 'YT',
		'winnipeg.backpage.com' => 'MB',
		'yellowknife.backpage.com' => 'YT',
	},
	'craigslist' => {
		'abbotsford.craigslist.ca' => 'BC',
		'barrie.craigslist.ca' => 'ON',
		'belleville.craigslist.ca' => 'ON',
		'brantford.craigslist.ca' => 'ON',
		'calgary.craigslist.ca' => 'AB',
		'cariboo.craigslist.ca' => 'BC',
		'chatham.craigslist.ca' => 'ON',
		'comoxvalley.craigslist.ca' => 'BC',
		'cornwall.craigslist.ca' => 'ON',
		'cranbrook.craigslist.ca' => 'BC',
		'edmonton.craigslist.ca' => 'AB',
		'ftmcmurray.craigslist.ca' => 'AB',
		'guelph.craigslist.ca' => 'ON',
		'halifax.craigslist.ca' => 'NS',
		'hamilton.craigslist.ca' => 'ON',
		'hat.craigslist.ca' => 'AB',
		'kamloops.craigslist.ca' => 'BC',
		'kelowna.craigslist.ca' => 'BC',
		'kingston.craigslist.ca' => 'ON',
		'kitchener.craigslist.ca' => 'ON',
		'lethbridge.craigslist.ca' => 'ON',
		'londonon.craigslist.ca' => 'ON',
		'montreal.craigslist.ca' => 'QC',
		'nanaimo.craigslist.ca' => 'BC',
		'newbrunswick.craigslist.ca' => 'NB',
		'newfoundland.craigslist.ca' => 'NF',
		'niagara.craigslist.ca' => 'ON',
		'ottawa.craigslist.ca' => 'ON|QC',
		'owensound.craigslist.ca' => 'ON',
		'peace.craigslist.ca' => 'AB',
		'pei.craigslist.ca' => 'PE',
		'peterborough.craigslist.ca' => 'ON',
		'princegeorge.craigslist.ca' => 'BC',
		'quebec.craigslist.ca' => 'QC',
		'reddeer.craigslist.ca' => 'AB',
		'regina.craigslist.ca' => 'SK',
		'saguenay.craigslist.ca' => 'QC',
		'sarnia.craigslist.ca' => 'ON',
		'saskatoon.craigslist.ca' => 'AB',
		'sherbrooke.craigslist.ca' => 'QC',
		'skeena.craigslist.ca' => 'BC',
		'soo.craigslist.ca' => 'ON',
		'sudbury.craigslist.ca' => 'ON',
		'sunshine.craigslist.ca' => 'BC',
		'territories.craigslist.ca' => 'YT',
		'thunderbay.craigslist.ca' => 'ON',
		'toronto.craigslist.ca' => 'ON',
		'troisrivieres.craigslist.ca' => 'QC',
		'vancouver.craigslist.ca' => 'BC',
		'victoria.craigslist.ca' => 'BC',
		'whistler.craigslist.ca' => 'BC',
		'whitehorse.craigslist.ca' => 'YT',
		'windsor.craigslist.ca' => 'ON',
		'winnipeg.craigslist.ca' => 'MB',
		'yellowknife.craigslist.ca' => 'YT',
	},
	'maryly' => {
		'calgary.maryly.com' => 'AB',
		'centre-du-quebec.maryly.com' => 'QC',
		'edmonton.maryly.com' => 'AB',
		'estrie.maryly.com' => 'QC',
		'fredericton.maryly.com' => 'NB',
		'gatineau-hull.maryly.com' => 'QC',
		'halifax.maryly.com' => 'NS',
		'hamilton.maryly.com' => 'ON',
		'london.maryly.com' => 'ON',
		'moncton.maryly.com' => 'NB',
		'montreal.maryly.com' => 'QC',
		'montreal-rive-nord.maryly.com' => 'QC',
		'montreal-rive-sud.maryly.com' => 'QC',
		'ottawa.maryly.com' => 'ON|QC',
		'quebec.maryly.com' => 'QC',
		'quebec-rive-sud.maryly.com' => 'QC',
		'regina.maryly.com' => 'SK',
		'saguenay-lac-st-jean.maryly.com' => 'QC',
		'saint-john.maryly.com' => 'NB',
		'saskatoon.maryly.com' => 'SK',
		'terrebonne.maryly.com' => 'QC',
		'toronto.maryly.com' => 'ON',
		'vancouver.maryly.com' => 'BC',
		'victoria.maryly.com' => 'BC',
		'windsor.maryly.com' => 'ON',
		'winnipeg.maryly.com' => 'MB',
	},
	'site3' => {
		'calgary' => 'AB',
		'central-ontario' => 'ON',
		'edmonton' => 'AB',
		'greater-toronto' => 'ON',
		'hamilton-niagara' => 'ON',
		'interior' => 'BC',
		'metro-vancouver' => 'BC',
		'n_alberta' => 'AB',
		'new-brunswick' => 'NB',
		'newfoundland' => 'NF',
		'northern-bc' => 'BC',
		'northern-ontario' => 'ON',
		'nova-scotia' => 'NS',
		'nw_territories' => 'YT',
		'quebec' => 'QC',
		'regina' => 'SK',
		'saskatoon' => 'SK',
		'south-eastern-ontario' => 'ON',
		'south-western-ontario' => 'ON',
		'vancouver-island' => 'BC',
		'winnipeg' => 'MB',
		'yukon' => 'YT',
	},
);

our $catpat = qr/-(female-escorts|fetish-domination|erotic-massage|erotic-model|male-escorts|massage-by-men|travesti-transexual)-/;

# PROVINCE,LOCATION,CATEGORY_PARENT,CATEGORY_CHILD
sub parse {
	my ($path) = @_;
	my @parts = split '/', $path;
	my %parse;
	if ($parts[0] eq 'backpage') {
		%parse = (
			prov => $locales{$parts[0]}{$parts[1]},
			location => ($parts[1]=~/^([\w\-]+)\./)[0],
			category_parent => 'adult entertainment',
			category_child => lc $parts[2],
		);

	} elsif ($parts[0] eq 'craigslist') {
		%parse = (
			prov => $locales{$parts[0]}{$parts[1]},
			location => ($parts[1]=~/^([\w\-]+)\./)[0],
			category_parent => 'services',
			category_child => 'therapeutic',
		);
		
	} elsif ($parts[0] eq 'site3') {
		} elsif ($parts[1] eq 'www.site3.com' or $parts[1] eq 'www.site3.cc') {
			%parse = (
				prov => $locales{$parts[0]}{$parts[4]},
				location => $parts[4],
				category_parent => $parts[2],
				category_child => $parts[3],
			);
		}

	} elsif ($parts[0] eq 'maryly') {
		my $city = ($parts[1]=~/^([\w\-]+)\./)[0];
		my ($category) = ($parts[2]=~/$catpat/);
		%parse = (
			prov => $locales{$parts[0]}{$parts[1]},
			location => $city,
			category_parent => 'adult entertainment',
			category_child => $category,
		);
	}
	\%parse;
}

1;

