package PPExtract;
# based on the Ers.pm module in this directory
# use this as a last resort as it tends to find too many phone numbers
use strict;
use lib ("$ENV{HOME}/dl/phones");
use PPAC;

our $spampat = qr{
\b(?:web)cam\b|
\bm[\W_]*y[\W_]*r[\W_]*e[\W_]*d[\W_]*p[\W_]*a[\W_]*g[\W_]*e[\W_]*s[\W_]*|
\bm\W*r\W*p\W*|
<font [^>]*><small>|
<font [^>]*size=(?:"|)[01](?:"|) [^>]*>|
<font color="ivory"|
font-size:\s*[012](?:px|%)|
lonelyjasmin|
angel\W*review|
playangels|
meetsomeoneforsex|
jamsite|
9p0\.net|
adultflirtonline|
absoluteamateurgirls|
asymmetricalmedia|
chatmasters|
flingnsa|
flirtpicsonline|
flirtyimages|
geocities|
hosturfiles|
hundredk|
maturelustnow|
meetflingdate|
online-flirtdates|
profiles\.yahoo|
purensa|
radioinsert|
ratemysexyflirt|
searchflingdate|
sfducktours|
sftourreviews|
sweetpicsonline|
thekyj|
(?:Yahoo|AIM|ICQ|MSN\/WL):\s*\w+|
Town.*?Friends.*?com|
WWW.*?weekend.*?friends.*?COM|
Holiday.*?wives.*?com|
hotgirlangelina|
kinkykelly778|
camaholic\.net|
camsprite\.com|
cerf\.ca|
fundaters\.com|
myglamgirls\.com
}ix;
# note that the order of the patterns being matched in the "or" affects what gets matched even if match is shorter
our $acronympat = qr#[^a-zA-Z0-9]{0,3}(\d{3}|[a-zA-Z]{3})[^a-zA-Z0-9]{0,3}(\d{4}|[a-zA-Z]{4})\b#;
our $numpat = qr/
(fourteen|sixteen|seventeen|eighteen|nine?teen|
good|honest|oo*hh*|zero|owe|one|won|on|two|too*|three*?|fourty|forty|fou*r|five|
six(?:ty|)|seven(?:ty|)|sev|eight(?:ty|)|eihgt|eieght|ate|nine(?:ty|)|
ten|eleven|twelve|thirteen|fifteen|twenty|thirty|fifty)
/x;
our $tenstr = "twenty|thirty|forty|fifty|sixty|seventy|eighty|ninety";
our $tenpat = qr/($tenstr)/;
our $digitpat = qr/($tenstr|[1-9]|one|two|thre*|fou*r|five|six|se*ve*n|eight|nine|)/;
our %acronymmap = (
	a => 2,
	b => 2,
	c => 2,
	d => 3,
	e => 3,
	f => 3,
	g => 4,
	h => 4,
	i => 4,
	j => 5,
	k => 5,
	l => 5,
	m => 6, 
	n => 6,
	o => 6,
	p => 7,
	q => 7,
	r => 7,
	s => 7,
	t => 8,
	u => 8,
	v => 8,
	w => 9,
	x => 9,
	y => 9,
	z => 9,
);

our %nummap = (
	'honest' => '',
	'good' => '',
	'oh' => 0,
	'ohh' => 0,
	'ooh' => 0,
	'zero' => 0,
	'owe' => 0,
	'one' => 1,
	'on' => 1,
	'won' => 1,
	'two' => 2,
	'too' => 2,
	'tooo' => 2,
	'to' => 2,
	'thre' => 3,
	'three' => 3,
	'threee' => 3,
	'four' => 4,
	'for' => 4,
	'five' => 5,
	'six' => 6,
	'seven' => 7,
	'sven' => 7,
	'sevn' => 7,
	'svn' => 7,
	'sev' => 7,
	'eight' => 8,
	'eieght' => 8,
	'eihgt' => 8,
	'ate' => 8,
	'nine' => 9,
	'ten' => 10,
	'eleven' => 11,
	'twelve' => 12,
	'thirteen' => 13,
	'fourteen' => 14,
	'fifteen' => 15,
	'sixteen' => 16,
	'seventeen' => 17,
	'eighteen' => 18,
	'ninteen' => 19,
	'nineteen' => 19,
	'twenty' => 20,
	'thirty' => 30,
	'forty' => 40,
	'fourty' => 40,
	'fifty' => 50,
	'sixty' => 60,
	'seventy' => 70,
	'eighty' => 80,
	'eihgty' => 8,
	'ninety' => 90,
);
our %tenmap = (
	'twenty' => 2,
	'thirty' => 3,
	'forty' => 4,
	'fifty' => 5,
	'sixty' => 6,
	'seventy' => 7,
	'eighty' => 8,
	'eihgty' => 8,
	'ninety' => 9,
);

sub new {
	my $class = shift;
	my $extracted = {@_};
	bless $extracted, $class;
	$extracted->{phones} = {};
	$extracted;
}

# now process $content and look for phone numbers etc
sub process {
	my ($extracted) = @_;
	my $content = $extracted->{content};
	$content .= ' ';
	$content = lc($content);
	$content =~ s/\r//g;
	$content =~ s/\s/ /sg;
	$content =~ s/#\d+;//g;
	my $spam = 1 if $content =~ m/(?:$spampat)/; 
	(my $spamcheck = $content) =~ s#<[^>]*>+##sg;
	$spam = 1 if $spamcheck =~ m/(?:$spampat)/; 
	$content =~ s/<[^>]*>+/ /g;
	$content =~ s/&\w+;/ /g;
	$spam = 1 if $content =~ m/(?:$spampat)/;
	$content =~ s/(?:star|\*)\s*(?:eight|8)\s*(?:two|2)\b//gi;
	$content =~ s/[456]\s*(?:foot|feet)\s*\d{1,2}\s*(?:inch|"|)\s*(?:tall|)/DELETED/;
	$content =~ s/[234]\d\s*[a-f]+[\W\s]*[234]\d[\W\s]*[234]\d/DELETED/;
	$content =~ s/[abcde]+\s*[234]\d\s*[\W\s]*[234]\d[\W\s]*[234]\d/DELETED/;
	$content =~ s/[abcde]*\s*[234]\d\s*[a-f]*(?:"|)\s*(?:waist|bust|chest|breast|hips)/DELETED/g;
	$content =~ s/\b(?:[12]\d\d|9\d)\s*[il]b/DELETED/;
	$content =~ s/[\W_]/ /g;
	$content =~ s/  +/ /g;
	my $testcontent = $content; # see below for where this is used
	$content =~ s/\W//g;
	$content =~ s/$tenpat\s*$digitpat/exists $tenmap{$2} ? $nummap{$1}.$nummap{$2} : $tenmap{$1}.($2 || '0')/eg;
	$content =~ s/$numpat/$nummap{$1}/eg;
	$content =~ s/(\d) /$1/g;
	$content =~ s/double\s*(\d)/$1$1/g;
	$content =~ s/triple\s*(\d)/$1$1$1/g;
	$content =~ s/thousand/000/gi;
	# test without converting o to 0
	# grabacronyms using letters instead of numbers not so common now
	# &grabacronyms($extracted,$content) unless $extracted->hasphone;
	&grabphones($extracted,$content) unless $extracted->hasphone;
	# test again with simpler mapping
	unless ($extracted->hasphone) {
		$testcontent =~ s/$tenpat/$tenmap{$1}0/g;
		$testcontent =~ s/$numpat/$nummap{$1}/eg;
		$testcontent =~ s/(\d) /$1/g;
		$testcontent =~ s/double\s*(\d)/$1$1/g;
		$testcontent =~ s/triple\s*(\d)/$1$1$1/g;
		$testcontent =~ s/thousand/000/gi;
		# &grabacronyms($extracted,$testcontent);
		&grabphones($extracted,$testcontent) unless $extracted->hasphone;
	}
	$content =~ s/o/0/g;
	$content =~ s/[il]/1/g;
	$content =~ s/(\d)\D+(\d)/$1$2/g;
	&grabphones($extracted,$content) unless $extracted->hasphone;
	$extracted->{phones}->{spam}++ if $spam and !$extracted->hasphone;
	my $phonelist = join ",",sort keys %{$extracted->{phones}};
	@{$extracted}{qw/processed_post spam phonelist/} = ($content,$spam,$phonelist);
	$extracted;
}

sub set {
	my ($extracted, $name, $value) = @_;
	$extracted->{$name} = $value;
	$extracted;
}

sub tostring {
	my ($extracted,$processed_post) = @_;
	my $sep = "\n";
	my $phones = join ",",sort keys %{$extracted->{phones}};
	my @string = ( 
		"spam: ".($extracted->{spam} ? "yes":"no"),
		"phone(s): $phones",
		"content: $extracted->{content}",
		"processed_post: ".($extracted->{processed_post}||$processed_post),
	);
	join "$sep", @string,$sep;
}

sub hasphone {
	my $extracted = shift;
	# return 1 if scalar(%{$extracted->{phones}});
	0;
}

sub grabacronyms {
	my ($extracted,$firsttest) = @_;
	foreach (@PPAC::keys) {
		my $pat = qr/($_$acronympat)/;
		while ($firsttest =~ m#$pat#g) {
			my @bits = ($2,$3);
			my $pn;
			foreach (@bits) {	
				if (/[a-z]+/) {
					foreach (split '', $_) {
						$pn .= $acronymmap{$_};
					}
				} else {
					$pn .= $_;
				}
			}
			&addphone($extracted,"$_$pn");
		}
	}
}

sub grabphones {
	my ($extracted,$firsttest,$pattern) = @_;
	# using 10 characters here may not be optimal
	$firsttest =~ s/(\d)\D{0,10}/$1/g;
	my @as = @PPAC::keys;
	foreach (@as) {
		# clean up 604604 type things to avoid some probably invalid phone numbers
		(my $ft = $firsttest) =~ s/$_$_/$_/g;
		my $pat = defined $pattern ? $pattern : qr/$_[2-9]\d{6}/;
		while ($ft =~ m#($pat)#g) {
			&addphone($extracted,$1);
		}
	}
}

sub addphone {
	my ($extracted,$num) = @_;
	my $phone;
	my @as = @PPAC::keys;
	my $acodepat = join '|', @as;
	$acodepat .= '[2-9]\d{6}';
	return unless $num =~ /$acodepat/;
	if (length($num) == 10) {
		$phone = $num;
	}
	if ($phone) {
		$extracted->{phones}->{$phone}++;
		return;
	}
	foreach (@as) {
		my $pat = $_.'[2-9]\d{6}';
		while ($num =~ /($pat)/g) {
			$phone = $1;
			$extracted->{phones}->{$phone}++;
		}
	}
	$extracted;
}

1;

