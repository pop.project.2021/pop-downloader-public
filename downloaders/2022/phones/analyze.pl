#!/usr/bin/perl
our ($dir, $canonical, $section, %opt);
BEGIN {
	use Getopt::Std;
	getopts('ui',\%opt);
    print "db $ENV{PPDB}";
	$opt{i} = ($opt{u} ? 0 : 1) unless $opt{i};
	$section = $dir = $ARGV[0];
	die "need a directory! (did you add a / at the end?)" unless $dir =~ m#^[\w/]+$#;
	$canonical = "$ENV{HOME}/dl/$dir";
	die "$canonical does not exist!" unless -d $canonical;
	chdir $canonical or die "can't change to $canonical!";
}

use lib ( $canonical, "$ENV{HOME}/dl/phones" );
use PPDB;
use PPID;
use PPIMG;
use Extract;
use Data::Dumper;
$Data::Dumper::Sortkeys = 1;
$Data::Dumper::Indent = 1;
use strict;
 
my $dbh = PPDB::conn();
exit 1 unless defined $dbh;
my $getquery = 
	"select fid, path, basedir, ignored from files ".
    "where path like '$dir/%' ".
    "and ignored=-1 or (ignored=0 and fid not in (select fid from ads)) ".
	"order by fid";
print "GET $getquery\n";
my $get = $dbh->prepare(
    $getquery
);

$get->execute or die $get->errstr;
while (my $row = $get->fetch) {
	my ($fid, $path, $basedir, $ignored) = @$row;
	my $fullpath = PPDB::fullpath($basedir,$path);
    print "checking $ENV{PPDB} $fullpath\n";
    unless (-f $fullpath) {
        if (-f "$fullpath.gz") {
            print "unzipping $fullpath.gz\n";
            system("gunzip","$fullpath.gz") and die "can't unzip file!";
        }
    }
	if (open POST, $fullpath) {
		print "fid $ENV{PPDB} $fid extracting from $path\n";
		my @lines;
		while (<POST>) {
			push @lines, $_;
		}
		close POST;
        PPID::updatedb(\@lines, $path, $ignored, \%opt);
		# find images in posts
		PPIMG::save($section, $fid, $dbh, \@lines, $basedir);
 	} else {
		warn "can't open $path: $!";
		if ($opt{i}) { PPDB::ignore(1,$fid); }
		next;
	}
}
$get->finish;

