package Extract;
# this is only for erslist / site3
use strict;
use URL::Encode qw/url_encode url_decode/;
use Postedon;
our $posts;

sub readpath {
    my ($path) = @_;
    open IN, "< $path" or die "$!";
    my @lines;
    while (my $l = <IN>) {
        push @lines, $l;
    }
    close IN;
    return(scrape(\@lines,$path));
}

sub dump {
    my ($path) = @_;
    use Data::Dumper;
    $Data::Dumper::Indent = $Data::Dumper::Sortkeys = 1;
    print Dumper(readpath($path));
}

sub scrape {
	my ($lines, $path) = @_;
	my ($name, $chatid, $chatname,$chatregion,$price,$avail,$views,$postedon,
        $title, $post, $postingid, $postingdate, $age, $ethnicity, $city, $phone, $email, 
        $canonical, $canonicalpath, $whatsapp, $url, $hasurl,
        %get);
	my @mt = ();
    if (-f "$ENV{HOME}/dl/$path") {
       @mt = localtime((stat("$ENV{HOME}/dl/$path"))[9]);
    }
	if ($path =~ m#.*?(\d+)$#) {
		$postingid = $1;
	}
    my $notactive = 0;
	foreach (@$lines) {
        if (/Sorry, this ad is no longer active|This ad does not exist|<h2 class="mbn">Error</) {
            $notactive = 1;
            last;
        }
        if ($get{ethnicity}) {
            $get{ethnicity} = 0;
            if (m#^\s*(.*\S)\s*</div>#) {
                $ethnicity = $1;
            }
        }
        if ($get{avail}) {
            $get{avail} = 0;
            if (m#^\s*(.*\S)\s*</div>#) {
                my @avail = split /\s*\&\s*/, $1;
                my %avail; @avail{@avail} = 1;
                $avail = join ",", (sort keys %avail);
            }
        }
        if (m#id="preview-name"[^>]*>([^<]*)#) {
            $name = $1;
        } 
        if (m#id="preview-age"[^>]*>(\d+)#) {
            $age = $1;
        } 
        if (m#id="preview-availability"#) {
            $get{avail} = 1;
            next;
        } 
        if (m#id="preview-ethnicity"[^>]*>#) {
            $get{ethnicity} = 1;
            next;
        } 
        if (m#<span itemprop="addressLocality">([^<]+?)</span>#) {
            $city = $1;
        } 
        if (m#<meta itemprop="priceRange" content="(\d+)"/>#) {
            $price = $1;
        } 
        if (m#data-chat-id="(\d+)"#) { 
            $chatid = $1;
        } 
        if (m#data-chat-name="([^"]+)"#) { 
            $chatname = $1;
        } 
        if (m#data-region-id="(\d+)"#) {
            $chatregion = $1;
		} 
        # this is mutually exclusive with itemprop="telephone" patterns
        if (m#<a class="phone" href="tel:(\d+)"#) {
            $phone = $1;
        } elsif (!defined $phone and m#itemprop="telephone">.*?(\d[^<>]*)#) {
			$phone = $1;
		} elsif (!defined $phone and m#<meta itemprop="telephone" content="([^"]*-\d\d\d\d)"/>#) {
            # these are now cut off like this -NNNN
            $phone = $1;
        } 
        if (m#<a href="([^"]*)" target="_blank" rel="nofollow" itemprop="url">#) {
            $url = $1;
            $hasurl = 1;
        }
        if (m#"mailto:([^"]*)[^>]*itemprop="email"#) {
			$email = $1;
		} 
        if (m#<h1[^>]*class="head__title"[^>]*>([^<]*)#) {
			$title = $1;
		} if (m#itemprop="description"#..m#</div>#) {
			s/<[^>]*(?:>|$)//g;
			s/&(?:#\d+|\w+);/ /g;
			$post .= $_ if m/\w/;
		} 
        if (m#link rel="canonical" href="([^"]*)#) {
            $canonical = $1;
        }
        if (m#<a class="whatsapp" target="_blank" href="([^"]*)#) {
            $whatsapp = $1;
        }
        if (m#<div class="address__item website">#) {
            $hasurl = 1;
        }
        {
            my $postedondata = Postedon::get($_, $postingdate);
            if (defined $postedondata) {
                $postedon = $postedondata->{postedon};
                $views = $postedondata->{views};
            }
        }
	}
	&trim($title);
	&trim($post);
    if ($notactive) {
        return { post => undef };
    }
    if (scalar @mt == 0 and defined $canonical) {
        ($canonicalpath = $canonical) =~ s#^https://#erslist/pages/#;
        my $fullpath = "$ENV{HOME}/dl/$canonicalpath";
        @mt = localtime((stat($fullpath))[9]) if -f $fullpath;
    }
	my $postingdate = sprintf '%04d-%02d-%02d %02d:%02d:%02d', $mt[5]+1900, $mt[4]+1, @mt[3,2,1,0] 
        if scalar @mt > 0;
	return { title=>$title, post=>$post, age=>$age, city=>$city, 
		 ethnicity=>$ethnicity, postingdate=>$postingdate, phone=>$phone, 
         email=>$email, postingid=>$postingid, name=>$name, availability=>$avail,
         chatid=>$chatid, chatname=>$chatname, chatregion=>$chatregion, price=>$price, 
         views=>$views, postedon=>$postedon, whatsapp=>$whatsapp, url=>$url, hasurl=>$hasurl,
         canonical=>$canonical, canonicalpath=>$canonicalpath, _op_=>'insert'};
}

sub images {
	my ($lines) = @_;
	my @images;
    return \@images;
    # this stuff is happening during the download now ...
	foreach (@$lines) {
		if (m#accountGallery#..m#<script>#) {
			if (m#<img([^>]*)#) {
				my $i = $1;
				my %img;
                # foreach my $t (qw/src alt title/) {
                # strange characters in the alt text may be causing this to fail, also no title afaik
				foreach my $t (qw/src/) {
					if ($i =~ m#$t="([^"]*)#) {
						$img{$t} = $1;
					}
				}
                $img{src} =~ s#/304x304/#/main/#;
				$img{src} =~ s#/ths/#/main/#;
                $img{src} = url_decode($img{src});
				push @images, \%img;
			}
		}
	}
	\@images;
}

sub trim {
	chomp $_[0];
	$_[0] =~ s/\r//gs;
	$_[0] =~ s/^\s*//s;
	$_[0] =~ s/\s*$//s;
}

1;
