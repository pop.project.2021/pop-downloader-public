#!/bin/bash
# run hourly at 59 min after the hour
export PPDB=pp2022
TS=$(/bin/date +%Y%m%d)
cd $(/usr/bin/dirname $(/usr/bin/realpath $0))
echo START $(/bin/date) TS $TS
/usr/bin/pwd
/usr/bin/perl -pe 's#^https://##' ../paths/newlinks.txt | ./files.pl >> logs/files-$TS.log 2>&1
# when the day changes we can miss some downloaded files
/usr/bin/find www.site3.cc -type f -mmin -60 -! -name '*.v' | ./files.pl >> logs/files-$TS.log 2>&1
cat files.mysql | $HOME/bin/pp2022 -vvv >> logs/files-$TS.log 2>&1
echo END $(/bin/date)
