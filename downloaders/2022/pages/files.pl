#!/usr/bin/perl -n
# adds files records for paths found
use PPDB;
use POSIX;
BEGIN {
    %opt;
    use Getopt::Std;
    getopts("v",\%opt);
    $verbose = $opt{v} || 0;
}

$basedir = "erslist/pages";
$file = $_;
chomp $file;
warn "can't find $file" and next unless -f $file;
$mtime = POSIX::strftime("%Y-%m-%d %H:%M:%S",(localtime((stat($file))[9])));
unless (PPDB::seen("$basedir/$file")) { 
    print "inserting $basedir/$file\n";
    PPDB::insfile("$basedir/$file",$mtime);
} else {
    print "updating lastseen for $basedir/$file\n" if $verbose;
    PPDB::lastseen("$basedir/$file");
}

