#!/bin/bash
# run every 15 minutes after ../paths/paths.sh
export PPDB=pp2022
TS=$(/bin/date +%Y%m%d)
echo START $(/bin/date) TS $TS
cd $(/usr/bin/dirname $(/usr/bin/realpath $0))
/usr/bin/pwd
echo getting links $(/bin/date)
/usr/bin/wget -nc -x -i../paths/newlinks.txt >> logs/newlinks-$TS.log 2>&1
# see also files.sh
echo END $(/bin/date)
