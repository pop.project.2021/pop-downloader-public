#!/bin/bash
# run at least once per day to add new files to the ads table
export PPDB=pp2022 
cd $(/usr/bin/dirname $(/usr/bin/realpath $0))
$HOME/dl/phones/analyze.pl erslist/pages
$HOME/dl/phones/words.pl -lall
$HOME/dl/phones/check.pl
$HOME/dl/phones/get_lang.pl
$HOME/dl/phones/ins-id2file-names.pl
$HOME/dl/phones/ins-id-names.pl

