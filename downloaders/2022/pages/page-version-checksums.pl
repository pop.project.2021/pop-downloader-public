#!/usr/bin/perl -n
# reads the contents of a file and creates an update command 
# for the sha256 field in the views table
# this is to see if ad content changes at all for a 
# given ad path   
BEGIN { 
    use Postedon;
    use Extract;
    use Digest::SHA qw/sha256_hex/ 
} 
chomp; 
@l = ();
open IN, "$_" or die "$!: $_"; 
push @l,$l while ($l=<IN>); 
close IN;

$Postedon::verbose=0; 
$d=Extract::scrape(\@l,$_); 
($p,$y,$m,$dy) = (m#(.*)\.(\d\d\d\d)(\d\d)(\d\d)\.v$#); 
$p=$_ unless defined $p; 
printf qq{update views set sha256="%s" where path="erslist/pages/%s" and date="%04d-%02d-%02d";\n},
    sha256_hex($d->{title}.$d->{post}),$p,$y,$m,$dy;
  
