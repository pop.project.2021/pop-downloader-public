#!/bin/bash
# processes images for the last day, depends on images.sh for input file
# run more than once per day
export PPDB=pp2022
TS=$(/bin/date +%Y%m%d)
MYSQL="/usr/bin/mysql -u$PPDBUSER -p$PPDBPW -D$PPDB"
echo START $(/bin/date) TS $TS db $PPDB
#cd $(/usr/bin/dirname $(/usr/bin/realpath $0))
cd $HOME/dl
/usr/bin/pwd

echo update id stats $(/bin/date)
/usr/bin/cat idstats.mysql | $MYSQL -vvv 

echo generate phashes $(/bin/date)
echo "select path from images where phash is null" | $MYSQL | \
    $HOME/pp/tools/simple-phash.py | \
    $MYSQL -vvv

echo test for faces $(/bin/date)
echo "select path from images where faces is null" | $MYSQL | \
    $HOME/pp/tools/simple-facetest.py | \
    $MYSQL -vvv


echo END $(/bin/date) 
