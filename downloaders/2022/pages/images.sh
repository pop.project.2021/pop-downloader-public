#!/bin/bash
# finds images for the last day, downloads them and saves ad associations in db
# run more than once per day
export PPDB=pp2022
TS=$(/bin/date +%Y%m%d)
MYSQL="/usr/bin/mysql -u$PPDBUSER -p$PPDBPW -D$PPDB -vvv"
echo START $(/bin/date) TS $TS db $PPDB
cd $(/usr/bin/dirname $(/usr/bin/realpath $0))
/usr/bin/pwd

echo get image links $(/bin/date)
/usr/bin/find www.site3.cc/ -type f -mtime -1 -! -name '*.v' -exec /usr/bin/grep -Ph '^\s*(href|src)="https://i.site3.cc/(migrated|\d+/\w+)/main' {} \; | \
    /usr/bin/perl -pe 's/\r//g; s/^\s*(?:href|src)="(.*)"/$1/' | /usr/bin/sort | /usr/bin/uniq > logs/images-$TS.txt

echo save image associations in db $(/bin/date)
/usr/bin/find www.site3.cc/ -type f -mtime -1 -! -name '*.v' -exec /usr/bin/grep -PH '^\s*(href|src)="https://i.site3.cc/(migrated|\d+/\w+)/main' {} \; | \
    /usr/bin/perl -pe '
    s#\r##g; s#(.*?):\s*(?:href|src)=(.*)#insert ignore into imgmap (loc,image) values ("erslist/pages/$1",$2);#
    ' | $MYSQL >> logs/images-$TS.mysql.log 2>&1 

echo update image tables in db $(/bin/date)
cat images.mysql | $MYSQL >> logs/images-$TS.mysql.log

echo download image links $(/bin/date)
/usr/bin/wget -nc -x -ilogs/images-$TS.txt >> logs/images-$TS.txt.log 2>&1

./process-images.sh >> logs/images-$TS.mysql.log 2>&1

echo END $(/bin/date) 
