#!/bin/bash
# run daily before ../pages/pages.sh
export PPDB=pp2022
TS=$(/bin/date +%Y%m%d)
echo START $(/bin/date) TS $TS
cd $(/usr/bin/dirname $(/usr/bin/realpath $0))
/usr/bin/pwd
echo hiding old files $(/bin/date)
# change back to biweekly updates Aug 10, 2023 12:04 PM
/usr/bin/find www.site3.cc -type f -mtime +3 -! -name '*.v*' -exec /usr/bin/mv "{}" "{}.$TS.v" \;
# /usr/bin/find www.site3.cc -type f -mtime +1 -! -name '*.v' -exec /usr/bin/mv "{}" "{}.$TS.v" \;
# change to daily updates July 7, 2023 12:04 PM
# /usr/bin/find www.site3.cc -type f -! -name '*.v' -exec /usr/bin/mv -v "{}" "{}.$TS.v" \;
echo END $(/bin/date) TS $TS
