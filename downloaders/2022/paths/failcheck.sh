#!/bin/bash
# check if the network was down and send a message to my main computer

cd $(/usr/bin/dirname $(/usr/bin/realpath $0))

LOG=logs/wget-$(/usr/bin/date +%Y%m%d).log
LINES=-100
FAIL=$HOME/Desktop/FAIL.txt

echo checking for network failures in last $LINES lines of $LOG 

/usr/bin/tail $LINES "$LOG" | \
    /usr/bin/grep 'Temporary failure in name resolution.' >/dev/null 2>&1 \
    && (/usr/bin/echo network failure > "$FAIL") \
    || (/usr/bin/rm "$FAIL" >/dev/null 2>&1)

if [ -f "$FAIL" ]
then
    /usr/bin/scp "$FAIL" cal1:/home/pp/Desktop
fi

