#!/bin/bash
# run every 15 min before ../pages/pages.sh
echo START $(/bin/date)
cd $(/usr/bin/dirname $(/usr/bin/realpath $0))
/usr/bin/pwd
TS=$(/bin/date +%Y%m%d)
/usr/bin/wget -x -i personals-paths.txt >> logs/wget-$TS.log 2>&1
/usr/bin/grep -Ph -A1 'name="ad[0-9]' www.site3.cc/*/*/*/index.html | /usr/bin/grep href= | /usr/bin/perl -pe 's#.*?href="(/personals.*)\?source=list".*#https://www.site3.cc$1#' > newlinks.txt
/usr/bin/cat newlinks.txt logs/newlinks-$TS.txt > /tmp/newlinks-all.txt 
/usr/bin/cat /tmp/newlinks-all.txt | /usr/bin/sort | /usr/bin/uniq > logs/newlinks-$TS.txt
./failcheck.sh
echo END $(/bin/date)

