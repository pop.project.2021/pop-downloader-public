#!/bin/bash
(echo "category,N,mean,stddev,1st quartile,median,3rd quartile"; ./boxplots.sh "$@" | perl -pe 's/\w\w* ,/,/g; s/\s*,\s*/,/g') | tee boxplots.csv
