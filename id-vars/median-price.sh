#!/bin/bash
DB="$1"
echo "
select 
    id, 
    if (price is not null, price, if (hourprice is not null, hourprice, null)) p 
from ads a join id2file f on a.fid=f.fid and a.pagesection=f.pagesection 
where 
   if (price is not null, price, if (hourprice is not null, hourprice, null)) is not null 
   and id in (
       select id from ids where minhourprice <> maxhourprice 
           and minhourprice is not null and maxhourprice is not null
   ) 
order by id, p" | ppall --skip-column-names | PPDB=$DB perl -MPPDB -ne '
chomp; 
($id,$p) = split "\t"; 
push @{$f{$id}}, $p; 
END { 
    $dh=PPDB::conn(); 
    $upd=$dh->prepare("update ids set medianhourprice=? where id=?"); 
    foreach $id (sort keys %f) { 
        @s=sort @{$f{$id}}; 
        printf "%s,%f\n", $id, $s[$#s/2]; 
        $upd->execute($s[$#s/2], $id) or die $upd->errstr 
    } 
}' 
echo "update ids set medianhourprice = minhourprice 
      where minhourprice is not null and minhourprice=maxhourprice" | ppall -vvv

