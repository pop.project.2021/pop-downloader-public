#!/bin/bash
stat=freq
for db in pp2007 ppall; do ../../tools/boxplot.py --where="where id not regexp '^cluster '" $db idstats $stat | perl -pe "BEGIN { %m = (ppall=>'2014-2016',pp2007=>'2007-2009'); } s/select .* from.*/dataset , \$m{$db}/"; done | perl -ne 'if (/([^,]*) , (\d+\S*)/) { push @f, $1 unless $f{$1}; push @{$f{$1}}, $2; } END { foreach $k (@f) { print "$k,",(join ",", @{$f{$k}}),"\n" } }' | xclip -i -target text/plain -selection clipboard && echo copied to clipboard
