#!/bin/bash

# make sure to run 
#   cat ids-classify-select.mysql | ppall -vvv
# before running this script

# will show commands
# set -x
fields="(N|median|average|stddev|1st\\s*quartile|3rd\\s*quartile)"
ind="(collective = 0 and namecount <= 1) and"
col="(collective = 1 or namecount > 1) and"
db=$1
if [ "x$db" = "x" ]
then
    db=ppall
fi
shift
stat=$1
if [ "x$stat" = "x" ]
then
    stat=days
fi
shift

echo baseline $(../../tools/boxplot.py $db idstats $stat | grep -P "$fields")
echo collective $(../../tools/boxplot.py --where="where (collective = 1 or namecount > 1)" $db idstats $stat | grep -P "$fields")
echo individual $(../../tools/boxplot.py --where="where collective = 0 and namecount <= 1" $db idstats $stat | grep -P "$fields")
 
# https://www.artificialworlds.net/blog/2012/10/17/bash-associative-array-examples/
# keys are not kept in order
declare -A varmap
varmap["drugs"]="prop420 > 0 or propparty > 0"
varmap["travel"]="propphoneneprov > 0 or propvisiting > 0"
varmap["trans"]="trans = 1"
varmap["female"]="female = 1"
varmap["male"]="male = 1"
varmap["bbw"]="bbw = 1"
varmap["fetish"]="fetish = 1"
varmap["escort"]="escort = 1"
varmap["massage"]="massage = 1"
varmap["tantric"]="propenergywork > 0"
varmap["face-visible"]="propfaces > 0"
varmap["street"]="propstreet > 0"
varmap["ladiesonly"]="propladiesonly > 0"
varmap["french"]="french = 1"
varmap["english"]="english = 1"
varmap["minority"]="notwhite = 1"
varmap["hourlyrate"]="priceclass > 0"

vars=( \
    female male trans bbw english french escort massage fetish tantric \
    travel drugs street minority face-visible hourlyrate \
)

function printbox() {
    var=$1
    query=$2
    echo "$var" $(../../tools/boxplot.py --where="where ($query)" $db idstats $stat | grep -P "$fields")
    echo "$var" individual $(../../tools/boxplot.py --where="where $ind ($query)" $db idstats $stat | grep -P "$fields")
    echo "$var" collective $(../../tools/boxplot.py --where="where $col ($query)" $db idstats $stat | grep -P "$fields")
}

for key in "${vars[@]}"; 
do 
    printbox $key ${varmap[$key]}; 
done

