#!/bin/bash

# make sure to run 
#   cat ids-classify-select.mysql | ppall -vvv
# before running this script

# will show commands
# set -x
fields="(N|median|average|stddev)"
ind="(collective = 0 and namecount <= 1) and"
col="(collective = 1 or namecount > 1) and"
db=$1
if [ "x$db" = "x" ]
then
    db=ppall
fi
shift
stat=$1
if [ "x$stat" = "x" ]
then
    stat=days
fi
shift
echo baseline $(../../tools/boxplot.py $db idstats $stat | grep -P $fields)
echo collective $(../../tools/boxplot.py --where="where (collective = 1 or namecount > 1)" $db idstats $stat | grep -P $fields)
echo individual $(../../tools/boxplot.py --where="where collective = 0 and namecount <= 1" $db idstats $stat | grep -P $fields)
echo drugs $(../../tools/boxplot.py --where="where (prop420 > 0 or propparty > 0)" $db idstats $stat | grep -P $fields)
echo drugs individual $(../../tools/boxplot.py --where="where $ind (prop420 > 0 or propparty > 0)" $db idstats $stat | grep -P $fields)
echo drugs collective $(../../tools/boxplot.py --where="where $col (prop420 > 0 or propparty > 0)" $db idstats $stat | grep -P $fields)
echo travel $(../../tools/boxplot.py --where="where (propphoneneprov > 0 or propvisiting > 0)" $db idstats $stat | grep -P $fields)
echo travel individual $(../../tools/boxplot.py --where="where $ind (propphoneneprov > 0 or propvisiting > 0)" $db idstats $stat | grep -P $fields)
echo travel collective $(../../tools/boxplot.py --where="where $col (propphoneneprov > 0 or propvisiting > 0)" $db idstats $stat | grep -P $fields)
echo trans $(../../tools/boxplot.py --where="where trans = 1" $db idstats $stat | grep -P $fields)
echo trans individual $(../../tools/boxplot.py --where="where $ind trans = 1" $db idstats $stat | grep -P $fields)
echo trans collective $(../../tools/boxplot.py --where="where $col trans = 1" $db idstats $stat | grep -P $fields)
echo female $(../../tools/boxplot.py --where="where female = 1" $db idstats $stat | grep -P $fields)
echo female individual $(../../tools/boxplot.py --where="where $ind female = 1" $db idstats $stat | grep -P $fields)
echo female collective $(../../tools/boxplot.py --where="where $col female = 1" $db idstats $stat | grep -P $fields)
echo male $(../../tools/boxplot.py --where="where male = 1" $db idstats $stat | grep -P $fields)
echo male individual $(../../tools/boxplot.py --where="where $ind male = 1" $db idstats $stat | grep -P $fields)
echo male collective $(../../tools/boxplot.py --where="where $col male = 1" $db idstats $stat | grep -P $fields)
echo bbw $(../../tools/boxplot.py --where="where bbw = 1" $db idstats $stat | grep -P $fields)
echo bbw individual $(../../tools/boxplot.py --where="where $ind bbw = 1" $db idstats $stat | grep -P $fields)
echo bbw collective $(../../tools/boxplot.py --where="where $col bbw = 1" $db idstats $stat | grep -P $fields)
echo fetish $(../../tools/boxplot.py --where="where fetish = 1" $db idstats $stat | grep -P $fields)
echo fetish individual $(../../tools/boxplot.py --where="where $ind fetish = 1" $db idstats $stat | grep -P $fields)
echo fetish collective $(../../tools/boxplot.py --where="where $col fetish = 1" $db idstats $stat | grep -P $fields)
echo escort $(../../tools/boxplot.py --where="where escort = 1" $db idstats $stat | grep -P $fields)
echo escort individual $(../../tools/boxplot.py --where="where $ind escort = 1" $db idstats $stat | grep -P $fields)
echo escort collective $(../../tools/boxplot.py --where="where $col escort = 1" $db idstats $stat | grep -P $fields)
echo massage $(../../tools/boxplot.py --where="where massage = 1" $db idstats $stat | grep -P $fields)
echo massage individual $(../../tools/boxplot.py --where="where $ind massage = 1" $db idstats $stat | grep -P $fields)
echo massage collective $(../../tools/boxplot.py --where="where $col massage = 1" $db idstats $stat | grep -P $fields)
echo tantric $(../../tools/boxplot.py --where="where propenergywork > 0" $db idstats $stat | grep -P $fields)
echo tantric individual $(../../tools/boxplot.py --where="where $ind propenergywork > 0" $db idstats $stat | grep -P $fields)
echo tantric collective $(../../tools/boxplot.py --where="where $col propenergywork > 0" $db idstats $stat | grep -P $fields)

echo show face $(../../tools/boxplot.py --where="where propfaces > 0" $db idstats $stat | grep -P $fields)
echo show face individual $(../../tools/boxplot.py --where="where $ind propfaces > 0" $db idstats $stat | grep -P $fields)
echo show face collective $(../../tools/boxplot.py --where="where $col propfaces > 0" $db idstats $stat | grep -P $fields)

echo street $(../../tools/boxplot.py --where="where propstreet > 0" $db idstats $stat | grep -P $fields)
echo street individual $(../../tools/boxplot.py --where="where $ind propstreet > 0" $db idstats $stat | grep -P $fields)
echo street collective $(../../tools/boxplot.py --where="where $col propstreet > 0" $db idstats $stat | grep -P $fields)

echo ladiesonly $(../../tools/boxplot.py --where="where propladiesonly > 0" $db idstats $stat | grep -P $fields)
echo ladiesonly individual $(../../tools/boxplot.py --where="where $ind propladiesonly > 0" $db idstats $stat | grep -P $fields)
echo ladiesonly collective $(../../tools/boxplot.py --where="where $col propladiesonly > 0" $db idstats $stat | grep -P $fields)


echo french $(../../tools/boxplot.py --where="where french = 1" $db idstats $stat | grep -P $fields)
echo french individual $(../../tools/boxplot.py --where="where $ind french = 1" $db idstats $stat | grep -P $fields)
echo french collective $(../../tools/boxplot.py --where="where $col french = 1" $db idstats $stat | grep -P $fields)

echo english $(../../tools/boxplot.py --where="where english = 1" $db idstats $stat | grep -P $fields)
echo english individual $(../../tools/boxplot.py --where="where $ind english = 1" $db idstats $stat | grep -P $fields)
echo english collective $(../../tools/boxplot.py --where="where $col english = 1" $db idstats $stat | grep -P $fields)

