#!/usr/bin/perl
# get conditional probs for different variables
# be able to do this for different cut off values 
# based on percentiles (min vs 90th percentile?)
use PPDB;
use Getopt::Std;
use strict;

my %opt;
getopts('w:',\%opt);
my $where = $opt{w};
if ($where and $where !~ / and\s*$/) { 
    $where .= " and";
}

my %stat = (
    days => { min => 1, thirdq => 56, p90 => 211 },
    freq => { min => 1, thirdq => 7, p90 => 23 },
    postingrate => { min => 1, thirdq => 3, p90 => 5 },
);

my %fields = (
    baseline => {
        comp => '1 = 1',
        columns => [qw/baseline:/],
    },
    binary => {
        comp => '= 1',
        columns =>  [qw/
            gender:female
            gender:male
            gender:trans
            lang:french
            lang:english
            demo:bbw
            service:escort
            service:fetish
            service:massage
        /],
    },
    proportional => {
        comp => '> 0',
        columns => [qw/
            locale:propstreet
            service:propenergywork
            demo:propfaces
            demo:proprestrictions
            travel:propphoneneprov
            travel:propvisiting
            demo:propparty
            demo:prop420
            demo:propladiesonly
        /],
    }
);

my $stat = shift;
die "need a statistic from ".(join ",", keys %stat) 
    unless defined $stat{$stat};

my $level = shift;
die "need a level from ".(join ",", keys %{$stat{$stat}}) 
    unless defined $stat{$stat}{$level};

my $dh = PPDB::conn();
my ($min,$max) = @{$stat{$stat}}{"min", $level};

my (%rows);
foreach my $ftype (keys %fields) { 
    foreach my $f (@{$fields{$ftype}{columns}}) {
        (my $c=$f) =~ s/.*://;
        $f =~ s/:prop/:/;
        my @row = ($f);
        foreach my $q (&get_queries($c, $stat, $min, $max, $fields{$ftype}{comp}, $where)) {
            push @row, &run_query($dh, $q);
        }
        $rows{$f} = [@row];
    }
}

print "category, $stat is $min, ${stat} from ${min} to ${max}, $stat greater than $max\n";
# for ggplot2 this won't have any effect on the stacked graph
# [1] sort on min, [3] sort on max
foreach my $f (sort { $rows{$b}->[1] <=> $rows{$a}->[1] } keys %rows) {
    print join ", ", @{$rows{$f}};
    print "\n";
}

sub run_query {
    my ($dh, $query) = @_;
    my $get = $dh->prepare($query);
    $get->execute or die $get->errstr;
    my $p;
    while (my $row = $get->fetch) {
        ($p) = @$row;
    }
    $get->finish;
    return $p;
}

sub get_queries {
    my ($f, $stat, $min, $max, $comp, $where) = @_;
    my $denom = "sum(if($f $comp,1,0))";
    my @nums = &get_nums($f, $min, $max, $comp);
    my @queries;
    foreach my $num (@nums) {
        my $query = "select $num/$denom from idstats where $where type <> 'cluster'";
        # my $query = "select $num from idstats";
        push @queries, $query;
    }
    return @queries;
}

sub get_nums {
    my ($f, $min, $max, $comp) = @_;
    return (
        "sum(if(freq<=$min and $f $comp,1,0))",
        "sum(if(freq>$min and freq <$max and $f $comp,1,0))",
        "sum(if(freq>=$max and $f $comp,1,0))",
    );
}

