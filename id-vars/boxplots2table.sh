#!/bin/bash
# must be run in same directory as analyses/id-vars/boxplots.sh 
var=$2
if [ "x$var" = "x" ]
then
    var=days
fi
./boxplots.sh "$@" | (echo "<table><tr><td>category</td><td>N</td><td>average $var</td><td>standard deviation</td><td>1st quartile</td><td>median $var</td><td>3rd quartile</td></tr>" ; perl -pe 's#(.*) N , (\d+) average , (\d\S*) stddev , (\d\S*) median , (\d+)#<tr><td>$1</td><td>$2</td><td>$3</td><td>$4</td><td>$5</td></tr>#; END { print "</table>\n" }') | tee boxplots.html 
cat boxplots.html | xclip -i -target text/html -selection clipboard && echo table saved to clipboard
