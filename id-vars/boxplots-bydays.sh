#!/bin/bash
# in this the differences between long duration 
# advertisers and short duration are measured

# make sure to run 
#   cat ids-classify-select.mysql | ppall -vvv
# before running this script

# will show commands
# set -x
fields="(N|median|average|stddev)"
# these result in ~ 13k and 16k records respectively
mindays=1
maxdays=211
db=$1
if [ "x$db" = "x" ]
then
    db=ppall
fi
shift
dbopts="--skip-column-names"
for var in \
    notwhite prop420 propparty propvisiting propphoneneprov \
    propenergywork propfaces propstreet propladiesonly
do
    count="sum(if($var > 0,1,0))"
    total=$(echo "select $count from idstats where days=$mindays or days>=$maxdays" | \
        $db $dbopts)
    echo $var total $total
    echo $var $mindays day \
        $( echo "select $count/$total from idstats where days=1" | $db $dbopts )
    echo $var $maxdays days or more \
        $( echo "select $count/$total from idstats where days>=$maxdays" | $db $dbopts )
done

# echo trans $(../../tools/boxplot.py --where="where trans = 1" $db idstats $stat | grep -P $fields)
# echo trans 1 day $(../../tools/boxplot.py --where="where $oneday trans = 1" $db idstats $stat | grep -P $fields)
# echo trans 211 days or more $(../../tools/boxplot.py --where="where $manydays trans = 1" $db idstats $stat | grep -P $fields)
# echo female $(../../tools/boxplot.py --where="where female = 1" $db idstats $stat | grep -P $fields)
# echo female 1 day $(../../tools/boxplot.py --where="where $oneday female = 1" $db idstats $stat | grep -P $fields)
# echo female 211 days or more $(../../tools/boxplot.py --where="where $manydays female = 1" $db idstats $stat | grep -P $fields)
# echo male $(../../tools/boxplot.py --where="where male = 1" $db idstats $stat | grep -P $fields)
# echo male 1 day $(../../tools/boxplot.py --where="where $oneday male = 1" $db idstats $stat | grep -P $fields)
# echo male 211 days or more $(../../tools/boxplot.py --where="where $manydays male = 1" $db idstats $stat | grep -P $fields)
# echo bbw $(../../tools/boxplot.py --where="where bbw = 1" $db idstats $stat | grep -P $fields)
# echo bbw 1 day $(../../tools/boxplot.py --where="where $oneday bbw = 1" $db idstats $stat | grep -P $fields)
# echo bbw 211 days or more $(../../tools/boxplot.py --where="where $manydays bbw = 1" $db idstats $stat | grep -P $fields)
# echo fetish $(../../tools/boxplot.py --where="where fetish = 1" $db idstats $stat | grep -P $fields)
# echo fetish 1 day $(../../tools/boxplot.py --where="where $oneday fetish = 1" $db idstats $stat | grep -P $fields)
# echo fetish 211 days or more $(../../tools/boxplot.py --where="where $manydays fetish = 1" $db idstats $stat | grep -P $fields)
# echo escort $(../../tools/boxplot.py --where="where escort = 1" $db idstats $stat | grep -P $fields)
# echo escort 1 day $(../../tools/boxplot.py --where="where $oneday escort = 1" $db idstats $stat | grep -P $fields)
# echo escort 211 days or more $(../../tools/boxplot.py --where="where $manydays escort = 1" $db idstats $stat | grep -P $fields)
# echo massage $(../../tools/boxplot.py --where="where massage = 1" $db idstats $stat | grep -P $fields)
# echo massage 1 day $(../../tools/boxplot.py --where="where $oneday massage = 1" $db idstats $stat | grep -P $fields)
# echo massage 211 days or more $(../../tools/boxplot.py --where="where $manydays massage = 1" $db idstats $stat | grep -P $fields)
# echo tantric $(../../tools/boxplot.py --where="where propenergywork > 0" $db idstats $stat | grep -P $fields)
# echo tantric 1 day $(../../tools/boxplot.py --where="where $oneday propenergywork > 0" $db idstats $stat | grep -P $fields)
# echo tantric 211 days or more $(../../tools/boxplot.py --where="where $manydays propenergywork > 0" $db idstats $stat | grep -P $fields)
# 
# echo show face $(../../tools/boxplot.py --where="where propfaces > 0" $db idstats $stat | grep -P $fields)
# echo show face 1 day $(../../tools/boxplot.py --where="where $oneday propfaces > 0" $db idstats $stat | grep -P $fields)
# echo show face 211 days or more $(../../tools/boxplot.py --where="where $manydays propfaces > 0" $db idstats $stat | grep -P $fields)
# 
# echo street $(../../tools/boxplot.py --where="where propstreet > 0" $db idstats $stat | grep -P $fields)
# echo street 1 day $(../../tools/boxplot.py --where="where $oneday propstreet > 0" $db idstats $stat | grep -P $fields)
# echo street 211 days or more $(../../tools/boxplot.py --where="where $manydays propstreet > 0" $db idstats $stat | grep -P $fields)
# 
# echo ladiesonly $(../../tools/boxplot.py --where="where propladiesonly > 0" $db idstats $stat | grep -P $fields)
# echo ladiesonly 1 day $(../../tools/boxplot.py --where="where $oneday propladiesonly > 0" $db idstats $stat | grep -P $fields)
# echo ladiesonly 211 days or more $(../../tools/boxplot.py --where="where $manydays propladiesonly > 0" $db idstats $stat | grep -P $fields)
# 
# 
# echo french $(../../tools/boxplot.py --where="where french = 1" $db idstats $stat | grep -P $fields)
# echo french 1 day $(../../tools/boxplot.py --where="where $oneday french = 1" $db idstats $stat | grep -P $fields)
# echo french 211 days or more $(../../tools/boxplot.py --where="where $manydays french = 1" $db idstats $stat | grep -P $fields)
# 
# echo english $(../../tools/boxplot.py --where="where english = 1" $db idstats $stat | grep -P $fields)
# echo english 1 day $(../../tools/boxplot.py --where="where $oneday english = 1" $db idstats $stat | grep -P $fields)
# echo english 211 days or more $(../../tools/boxplot.py --where="where $manydays english = 1" $db idstats $stat | grep -P $fields)
# 
