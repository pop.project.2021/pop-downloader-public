Tallying restrictions for workers

This provides a script (get_restrictions.pl) that can detect restrictions
for workers. The script uses a module NoWords.pm which provides a 
pattern listing over 2000 common terms following the work "No".

Two variables are calculated by get_restrictions.pl:

- the average number of detected restrictions per ad per contact
- the number of ads mentioning at least one restriction per contact


