#!/usr/bin/perl
# using the NoWords module pattern count the number of times
# restrictions show up in posts
use PPDB;
use NoWords;
use Getopt::Std;
use strict;
my (%opt);
getopts("d",\%opt);
my $DEBUG = $opt{d} || 0;

print "getting restrictions and keywords\n";
my $dh = PPDB::conn();
my $get = $dh->prepare(
    "select id, a.fid, a.pagesection, group_concat(distinct words) 
    from id2file f join ads a on a.fid=f.fid and a.pagesection=f.pagesection 
    where a.keep > 0
    group by id, a.fid, a.pagesection"
    .($DEBUG ? " order by rand() limit 10": " order by id, a.fid, a.pagesection")
);
my $updads = $dh->prepare("update ads set restrictions=? where fid=? and pagesection=?");
my $stopwords = qr/^(_N*_|only|text|and|for|the|to|in|or|of|i|a)$/;
my $stopwordsfr = qr/^(me|je|pour|une|call|you|d|avec|suis|)$/;
$get->execute or die $get->errstr;
my (%ids, %words, %seen, %vocab);
while (my $row = $get->fetch) {
    my ($id, $fid, $pagesection, $words, $post) = @$row;
    print "looking at $id fid $fid.$pagesection words $words\n" if $DEBUG;
    my $restrictions = 0;
    while ($words =~ m/($NoWords::pat)/g) { 
        print "$id,$fid,$1\n" if $DEBUG;
        $restrictions++;
    }
    $ids{$id}{restrictions} += $restrictions;
    $ids{$id}{ads_w_restrictions}++ if $restrictions > 0;
    $ids{$id}{N}++;
    unless (defined $seen{$fid}{$pagesection}) {
        print "$fid.$pagesection restrictions $restrictions\n" if $DEBUG;
        $updads->execute($restrictions, $fid, $pagesection) or die $updads->errstr;
    }
    $seen{$fid}{$pagesection} = 1;
    my @words = split ' ', $words;
    foreach my $w (sort @words) {
        next unless $w =~ /[a-z]/;
        next if $w =~ /$stopwords/;
        next if $w =~ /$stopwordsfr/;
        $w =~ s/(\d+)/sprintf "_%s_", 'N' x length($1)/eg;
        $words{$id}{$w}++;
        $vocab{$w} = 1;
    }
}
$get->finish;

print "getting idfs\n";
my $getidf = $dh->prepare(
    "select word, idf from words where word regexp '[a-z]'"
);
$getidf->execute or die $getidf->errstr;
my (%idfs, $maxidf);
while (my $row = $getidf->fetch) {
    my ($word, $idf) = @$row;
    $idfs{$word} = $idf;
    $maxidf = $idf if $idf > $maxidf; 
}
$getidf->finish;
foreach my $word (keys %vocab) {
    unless (defined $idfs{$word}) {
        print "missing idf for $word - adding smoothed value\n";
        $idfs{$word} = $maxidf;
    }
}

print "updating ids\n";
my $updids = $dh->prepare(
    "update ids set keywords=?, avgkeywordidf=?, maxkeywordidf=?, ".
    "ads_w_restrictions=?, avgrestrictions=? where id=?");
foreach my $id (sort keys %ids) {
    my $avgr = $ids{$id}{restrictions}/$ids{$id}{N};
    my ($maxidf, $avgidf, $sumidf, @keywords, $wc);
    foreach my $w (
        sort { $words{$id}{$b}*$idfs{$b} <=> $words{$id}{$a}*$idfs{$a} } 
            keys %{$words{$id}}
    ) {
        $wc++;
        $sumidf += $idfs{$w};
        $maxidf = $idfs{$w} if $idfs{$w} > $maxidf;
        next if $wc >= 20;
        push @keywords, $w;
    }
    my $keywords = substr((join(",", @keywords)),0,512);
    $avgidf = $sumidf/$wc if $wc > 0;
    printf "$id restrictions %d, ads %d, avg restrictions %.4f, ".
        "ads with restrictions %d, avgidf %.4f maxidf %.4f keywords %s\n", 
        $ids{$id}{restrictions}, $ids{$id}{N}, $avgr, $ids{$id}{ads_w_restrictions}, 
        $avgidf, $maxidf, $keywords;
    $updids->execute($keywords, $avgidf, $maxidf, $ids{$id}{ads_w_restrictions}, $avgr, $id) 
        or die $updids->errstr unless $DEBUG;
}

$dh->disconnect;

