#!/usr/bin/perl
# generate table of collective vs individual advertisers and names
use PPDB;
use strict;
my $dh = PPDB::conn();
my @conditionkeys = ("collective","individual","individual no names","collective no names");
my %conditions = (
    "collective" => "(collective = 1 or namecount > 1)",
    "individual" => "(collective is null and (namecount is null or namecount = 1))",
    "individual no names" => "(collective is null and namecount is null)",
    "collective no names" => "(collective = 1 and namecount is null)",
);

my $getmedian = $dh->prepare("select namecount from ids where include = 1 and namecount > 1 order by namecount");
my @ncs;
$getmedian->execute or die $getmedian->errstr;
while (my $row = $getmedian->fetch) { 
    my ($namecount) = @$row;
    push @ncs, $namecount;
}
$getmedian->finish;

die "no collective median!" if scalar(@ncs) == 0;
my %medians = ("individual" => 1, "collective" => @ncs[$#ncs/2]);

my (%tb, %totals, %types);
foreach my $k (@conditionkeys) {
    die "missing condition $k" unless defined $conditions{$k};
    my $where = $conditions{$k};

    my $median = 1;
    if ($k =~ /collective/) {
        $median = $medians{collective};
    }

    my $query = "select type, count(*) c, sum(namecount) nc, sum(if(namecount is null, $median, namecount)) estnc ".
            "from ids where include= 1 and $where group by type";
    my $get = $dh->prepare($query);
    $get->execute or die $get->errstr;
    while (my $row = $get->fetch) {
        my ($type, $ids, $namecount, $estnamecount) = @$row;
        $types{$type}++;
        $totals{$k}{ids} += $ids;
        $totals{$k}{namecount} += $namecount;
        $totals{$k}{estnamecount} += $estnamecount;
        $tb{$k}{$type} = "Advertisers $ids<br>\nEst. names $estnamecount<br>\n".($namecount ? "Name count $namecount":"");
    }
    $get->finish;
}

my $html = "<table>\n<tr>\n<td></td>\n";
foreach my $k (@conditionkeys) {
    $html .= "<td>$k</td>\n";
}
$html .= "</tr>\n";
foreach my $t (reverse sort keys %types) {
    $html .= "<tr>\n<td>$t</td>\n";
    foreach my $k (@conditionkeys) { 
        $html .= "<td>\b$tb{$k}{$t}</td>\n";
    }
    $html .= "</tr>\n";
}

$html .= "<tr>\n<td>totals</td>\n";
foreach my $k (@conditionkeys) {
    $html .= "<td>\nAdvertisers $totals{$k}{ids}<br>\nEst. names $totals{$k}{estnamecount}<br>\n"
        .($totals{$k}{namecount} ? "Name count $totals{$k}{namecount}":"")."</td>\n";
}
$html .= "</table>\n";

print $html;

