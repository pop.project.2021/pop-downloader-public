#!/bin/bash
PREFIX="$1"
TS="$2"
grep '{' "${PREFIX}"bothyears-"${TS}".txt | python table.py | xclip -i -target text/html -selection clipboard && \
echo "population table now copied to the clip board" || echo "error generating population table"
