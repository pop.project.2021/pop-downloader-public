#!/usr/bin/env python
"""
uses multipop to get a complete set of estimates 
for a time period, creates plots as required
can print html for tables summarizing the time period
"""
# the default plots look somewhat uglier than the R plots
# how to make a trend line
# https://stackoverflow.com/questions/26447191/how-to-add-trendline-in-python-matplotlib-dot-scatter-graphs

import multipop, pop
from matplotlib import pyplot as plt
import matplotlib.colors as mcolors
import pandas as pd
import numpy
import sys

colors = list(mcolors.TABLEAU_COLORS)
idkeys = {'rawids':"raw contacts",'ids':'scaled effective advertisers','clusters':'clusters'}
configs = {
    "Monthly effective advertisers": 
        {"labels":idkeys.keys(), "keys":idkeys, "filename":"advertisers.csv",},
    "Monthly scaled population counts by gender": 
        {"labels":['f','t','m','u'], 
            "keys":pop.GENDERS,
            "filename":"pop.csv",},
    "Monthly scaled population counts: cis male, transgendered, unknown": 
        {"labels":['t','m','u'], "keys":pop.GENDERS,"filename":"others.csv",},
}

def genall(ests, verbose=False):
    """
    goes through each type of plot and generates the plot image
    as these are kind of crappy looking, this will also save the csv 
    data from the data frames for each image
    by default will show the plots, set verbose = False to turn this off
    """
    pltcount = 0
    linecount = 0
    for title, config in configs.items():
        df = pd.DataFrame(
                ests.periodslice("scaled counts", config['labels'], keys=config['keys']))
        df.to_csv(ests.db + config["filename"], index=False)
        for label in config['labels']:
            key = config["keys"][label]
            linecount += 1
            color = colors[linecount % len(colors)]
            plt.plot(df["period"],df[key], label=config["keys"][label], color=color)
            # make trend line
            x = range(len(df["period"]))
            y = df[key]
            z = numpy.polyfit(x, y, 1)
            p = numpy.poly1d(z)
            # not sure how useful these equations are ...
            label = "y=%.6fx+(%.6f)"%(z[0],z[1])
            plt.plot(df["period"],p(x),color=color,linewidth=0.7,linestyle='dashed')
        # this seems to be causing the x axis to get clipped TODO: fix
        plt.xticks(rotation=60,fontsize="small",horizontalalignment="right")
        plt.title(title)
        plt.legend()
        # using a different image name so as not to overwrite the R images
        plt.savefig(ests.db + config['filename'] + ".plt.png")
        if verbose:
            plt.show()
        plt.clf()

if __name__ == "__main__":
    ests = multipop.Estimates(*sys.argv[1:4])
    # verbose=True will show each graph made in succession
    genall(ests, verbose=False)
    # to paste into google docs, pipe the output to xclip 
    # ./plotpop.py | xclip -i -target text/html -selection clipboard
    print(ests.htmlsummary())

