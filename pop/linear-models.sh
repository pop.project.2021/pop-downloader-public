#!/bin/bash
# run the following before running this script
# ./pposf.sh 
# ./multipop.py ppall_osf 2014-11-01 2017-01-01 > multipop-both-ppall_osf-2014-2016-CI0.95.json
jq '.monthly.periods[] | ."raw counts".ids' multipop-both-ppall_osf-2014-2016-CI0.95.json | perl -pe 'BEGIN { print "mon,count\n" } s/^/$.,/' > monthly-raw-ids.csv 
jq '.monthly.periods[] | ."scaled counts".ids' multipop-both-ppall_osf-2014-2016-CI0.95.json | perl -pe 'BEGIN { print "mon,count\n" } s/^/$.,/' > monthly-corrected-ids.csv 
jq '.monthly.periods[] | ."scaled counts".all' multipop-both-ppall_osf-2014-2016-CI0.95.json | perl -pe 'BEGIN { print "mon,count\n" } s/^/$.,/' > monthly-corrected-all.csv 
jq '.monthly.periods[] | ."scaled counts".f' multipop-both-ppall_osf-2014-2016-CI0.95.json | perl -pe 'BEGIN { print "mon,count\n" } s/^/$.,/' > monthly-corrected-f.csv 
jq '.monthly.periods[] | ."scaled counts".t' multipop-both-ppall_osf-2014-2016-CI0.95.json | perl -pe 'BEGIN { print "mon,count\n" } s/^/$.,/' > monthly-corrected-t.csv 
jq '.monthly.periods[] | ."scaled counts".m' multipop-both-ppall_osf-2014-2016-CI0.95.json | perl -pe 'BEGIN { print "mon,count\n" } s/^/$.,/' > monthly-corrected-m.csv 
jq '.monthly.periods[] | ."raw counts".clusters' multipop-both-ppall_osf-2014-2016-CI0.95.json | perl -pe 'BEGIN { print "mon,count\n" } s/^/$.,/' > monthly-corrected-clusters.csv
# get some basic stats to report
echo "basic statistics"
./extractmulti.py multipop-both-ppall_osf-2014-2016-CI0.95.json
echo
# generate linear models for monthly changes in statistics
echo "linear models count ~ month"
Rscript linear-models.R
