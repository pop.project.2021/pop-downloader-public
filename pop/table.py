#!/usr/bin/env python
"""
take output from pop.py and make a formatted table
"""
import json
from pop import GENDERS
import sys

counts = json.load(sys.stdin)
rows = []
topheadings = []

html = "<table>\n";
# make header row
html += "<tr>\n<td>gender</td>\n";
for corrected in counts.keys():
    topheadings.append(corrected)
    html += f"<td>{corrected}</td>\n";
html += "</tr>"

# reorganize 
for key,gender in GENDERS.items():
    row = []
    row.append(gender)
    row += [counts[h][key] for h in topheadings]
    rows.append(row)

for row in rows:
    html += "<tr>\n"
    for item in row:
        html += f"<td>{item}</td>\n"
    html += "</tr>\n"

html += "</table>"

print(html)
