#!/usr/bin/env python
"""
build up a list of clusters of related ids based on the DBSCAN algorithm
"""

def dbscan(neighbours):
    """
    Uses a dict of ids that co-occur to build clusters.
    This dict should be bi-directional.
    This dict should represent bidirectional neighbour relationships between ids.
    Normally this would be ids that co-occur in ads.
    Returns labels, a dict mapping id to cluster(s).
    """
    anchors = neighbours.keys()
    labels = {}

    cluster = 0
    for anchor in anchors:
        if anchor in labels:
            continue
        cluster += 1
        S = [anchor]
        cursor = neighbours[anchor]
        for neighbour in cursor:
            if neighbour not in labels:
                S.append(neighbour)
        
        while len(S) > 0:
            neighbour = S.pop()
            if neighbour in labels:
                continue
            labels[neighbour] = cluster
            cursor = neighbours[neighbour]
            for newneighbour in cursor:
                if newneighbour not in labels:
                    S.append(newneighbour)

    return labels

