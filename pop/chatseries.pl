#!/usr/bin/perl -n 
# reads through a data file with chatid, name, day, count, ids 
# and optionally phashes to collect stats on when names/ids 
# change
# if the image data is available tries to use this to determine
# if a new name shares images with an old name
chomp;
use Time::Local qw/timelocal/;
use Statistics::Basic qw/:all/;
my $month = 31; my $week = 7;
my $threshold = 1; $week;
my $daysecs = 86400;
my (undef, $chatid, $name, $date, $ads, $ids, $images) = split / \| /, $_;
my @ymd = ($date=~/(\d\d\d\d)-(\d\d)-(\d\d)/); 
next if $ymd[0] < 2014;
$ymd[1]--; 
my $day = int(timelocal(undef,undef,undef,(reverse @ymd))/$daysecs);
# data is ordered by date by chatid
$days{$chatid}{min} = $day unless defined $days{$chatid}{min};
$days{$chatid}{max} = $day;
# when getting rates we can compare days between or total number of days
# currently using total number of days - uncomment sections below for days between
$namedays{$chatid}{$name}{min} = $day unless defined $namedays{$chatid}{$name}{min};
$namedays{$chatid}{$name}{max} = $day;

# optional
unless (not defined $images or $images eq 'NULL') { 
    @images = split ",", $images;
}

# most likely there is only one id unless there are multiple posts in a day
unless (not defined $ids or $ids eq 'NULL') {
    @ids = split ",", $ids;
}

print "new chatid $chatid\n" and $mayhavechanged = $prevname = undef 
    unless $chatid == $prevchatid;
$prevchatid = $chatid;
my $datediff = (defined $prevname ? $day - $prevday : 0);

print "$chatid: new name $name on $date\n" unless $seen{$chatid}{$name};
$seen{$chatid}{$name}++;
print "$chatid: name changed to $name on $date\n" unless $name eq $prevname;
$mayhavechanged = 1 if $datediff > $threshold and $name ne $prevname;

foreach my $id (@ids) {
    print "$chatid: new id $id for $name on $date\n" 
        unless $ids{$chatid}{$name}{$id};
    $ids{$chatid}{$name}{$id} = 1;
    $idnames{$chatid}{$id}{$name} = 1;
    push @{$changedid{$chatid}{$name}}, $datediff if $previd{$chatid}{$name} ne $id;
    $previd{$chatid}{$name} = $id;
    my $ns;
    foreach my $n (keys %{$idnames{$chatid}{$id}}) {
        next if $n eq $name;
        $ns .= " $n";
    }
    print "$chatid: id $id also used by$ns on $date\n" 
    # and ($mayhavechanged > 0 ? $mayhavechanged++: undef)
        if $ns and $name ne $prevname and $day - $prevday > $threshold;
}

foreach my $image (@images) {
    print "$chatid: new image $image for $name on $date\n" 
        unless $images{$chatid}{$name}{$image};
    $images{$chatid}{$name}{$image} = 1;
    $imagenames{$chatid}{$image}{$name} = 1;
    my $ns;
    foreach my $n (keys %{$imagenames{$chatid}{$image}}) { 
        next if $n eq $name;
        $ns .= " $n";
    }
    print "$chatid: image $image also used by$ns on $date\n" 
        and ($mayhavechanged > 0 ? $mayhavechanged++: undef)
        if $ns and $name ne $prevname and $datediff > $threshold;
}

push @{$namechanged{$chatid}}, $datediff 
    and print "$chatid: added name change after $datediff days on $date\n" 
    and $mayhavechanged = 0
        if $mayhavechanged > 1;

$prevname = $name;
$prevday = $day;

END { 
    # how often ids change over time
    my @idfreqs;
    my $sumdays = 0;
    my %daycounts; 
    @daycounts{keys %days} = 
        map { $el = $days{$_}{max} - $days{$_}{min} + 1; $sumdays += $el; $el } keys %days;
    my $chatidcount = scalar(keys %days);
    @days = sort { $a <=> $b } values %daycounts;
    my $mediandays = $days[$#days/2];
    my $avgdays = $sumdays/$chatidcount;
    printf "average days for one chatid %.4f median %d for %d chatids\n", 
        $avgdays, $mediandays, $chatidcount; 
    my $minage = 48; # sprintf("%.0f", $avgdays); 
    print "minimum age in days for inclusion of chat id: $minage\n";
    my $includedcount = 0;
    foreach my $chatid (sort {$a <=> $b} keys %days) {
        # next if $daycounts{$chatid} < $mediandays or $daycounts{$chatid} > $avgdays;
        next if $daycounts{$chatid} < $minage;
        $includedcount++;
        if (not defined $changedid{$chatid}) {
            push @idfreqs, 0;
            next;
        }
        foreach my $name (keys %{$changedid{$chatid}}) {
            my $n = $sum = 0;
            # uncomment to only count days between changes
            # foreach my $days (@{$changedid{$chatid}{$name}}) {
            #    $sum += $days;
            #    $n++;
            # }
            # $n-- if $n > 0;

            # not using scalar here as we only count when an id changes
            $n = $#{$changedid{$chatid}{$name}};
            $sum = $namedays{$chatid}{$name}{max} - $namedays{$chatid}{$name}{min} + 1;
            # print "$chatid: idfreq sum $sum n $n\n";
            push @idfreqs, $n/$sum;
        }
    }
    @idfreqs = sort { $a <=> $b } @idfreqs;
    my $idrates = 0; 
    map { $idrates += $_ } @idfreqs;
    printf "median id change rate %.4f %.4f average id change rate %.4f stddev %.4f for %d chatids\n",
        $idfreqs[$#idfreqs/2], median(\@idfreqs), $idrates/$includedcount, stddev(\@idfreqs), $includedcount;

    # rates of name change
    my @freqs;
    $includedcount = 0;
    foreach my $chatid (sort { $a <=> $b } keys %days) { 
        # next if $daycounts{$chatid} < $mediandays or $daycounts{$chatid} > $avgdays;
        next if $daycounts{$chatid} < $minage;
        $includedcount++;
        if (not defined $namechanged{$chatid}) {
            push @freqs, 0;
            next;
        }
        my $n = $sum = 0;
        # uncomment to only count days between changes
        # foreach my $days (@{$namechanged{$chatid}}) { 
        #     $sum += $days;
        #     $n++;
        # }
        $n = scalar(@{$namechanged{$chatid}});
        $sum = $days{$chatid}{max} - $days{$chatid}{min} + 1;
        my $freq = ($sum == 0 ? 0: $n/$sum);
        # printf "$chatid: avg days %.4f rate %.4f over %d days\n", $sum/$n, $freq, $daycounts{$chatid};
        push @freqs, $freq;
    }
    my @freqs = sort { $a <=> $b } @freqs;
    my $ratesum = 0;
    map { $ratesum += $_ } @freqs;
    printf "name changes: median rate %.4f %.4f average rate %.4f %.4f sd %.4f, %d chatids\n", 
        $freqs[$#freqs/2], median(\@freqs), $ratesum/$includedcount, mean(\@freqs), stddev(\@freqs), $includedcount;
    printf "we would expect %.4f (from median) %.4f (from average) name changes over 2 years\n", 
        (2*365)*$freqs[$#freqs/2], (2*365)*$ratesum/$includedcount;
    # use Data::Dumper;
    # print Dumper(\@freqs);
}
