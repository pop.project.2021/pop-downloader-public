#!/bin/bash
START="$1"
END="$2"
TS="$3"
PREFIX="$4"
DB="$5"
VOLFIELDS="$6"
CI="0.95"

echo running volume.pl
cd ../ad-volume
PPDB=${DB} ./volume.pl -t ${START} ${END} > "${PREFIX}"volume.csv
cd ../pop

# all of the above should be replaced by:
# however, the graphics look bad ... :(
echo starting plotpop.py
./plotpop.py "$DB" "$START" "$END" "$CI" "$PREFIX" | xclip -i -target text/html -selection clipboard && \
   echo table saved to clipboard

echo creating images
rm "${PREFIX}"volume.csv
cut -d, -f1,${VOLFIELDS} ../ad-volume/"${PREFIX}"volume.csv > "${PREFIX}"volume.csv
Rscript pop-errorbars.R "${PREFIX}"volume.csv "Ads per month ${START} to ${END}"

# Rscript pop-area.R "${PREFIX}"pop.csv "Scaled population counts ${START} to ${END}"
# Rscript pop-area.R "${PREFIX}"others.csv "Scaled cis-male, transgender and unknown "${START}" to "${END}""
# Rscript pop.R "${PREFIX}"advertisers.csv "Contact counts "${START}" to "${END}""
Rscript pop-errorbars.R "${PREFIX}"pop.csv "Scaled population counts ${START} to ${END}"
Rscript pop-errorbars.R "${PREFIX}"others.csv "Scaled cis-male, transgender and unknown "${START}" to "${END}""
Rscript pop-errorbars.R "${PREFIX}"advertisers.csv "Contact counts "${START}" to "${END}""

echo combining images
convert "${PREFIX}"volume.csv.png -pointsize 60 -fill black -draw 'text 80,120 "a)"' "${PREFIX}"volume-a.png
convert "${PREFIX}"advertisers.csv.png -pointsize 60 -fill black -draw 'text 80,120 "b)"' "${PREFIX}"advertisers-b.png
convert "${PREFIX}"pop.csv.png -pointsize 60 -fill black -draw 'text 80,120 "c)"' "${PREFIX}"pop-c.png
convert "${PREFIX}"others.csv.png -pointsize 60 -fill black -draw 'text 80,120 "d)"' "${PREFIX}"others-d.png
convert "${PREFIX}"volume-a.png "${PREFIX}"advertisers-b.png +append "${PREFIX}"ads.png 
convert "${PREFIX}"pop-c.png "${PREFIX}"others-d.png +append "${PREFIX}"people.png
convert "${PREFIX}"ads.png "${PREFIX}"people.png -append "${PREFIX}"all.png

# view resulting image ...
# eog "${PREFIX}"all.png

