This documents attempts to find a "true" lower bound 
population count.

After some testing what seems to be working is to count
unique names gathered from running the dl/phones/ins-id-names.pl
script on every id. Ids with no names are assumed to have 
one name associated with them - this is the median value
for ids where names can be found.

To prevent overcounting, the clusters identified using
id-clusters/related.py were inserted into the ids table
and ins-id-names.pl was run on them. A new table clustercounts
was created which contains:

- ids and name counts not represented in a cluster
- ids and name counts from the clusters

Only ids/clusters that contain relevant posts were included.
Relevant can be either ads in a relevant section (see filecats)
or craigslist ads which had a isadpred score > 0.

Note that, especially for clusters, an id can represent women,
men and transgendered women and men. To tease out the likely
number of women for comparison to the Canadian population,
the proportion of names relating to women was calculated.

This calculation used gender proportions approximated by the 
ratio of posts in each section. However, this should be calculated 
directly from the number of names per post instead.

To get a comparison of the full 2 year count vs spot counts 
a script should be written that can recreate this process for 
different time domains. (See pop.py)

The numbers from this script should be reduced to reflect 
the number of identities a given worker may have taken on.
At the moment it looks like the median value for this is two
(based on names) or around 1.39 (based on ids) - something that
surprised me.

The phashes directory covers code that looks at how many 
"identities" an advertiser may have had - the goal there
was to produce a factor to reduce the resulting count
relating to the number of new identities an advertiser may
have taken on.

Some constants are required to correct for overcounting. Some
of these are just direct counts of valid name-contact pairs
or relevant advertisers. However, the probability that a name
is in fact a new name for an existing worker is needed as
well as the rate at which advertisers change contact info.
These are calculated via the changes.sh script which runs
two other scripts idchanged.pl and namechanged.pl. The input
data for these scripts will be provided as part of the osf
project for the paper.

