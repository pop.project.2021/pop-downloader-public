#!/usr/bin/env python
"""
Runs the pop estimator for the periods between a longer 
time period and amalgamates the results
"""
import pop
import sys, calendar, json, statistics
from datetime import datetime, timedelta
from collections import OrderedDict

def startenddates(startstr, endstr):
    # Sample start and end dates
    start = datetime.strptime(startstr, "%Y-%m-%d")
    end = datetime.strptime(endstr, "%Y-%m-%d")
    return start, end

def genweeks(startstr, endstr):
    start, end = startenddates(startstr, endstr)
    weeks = []
    for d in range(0, (end - start).days, 7):
        weekstart = (start + timedelta(d)).strftime("%Y-%m-%d")
        weekend = (start + timedelta(d+6)).strftime("%Y-%m-%d")
        # weeks.append((weekstart, weekend))
        yield (weekstart, weekend)

def genmonths(startstr, endstr):
    """
    get a list of month starts and ends from two dates
    returns tuple of month start and end
    see https://www.matthealy.com.au/blog/post/find-months-between-two-dates-with-python/
    """
    start, end = startenddates(startstr, endstr)

    # Get list of months >= start and < end
    months = OrderedDict()
    for d in range((end - start).days):
        day = start + timedelta(d)
        if day.strftime("%Y-%m") in months:
            continue
        dow, monthendday = calendar.monthrange(day.year, day.month)
        monthstart = day.strftime("%Y-%m-01")
        monthend = day.strftime("%Y-%m-{0}".format(monthendday))
        months[day.strftime("%Y-%m")] = (monthstart, monthend)
        yield monthstart, monthend

class Estimates(object):
    def __init__(self, db, start, end, conflevel=0.95, dategen=genmonths):
        self.db = db
        self.start = start
        self.end = end
        self.dategen = dategen
        self.estimates = self.get_estimates(db, start, end, conflevel)

    def get_estimates(self, db, start, end, conflevel):
        """
        gets a list of estimates for a number of date intervals between start
        and end
        """
        estimates = {}
        counts = {}
        estimates["all"] = pop.estimate(db, start, end, conflevel)
        estimates["periods"] = {}
        for month in self.dategen(start, end):
            estimate = pop.estimate(db, *month)
            if estimate is None:
                continue
            for restype, tallies in estimate.items():
                if restype not in counts:
                    counts[restype] = {}
                for gender, count in tallies.items():
                    if isinstance(count, str):
                        continue
                    if gender not in counts[restype]:
                        counts[restype][gender] = []
                    counts[restype][gender].append(count)
            estimates["periods"][month[0]] = estimate

        estimates["averages"] = {}
        for restype in counts.keys():
            if restype not in estimates["averages"]:
                estimates["averages"][restype] = {}
            for gender in counts[restype].keys():
                estimates["averages"][restype][gender] = \
                        (statistics.mean(counts[restype][gender]),
                         statistics.stdev(counts[restype][gender]))

        return estimates

    def periodslice(self, restype, gender=None, keys=pop.GENDERS, showdesc = True):
        """
        returns a dict of items by gender for a given result type
        this should be formatted to work with pandas.DataFrame
        """

        genders = []
        if gender is None:
            for g in keys.keys():
                genders.append(g)
        elif isinstance(gender, str):
            if gender not in keys:
                raise(Exception(f"periodslice: gender {gender} missing from keys"))
            genders.append(gender)
        else:
            for g in gender:
                genders.append(g)

        slic = {}
        slic["period"] = []
        
        for period, results in self.estimates["periods"].items():
            slic["period"].append(period)
            for gender in genders:
                if showdesc == True:
                    key = keys[gender]
                else:
                    key = gender
                if key not in slic:
                    slic[key] = []
                if gender not in results[restype]:
                    slic[key].append(0)
                else:
                    slic[key].append(results[restype][gender])
        return slic

    def htmlsummary(self):
        """
        return html text summary of the all and average keys in the estimates
        """
        keys = ["raw counts","scaled counts"]
        html = "<table>\n"
        for key in keys:
            html += "<tr>\n"
            html += f"<td colspan=3>{key}</td>\n"
            html += "</tr>\n"
            html += "<tr><td>gender</td>\n"
            html += "<td>monthly average</td><td>entire period</td>\n"
            html += "</tr>\n"
            # subset of pop.GENDERS 
            genders = ["f","m","t","u","all","ids"]
            for gender in genders:
                desc = pop.GENDERS[gender]
                html += "<tr>\n"
                html += f"<td>{desc}</td>"
                if gender not in self.estimates['averages'][key]:
                    html += f"<td>0</td>"
                    html += f"<td></td>"
                else:
                    html += f"<td>{round(self.estimates['averages'][key][gender][0])}"
                    html += f" (SD {round(self.estimates['averages'][key][gender][1])})</td>"

                if gender not in self.estimates['all'][key]:
                    html += f"<td>0</td>"
                else:
                    html += f"<td><nobr>{round(self.estimates['all'][key][gender])}"
                    if key == "scaled counts":
                        html += " (CI "
                        html += f"{round(self.estimates['all']['scaled counts lower'][gender])}"
                        html += "-"
                        html += f"{round(self.estimates['all']['scaled counts upper'][gender])}"
                        html += ")"
                    html += f"</nobr></td>"
                html += "</tr>\n"
        html += "</table>\n"
        return html

    def __repr__(self):
        return json.dumps(self.estimates, indent=4)


if __name__ == '__main__':
    ests = {}
    ests["monthly"] = Estimates(*pop.get_args()).estimates
    ests["weekly"] = Estimates(*pop.get_args(), dategen=genweeks).estimates
    ests["all"] = ests["monthly"]["all"]
    print(json.dumps(ests, indent=4, sort_keys=True))

