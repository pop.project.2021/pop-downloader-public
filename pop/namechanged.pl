#!/usr/bin/perl
# redo the analysis script to use aggregates by default
# input file is assumed to be in chatid/date order 
# only one name should be in the name column
# assumption is that one image belongs to one name
# images are phashes
use strict;
use Getopt::Std;
my %opt;
getopts("D", \%opt);
my $DEBUG = $opt{"D"} || 0;
my $infile = shift;
open IN, $infile or die "can't open input file $infile!";
my (%lines, %imagenames, %names, $newnames);
while (my $l = <IN>) {
    chomp $l;
    # | chatid | name | date | N | id(s) | image(s) |
    # source data from pop/ids-vs-names.mysql
    my (undef,$chatid,$name,$date,$N,$ids,$images) = split /\s*\|\s*/, $l;
    # make the input sortable
    $lines{$date} = {chatid=>$chatid,name=>$name,date=>$date,N=>$N,ids=>$ids,images=>$images};
}
close IN;
foreach my $l (sort keys %lines) {
    my ($chatid,$name,$date,$N,$ids,$images) = @{$lines{$l}}{qw/chatid name date N ids images/};
    print "$chatid, $name, $date, $N, $ids, $images\n" if $DEBUG;
    $names{$name."-".$chatid}++;
    # find out what names are associated with images
    my $isnew = 0;
    foreach my $img (split ",", $images) {
        # check to see if the images are associated with othernames
        if (!$isnew and defined $imagenames{$img}{$chatid}) {
            if (!defined $imagenames{$img}{$chatid}{$name}) {
                print "found new name $name for $img\n" if $DEBUG;
                $newnames++;
                $isnew = 1;
            }
        }
        $imagenames{$img}{$chatid}{$name}++;
    }
}
printf "new names %d, all names %d, p(n new) %.4f\n", $newnames, scalar(keys %names), 1-$newnames/scalar(keys %names);

