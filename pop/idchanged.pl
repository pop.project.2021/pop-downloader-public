#!/usr/bin/perl
# redo the analysis script to use aggregates by default
# input file is assumed to be in chatid/date order 
# only one name should be in the name column
# assumption is that one image belongs to one name
# images are phashes
# this script will only work where chatid is an actual site3 chatid
use strict;
use Getopt::Std;
my %opt;
getopts("D", \%opt);
my $DEBUG = $opt{"D"} || 0;
my $infile = shift;
open IN, $infile or die "can't open input file $infile!";
my (%lines, %imageids, %names, $newids, %ids, %dates);
while (my $l = <IN>) {
    chomp $l;
    # | chatid | name | date | N | id(s) | image(s) |
    # data from pop/chatnames-vs-ids-vs-names.mysql
    my (undef,$chatid,$name,$date,$N,$ids,$images) = split /\s*\|\s*/, $l;
    # make the input sortable
    $lines{$date} = {chatid=>$chatid,name=>$name,date=>$date,N=>$N,ids=>$ids,images=>$images};
}
close IN;
foreach my $l (sort keys %lines) {
    my ($chatid,$name,$date,$N,$ids,$images) = @{$lines{$l}}{qw/chatid name date N ids images/};
    $dates{$date}++;
    print "$chatid, $name, $date, $N, $ids, $images\n" if $DEBUG;
    # find out what names are associated with images
    foreach my $id (split ",", $ids) {
        $ids{$id}++;
        my $isnew = 0;
        foreach my $img (split ",", $images) {
            # check to see if the images are associated with othernames
            if (!$isnew and defined $imageids{$img}{$name}) {
                if (!defined $imageids{$img}{$name}{$id}) {
                    print "found new id $id for $img\n" if $DEBUG;
                    $newids++;
                    $isnew = 1;
                }
            }
            $imageids{$img}{$name}{$id}++;
        }
    }
}
printf "new ids %d, all ids %d, dates %d, rate %.4f\n", $newids, scalar(keys %ids), scalar(keys %dates), $newids/scalar(keys %dates);

