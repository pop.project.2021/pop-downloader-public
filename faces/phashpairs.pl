#!/usr/bin/perl
# find examples of people changing ids from regular data
# this uses image data as a starting point
# as there are a huge number of examples limit the 
# initial search to people's face images
use POSIX;
use PPDB;
use strict;
my $table = shift || "phashpairs_faces";
my $DEBUG = shift || 0;
my $dh = PPDB::conn();
# this table is all phashes with faces >= 0.925
# each is matched with a non-overlapping set of ids
# with the min and max dates for each as well as 
# the type and include values

# is ordering important here?
my $get = $dh->prepare("select distinct phash, id1,id2,min1,max1,min2,max2 from $table ".
    "where include1=1 and include2=1 and type1 <> 'cluster' and type2 <> 'cluster' and id2 is not null ".
    "order by phash, min1, min2");

my (%include,%ranges,%exclude,%byid);
$get->execute or die $get->errstr;
while (my $row = $get->fetch) {
    my ($phash,$id1,$id2,$min1,$max1,$min2,$max2) = @$row;
    print join ",", @$row, "\n" if $DEBUG;
    print "$phash excluded\n" and next if $exclude{$phash};
    $ranges{$phash}{$id1} = {min=>$min1, max=>$max1} unless defined $ranges{$phash}{$id1};
    $ranges{$phash}{$id2} = {min=>$min2, max=>$max2} unless defined $ranges{$phash}{$id2};
    unless (defined $include{$phash}) {
        print "first time $phash seen\n" if $DEBUG;
        $byid{$id1}{$phash} = 1;
        $byid{$id2}{$phash} = 1;
        push @{$include{$phash}{list}}, $id1; 
        push @{$include{$phash}{list}}, $id2;
        $include{$phash}{seen}{$id1} = 1;
        $include{$phash}{seen}{$id2} = 1;
        next;
    }
    my $error = 0;
    foreach my $id ($id1, $id2) {
        foreach my $testi (@{$include{$phash}{list}}) {
            next if $testi eq $id; 
            # we are expecting phashes to not have overlapping ranges
            # if this is the case then the phash is likely being shared
            if ($ranges{$phash}{$id} lt $ranges{$phash}{$testi}{max}) {
                print "excluding $phash\n" if $DEBUG;
                $include{$phash}{list} = [];
                $exclude{$phash} = 1;
                $error = 1;
                last;
            }
        }
        last if $error;
        $byid{$id}{$phash} = 1 and push @{$include{$phash}{list}}, $id 
            unless $error or $byid{$id}{$phash};
    }
}
$get->finish;

if ($DEBUG) {
    use Data::Dumper;
    $Data::Dumper::Sortkeys = 1;
    $Data::Dumper::Indent = 1;
    print "exclusions\n";
    print Dumper(\%exclude);
    print "inclusions\n";
    print Dumper(\%include);
}

my ($min, $max, $N, $totalids, @idcounts, %hist, %cluster);
foreach my $phash (sort keys %include) {
    next if scalar(@{$include{$phash}{list}}) == 0;
    $N++;
    my $idcount = scalar(@{$include{$phash}{list}});
    print "starting with image $phash\n";
    while (my $id = shift @{$include{$phash}{list}}) {
        printf "    %s from %s to %s\n", $id, @{$ranges{$phash}{$id}}{qw/min max/};
        $cluster{$N}{$id}{$phash} = 1;
        # if we find other images associated with this id, add all of the other ids in
        my @otherphashes = keys %{$byid{$id}};
        if (scalar @otherphashes > 1) { 
            print "    getting more...\n";
            foreach my $oph (@otherphashes) { 
                next if $oph eq $phash;
                $cluster{$N}{$id}{$oph} = 1;
                foreach my $oid (@{$include{$oph}{list}}) {
                    unless ($byid{$oid}{$phash}) {
                        print "    adding $oid from $oph\n";
                        $cluster{$N}{$oid}{$phash} = 1;
                        $cluster{$N}{$oid}{$oph} = 1;
                        $idcount++;
                        push @{$include{$phash}{list}}, $oid; 
                        $ranges{$phash}{$oid} = 
                            {min=> $ranges{$oph}{$oid}{min}, max => $ranges{$oph}{$oid}{max}};
                        $byid{$oid}{$phash} = 1;
                    }
                }
                $include{$oph}{list} = [];
            }
        }
    }
    $totalids += $idcount;
    push @idcounts, $idcount;
    $max = $idcount if $idcount > $max;
    $hist{POSIX::ceil($idcount/10)}++;
    $min = $idcount if not defined $min or $min > $idcount;
    print "---- $idcount ids\n";
}
use Data::Dumper;
$Data::Dumper::Indent = 1;
$Data::Dumper::Sortkeys = 1;
print Dumper(\%cluster);
my @sorted = sort @idcounts;
printf "exclusions=%d, inclusions: N=%d, idcount: min=%d median=%d max=%d average=%.4f\n", 
    scalar(keys %exclude), $N, $min, $sorted[$#sorted/2], $max, $totalids/$N;
print "idcount       # \n";
foreach my $idcount (sort { $a <=> $b } keys %hist) {
    printf "%7d %7d %s\n", $idcount*10, $hist{$idcount}, ">" x POSIX::ceil($hist{$idcount}/10);
}

