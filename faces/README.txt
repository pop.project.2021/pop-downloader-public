Face detection

See hasfaces.txt for more details on how the code
was used.

Initially all images were processed through the 
facetest.py script. The JSON format output of this 
script was used as input to savefacetest.py
which saved the highest confidence rating from mtcnn
to the images table in the database.

A sample of images was visually checked using the 
linked display-facetest.py which would display the 
image with the proposed faces from mtcnn. 

The logged output from display-facetest.py was used
to find the optimal confidence level with optimize.py.
This confidence level was later used to indicate 
which advertisers used images with recognizable faces.

The category.py script is like display-facetest.py 
but does not run mtcnn on the images. 

