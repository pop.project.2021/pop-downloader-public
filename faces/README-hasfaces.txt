Visual check of a representative sample of images

A sample of 4461 images was checked visually using a version of facetest.py
For a population of 1037956 where we assume the percent valid is 25% a sample size gives a confidence interval
of 1.71 for a confidence level of 99% and 1.3 for a confidence level of 95%.

Note that some of this includes irrelevant images. Ignoring irrelevant/fake images the sample decreased to 3950.
To increase the sample checked additional images were added. The number of relevant images checked was 4130. 

Output from optimize.py which determines what cutoff gives the best f score and accuracy
best {'f': 0.926, 'accuracy': 0.9500000000000001} max f 0.843680709534368 max accuracy 0.9329297820823245

This means that we can say that for a given image where MTCNN gave a score of 0.926 or more
we can be 99% confident that we got 93.29% +/- 1.6 (91.69% to 94.89%)
of the images classified correctly. This assumes that images with partially 
visible faces are the equivalent of images where there is no visible face at all. 
i.e. we are assuming that the image taker concealed their face in both cases. 


Number of images with MTCNN confidence scores:

RAW images (contains duplicates):

MariaDB [pp_pp2]> select sum(if(faces is not null, 1, 0)) facetest, count(*), sum(if(faces is not null,1,0))/count(*) from images;
+----------+----------+-----------------------------------------+
| facetest | count(*) | sum(if(faces is not null,1,0))/count(*) |
+----------+----------+-----------------------------------------+
|  3109731 |  3721713 |                                  0.8356 |
+----------+----------+-----------------------------------------+
1 row in set (1.135 sec)

UNIQUE images:

MariaDB [pp_pp2]> select sum(if(faces is not null, 1, 0)) facetest, count(*), sum(if(faces is not null,1,0))/count(*) from (select distinct faces, sha1 from images) a;
+----------+----------+-----------------------------------------+
| facetest | count(*) | sum(if(faces is not null,1,0))/count(*) |
+----------+----------+-----------------------------------------+
|   973240 |  1037956 |                                  0.9377 |
+----------+----------+-----------------------------------------+
1 row in set (30.023 sec)


Total number of unique images checked:

MariaDB [pp_pp2]> select min(faces),avg(faces),max(faces), count(*) from (select distinct sha1,faces,hasfaces from images where hasfaces is not null) a;
+------------+--------------------+------------+----------+
| min(faces) | avg(faces)         | max(faces) | count(*) |
+------------+--------------------+------------+----------+
|          0 | 0.2682216891497407 |          1 |     4461 |
+------------+--------------------+------------+----------+
1 row in set (0.125 sec)

