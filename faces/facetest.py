#!/usr/bin/env python
"""
from machinelearningmastery.com
https://machinelearningmastery.com/how-to-perform-face-detection-with-classical-and-deep-learning-methods-in-python-with-keras/
"""
from matplotlib import pyplot
from matplotlib.patches import Rectangle
from matplotlib.widgets import Button
from mtcnn.mtcnn import MTCNN
import sys, os, logging, re
from pathlib import Path

log = sys.argv[1]
Path(log).touch()
seen = {}
with open(log, 'r') as lh:
    for l in lh:
        l = l.strip()
        m = re.match(r'.*(images/\S+)', l)
        if m is not None:
            seen[m.group(1)] = True

logging.basicConfig(filename=log, level=logging.INFO, format='%(asctime)s %(message)s')

class Index:
    def __init__(self, filename):
        self.hasfaces = None
        self.filename = filename

    def nofaces(self, event):
        self.hasfaces = 'no'
        logging.info("{0} has faces? no".format(self.filename))

    def partfaces(self, event):
        self.hasfaces = 'partial'
        logging.info("{0} has faces? partial".format(self.filename))

    def faces(self, event):
        self.hasfaces = 'YES'
        logging.info("{0} has faces? YES".format(self.filename))

    def close(self, event):
        if self.hasfaces is not None:
            pyplot.close()
        else:
            logging.warning("please click a button")

# draw an image with detected objects
# uncomment the draw_image_with_boxes line below to use this function
def draw_image_with_boxes(filename, result_list):
    # load the image
    data = pyplot.imread(filename)
    # plot the image
    pyplot.imshow(data)
    # get the context for drawing boxes
    ax = pyplot.gca()
    # plot each box
    for result in result_list:
        # get coordinates
        x, y, width, height = result['box']
        # create the shape
        rect = Rectangle((x, y), width, height, fill=False, color='red')
        # draw the box
        ax.add_patch(rect)

    callback = Index(filename)
    axfaces = pyplot.axes([0.4, 0.03, 0.12, 0.075])
    axpartfaces = pyplot.axes([0.55, 0.03, 0.12, 0.075])
    axnofaces = pyplot.axes([0.7, 0.03, 0.12, 0.075])
    axclose = pyplot.axes([0.85, 0.03, 0.1, 0.075])
    bpartfaces = Button(axpartfaces, 'Part Face')
    bpartfaces.on_clicked(callback.partfaces)
    bnofaces = Button(axnofaces, 'No Faces')
    bnofaces.on_clicked(callback.nofaces)
    bfaces = Button(axfaces, 'Has Faces')
    bfaces.on_clicked(callback.faces)
    bclose = Button(axclose, 'Next')
    bclose.on_clicked(callback.close)

    # show the plot
    pyplot.show()

# create the detector, using default weights
detector = MTCNN()

for listname in sys.argv[2:]:
    with open(listname,"r") as imgh:
        for line in imgh:
            filename = line.strip()
            if not os.path.isfile(filename):
                logging.warning("skipping {0}".format(filename))
                continue
            if filename in seen:
                logging.warning("already saw {0}".format(filename))
                continue
            try:
                # load image from file
                pixels = pyplot.imread(filename)
                # detect faces in the image
                faces = detector.detect_faces(pixels)
                # display faces on the original image
                if re.match(r'.*display.*',sys.argv[0]):
                    draw_image_with_boxes(filename, faces)
                else: 
                    logging.info("{0} {1}".format(filename,faces))
            except Exception as e:
                logging.error("error {0}: {1}".format(filename,e))

