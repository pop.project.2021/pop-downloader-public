#!/usr/bin/env python
"""
extract confidence ratios from the facetest*.json file and save the largest to the images table
"""
import json, sys, os
from mysql.connector import connect, Error

if len(sys.argv) != 3:
    print("syntax:",sys.argv[0],"{db name} {json file}")
    sys.exit(1)

db = sys.argv[1]
if not os.path.isfile(sys.argv[2]):
    print("json file is not a file")
    sys.exit(1)

try:
    with open(sys.argv[2], "r") as jh:
        sha1s = json.load(jh)
        print(len(sha1s),"images")
        try:
            with connect(
                        host="localhost", 
                        database=db, 
                        user=os.environ['PPDBUSER'], 
                        password=os.environ['PPDBPW']
                    ) as conn:
                for sha1, faces in sha1s.items():
                    confidence = 0.0
                    print(sha1,"has",len(faces),"face(s):",faces)
                    for face in faces:
                        if confidence < face['confidence']:
                            confidence = face['confidence']
                    try:
                        with conn.cursor() as cursor:
                            query = "update images set faces = {0} where sha1 = '{1}'".format(confidence, sha1)
                            print(query)
                            cursor.execute(query)
                            conn.commit()
                    except Error as qe:
                        print("query failed", qe)
        except Error as ce:
            print("connection error",ce)
except Exception as e:
    print("file error",e)

