#!/usr/bin/env python
"""
find optimal confidence cutoff for images that may have faces 
see hasfaces.txt for more information
using the F score of 2/(1/precision + 1/recall) to rate different 
confidence cutoffs in the faces field for a sample of 4260 images visually checked 
"""
import sys, os
from mysql.connector import connect, Error

if len(sys.argv) != 2:
    print("syntax:",sys.argv[0],"{db name} ")
    sys.exit(1)

db = sys.argv[1]

def precision(tp, fn):
    return tp/(tp+fn)

def recall(tp, fp):
    return tp/(tp+fp)

def accuracy(tp, tn, fp, fn):
    return (tp+tn)/(tp+tn+fp+fn)

def f_score(rec, pre):
    return (2.0*pre*rec)/(pre+rec)
try:
    with connect(
                host="localhost", 
                database=db, 
                user=os.environ['PPDBUSER'], 
                password=os.environ['PPDBPW']
            ) as conn:
        cutoff = 0.9
        max_f_score = 0.0
        max_accuracy = 0.0
        best_cutoff = {'f': 0.0, 'accuracy': 0.0}
        while cutoff <= 0.99:
            try:
                with conn.cursor() as cursor:
                    # hasfaces can have values of null, no, YES and partial
                    # this case we are treating no + partial as the negative class
                    negative_query = """select hasfaces, count(*) freq from 
                        (select distinct sha1,faces,hasfaces from images where faces < {0} and hasfaces is not null and relevantfaces='YES') a 
                        group by hasfaces""".format(cutoff)
                    cursor.execute(negative_query)
                    negatives = {}
                    for n in cursor:
                        hasfaces, freq = n
                        negatives[hasfaces] = freq

                    positive_query = """select hasfaces, count(*) freq from 
                        (select distinct sha1,faces,hasfaces from images where faces >= {0} and hasfaces is not null and relevantfaces='YES') a 
                        group by hasfaces""".format(cutoff)
                    cursor.execute(positive_query)
                    positives = {}
                    for p in cursor:
                        hasfaces, freq = p
                        positives[hasfaces] = freq
                    # true positives
                    tp = positives['YES']
                    # true negatives
                    tn = negatives['no'] + negatives['partial']
                    # false positives
                    fp = negatives['YES']
                    # false negatives
                    fn = positives['no'] + positives['partial']

                    pre = precision(tp, fn)
                    rec = recall(tp, fp)
                    f = f_score(rec, pre)
                    acc = accuracy(tp, tn, fp, fn)

                    if acc > max_accuracy:
                        best_cutoff['accuracy'] = cutoff
                        max_accuracy = acc
                    if f > max_f_score:
                        best_cutoff['f'] = cutoff
                        max_f_score = f

                    print("cutoff",cutoff,"total",(tp+tn+fp+fn),"tp",tp,"tn",tn,"fp",fp,"fn",fn,"precision",pre,"recall",rec,"accuracy",acc,"f score",f,"best",best_cutoff)
                    cutoff += 0.001

                print("best", best_cutoff, "max f", max_f_score, "max accuracy", max_accuracy)

            except Error as qe:
                print("query failed", qe)
except Error as ce:
    print("connection error",ce)

