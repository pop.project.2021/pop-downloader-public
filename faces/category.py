#!/usr/bin/env python
"""
Visually categorize images based on whether they are actual workers (relevant), other stuff (other) or likely recycled images (fake)
"""
from matplotlib import pyplot
from matplotlib.patches import Rectangle
from matplotlib.widgets import Button
import sys, os, logging, re
from pathlib import Path

log = sys.argv[1]
Path(log).touch()
seen = {}
with open(log, 'r') as lh:
    for l in lh:
        l = l.strip()
        m = re.match(r'.*(images/\S+)', l)
        if m is not None:
            seen[m.group(1)] = True

logging.basicConfig(filename=log, level=logging.INFO, format='%(asctime)s %(message)s')

class Index:
    def __init__(self, filename):
        self.relevant = None
        self.filename = filename

    def isrelevant(self, event):
        self.relevant = 'YES'
        logging.info("{0} relevant? YES".format(self.filename))

    def fake(self, event):
        self.relevant = 'fake'
        logging.info("{0} relevant? fake".format(self.filename))

    def other(self, event):
        self.relevant = 'other'
        logging.info("{0} relevant? other".format(self.filename))

    def close(self, event):
        if self.relevant is not None:
            pyplot.close()
        else:
            logging.warning("please click a button")

def draw_image(filename):
    # load the image
    data = pyplot.imread(filename)
    # plot the image
    pyplot.imshow(data)

    callback = Index(filename)
    axother = pyplot.axes([0.4, 0.03, 0.12, 0.075])
    axfake = pyplot.axes([0.55, 0.03, 0.12, 0.075])
    axrelevant = pyplot.axes([0.7, 0.03, 0.12, 0.075])
    axclose = pyplot.axes([0.85, 0.03, 0.1, 0.075])
    brelevant = Button(axrelevant, 'Relevant')
    brelevant.on_clicked(callback.isrelevant)
    bother = Button(axother, 'Other')
    bother.on_clicked(callback.other)
    bfake = Button(axfake, 'Fake')
    bfake.on_clicked(callback.fake)
    bclose = Button(axclose, 'Next')
    bclose.on_clicked(callback.close)

    # show the plot
    pyplot.show()

for listname in sys.argv[2:]:
    with open(listname,"r") as imgh:
        for line in imgh:
            filename = line.strip()
            if not os.path.isfile(filename):
                logging.warning("skipping {0}".format(filename))
                continue
            if filename in seen:
                logging.warning("already saw {0}".format(filename))
                continue
            try:
                draw_image(filename)
            except Exception as e:
                logging.error("error {0}: {1}".format(filename,e))

