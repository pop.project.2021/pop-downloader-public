#!/usr/bin/perl
# insert names associated with ads into the id2file table
# note that getting names is a very error prone process
use PPDB;
use PPNames;
use strict;
my $dh = PPDB::conn();
my $updads = $dh->prepare("update ads set names=?, namecount=? where fid=? and pagesection=?");

my $fid = shift;
my $pagesection = shift || 0;
my $getposts;
if (defined $fid) {
    $getposts = $dh->prepare("select fid,pagesection,words from ads where fid=? and pagesection=?");
    $getposts->execute($fid, $pagesection) or die $getposts->errstr;
} else {
    $getposts = $dh->prepare("select fid,pagesection,words from ads");
    $getposts->execute or die $getposts->errstr;
}
while (my $row = $getposts->fetch) {
    my ($fid,$pagesection,$words) = @$row;
    print "search post $fid.$pagesection for names\n";
    my %names;
    my $namecount;
    my @names = PPNames::search($words);
    my $nc = scalar @names;
    if ($nc > 0) {
        foreach my $n (@names) {
            $names{$n}++;
        }
    }
    my $names = join ",", (sort keys %names);
    my $namecount = scalar (keys %names);
    if ($namecount > 0) {
        print "updating $fid.$pagesection with $namecount names: $names\n";
        $updads->execute($names, $namecount, $fid, $pagesection) or die $updads->errstr;
    } else {
        print "no names found, clearing\n";
        $updads->execute(undef, undef, $fid, $pagesection) or die $updads->errstr;
    }
}
$getposts->finish;
