List of names used for counting individual workers

Detecting these names was a multistep process where an initial list of names (Chen 
2018) was compared against trigrams containing those name strings. Name strings that 
appeared to be mostly used as names in the data sets were selected. These seed names 
were used to identify words that were commonly associated with names (for example 
“name is …” or “je m’appelle …”). Trigrams containing these context words were then 
used to identify a larger set of names actually used in ads. 

The file is a list of all strings used to detect names in the 2015-2016 data set.
This list was largely used "as-is" with the other data sets.

See also the ins-id-names.pl and ins-id2file-names.pl scripts
and the PPNames.pm module for how these were used.

PPDB.pm, which provides basic database connectivity, can be found in one of 
the {2016,2021}/phones directories.

