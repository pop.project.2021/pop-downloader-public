#!/usr/bin/perl
# insert names associated with ids into the ids table
# note that this is a very error prone process
use PPDB;
use PPNames;
use strict;
my $dh = PPDB::conn();
my $getposts = $dh->prepare(
        "select distinct words from ads a join id2file f on a.fid=f.fid and a.pagesection=f.pagesection ".
        "where id=? and keep > 0");
my $upd = $dh->prepare("update ids set names=?, namecount=? where id=?");
my $get = $dh->prepare("select id from ids where ignored = 0 order by id");
# used to fill in the new "cluster" id type
# my $get = $dh->prepare("select id from ids where ignored = 0 and namecount is null and type='cluster' order by id");
$get->execute or die $get->errstr;
while (my $row = $get->fetch) {
    my ($id) = @$row;
    print "search $id posts for names\n";
    $getposts->execute($id) or die $getposts->errstr;
    if ($getposts->rows) {
        my %names;
        my $namecount;
        while (my $postrow = $getposts->fetch) {
            my ($words) = @$postrow;
            my @names = PPNames::search($words);
            my $nc = scalar @names;
            if ($nc > 0) {
                foreach my $n (@names) {
                    $names{$n}++;
                }
            }
        }
        $getposts->finish;
        my $names = join ",", (sort keys %names);
        my $namecount = scalar (keys %names);
        if ($namecount > 0) {
            print "updating $id with $namecount names: $names\n";
            $upd->execute($names, $namecount, $id) or die $upd->errstr;
        } else {
            print "no names found, clearing\n";
            $upd->execute(undef, undef, $id) or die $upd->errstr;
        }
    }
}
$get->finish;
