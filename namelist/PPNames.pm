package PPNames;
# handle identifying names in posts
use PPDB;
use Data::Dumper;
use strict;
our %pats;
our $patcache = 'PPNames-pats.pl';

# either gets or makes patstr and pat for name matching for a given language
sub init {
    my ($force) = @_;
$force = 1;
    if (!$force and -f $patcache) {
        eval {
            do $patcache;
        }
    }
    unless (scalar %pats) {
        @pats{qw/patstr pat/} = &regex;
        if (open CACHE, "> $patcache") {
            print CACHE Data::Dumper->Dump([\%pats],[qw/*pats/]);
            close CACHE;
        }
    }
}

# find all the names in the db and make a list
sub list {
    my $dh = PPDB::conn();
    my $get = $dh->prepare("select names,namecount from ids where names is not null");
    $get->execute or die $get->errstr;
    my (%allnames);
    my $ncount = my $allnames2 = my $allnames = 0;
    while (my $row = $get->fetch) {
        my ($names, $namecount) = @$row;
        my @names = split ",", $names;
        @allnames{@names} = map { $allnames{$_}+1 } @names;
        $allnames += $namecount;
        $allnames2 += $namecount**2;
        $ncount++;
    }
    $get->finish;
    foreach my $name (sort { $allnames{$a} <=> $allnames{$b} } %allnames) {
        print "$name $allnames{$name}\n";
    }
    my $av = $allnames/$ncount;
    my $sd = sqrt(($allnames2 + ($allnames**2)/$ncount)/($ncount-1));
    printf "average number of names %.2f standard deviation %.2f total found=%d total records=%d",
        $av, $sd, $allnames, $ncount;
}
 
# find names in text
sub search {
    my ($text) = @_;
    &init unless scalar %pats;
    my $pat = $pats{pat};
    die "error creating name pattern!" unless defined $pat;
    $text = lc($text);
    my %names;
    while ($text =~ /(?:\b|[^a-zA-Z])($pat)(?:\b|[^a-zA-Z])/g) {
        my $name = $1;
        next unless $name =~ /\w/;
        # removes duplicate characters
        $name =~ s/(.)\1+/$1/g;
        $names{$name}++;
    }
    my @names = keys %names;
    @names;
}

# remove repeated characters
sub flatten {
    my ($name) = @_;
    return $name if length($name) <= 3;
    return if $name =~ /^ann+y$/;
    return if $name =~ /^envy+$/;
    # convert number patterns
    $name =~ s/_N+_/\\d+/g;
    # have to use \1 rather than $1 here as per 
    # http://stackoverflow.com/questions/178837/how-can-i-find-repeated-letters-with-a-perl-regex
    $name =~ s/(.)(\1)+/$1\+/g;
    # assume multiple last characters as this is quite common
    $name .= '+' unless $name =~ /\+$/;
    $name;
}

# make a regex for finding names 
# this should be saved and reused as the operation is fairly costly
sub regex {
    my $dh = PPDB::conn();
    my $get = $dh->prepare(
        "select distinct names.name ".
        "from names join words on (names.name=words.word) ".
        "where isname = 1"
    );
    $get->execute or die $get->errstr;
    my %names;
    while (my $row = $get->fetch) {
        my ($name) = @$row;
        # this seems to be giving us name like "male" which match too many things
        # $name = &flatten($name);
        $names{$name}++;
    }
    $get->finish;
    my @pat;
    foreach my $name (sort { length($b) <=> length($a) || $a cmp $b } keys %names) {
        push @pat, $name;
    }
    my $patstr = join "|\n\t", @pat;
    my $pat = qr/$patstr/x;
    return ($patstr, $pat) if wantarray;
    $pat;
}

1;
