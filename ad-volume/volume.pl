#!/usr/bin/perl
# calculate number of "live" ads and number of new ads for a given time period
use PPDB;
use Getopt::Std;
use MonthList;
use strict;

my %opt;
getopts("t", \%opt);
my $showtitle = $opt{t} || 0;

my $start = shift;
my $finish = shift;
die "need start and finish dates!" 
    unless $start =~ /^\d\d\d\d-\d\d-\d\d$/ 
        and $finish =~ /^\d\d\d\d-\d\d-\d\d$/;

my $dh = PPDB::conn();

my (%counts, %sections);
$sections{total} = {new=>1,all=>1};

foreach my $dates (@{MonthList::generate($start, $finish)}) {
    my ($mstart, $mfinish) = @$dates;
    my $getall = $dh->prepare(
        "select section, count(*) from files 
        where (section = 'site 1' or cat in (select cat from filecats where relevant > 0))
        and (
            ('$mstart' <= lastdate) and ('$mfinish' >= firstdate)
        ) and keep > 0
        group by section");
    my $getnew = $dh->prepare(
        "select section, count(*) from files 
        where (section = 'site 1' or cat in (select cat from filecats where relevant > 0))
        and firstdate between ? and ? 
        and keep > 0
        group by section");

    $getall->execute() or die $getall->errstr;
    while (my $row = $getall->fetch) {
        my ($section, $ads) = @$row;
        $sections{$section} = 1;
        $counts{$mstart}{$section}{all} = $ads;
        $counts{$mstart}{total}{all} += $ads;
    }
    $getall->finish;
    $getnew->execute($mstart, $mfinish) or die $getnew->errstr;
    while (my $row = $getnew->fetch) {
        my ($section, $ads) = @$row;
        $counts{$mstart}{$section}{new} = $ads;
        $counts{$mstart}{total}{new} += $ads;
    }
    $getnew->finish;
}
if ($showtitle) {
    my @line = ("period");
    foreach my $section (sort keys %sections) {
        push @line, "${section} visible ads";
        push @line, "${section} new ads";
    }
    print (join ",", @line);
    print "\n";
}
foreach my $month (sort keys %counts) {
    my @line;
    push @line, $month;
    foreach my $section (sort keys %sections) {
        my $all = $counts{$month}{$section}{all} || 0;
        my $new = $counts{$month}{$section}{new} || 0;
        push @line, $all;
        push @line, $new;
    }
    print (join ",", @line);
    print "\n";
}

