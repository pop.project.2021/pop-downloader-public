package MonthList;
# from https://stackoverflow.com/questions/26245629/how-do-i-get-a-hash-of-start-end-dates-between-a-range-with-perl-where-the-start
use strict;
use warnings;

use Time::Piece;
use Time::Seconds;

sub generate {
    my @pairs;
    my ($start,$end) = @_;

    my $fmt   = '%Y-%m-%d';

    # Normalized to Noon to avoid DST
    my $month_start = Time::Piece->strptime( $start, $fmt ) + 12 * ONE_HOUR;
    my $period_end  = Time::Piece->strptime( $end,   $fmt );

    while (1) {
        my $startstr =  $month_start->strftime($fmt);

        my $month_end = $month_start + ONE_DAY * ( $month_start->month_last_day - $month_start->mday );

        # End of Cycle if current End of Month is greater than or equal to End Date
        if ( $month_end > $period_end ) {
            last;
        }

        # Print End of Month and begin cycle for next month
        my $endstr = $month_end->strftime($fmt);
        push @pairs, [$startstr, $endstr];

        $month_start = $month_end + ONE_DAY;
    }
    return \@pairs;
}

1;

