#!/usr/bin/env python
"""
simple takes a file as input and runs the perceptual hash algorithm
on the image path then prints out an update command.
assumes: iid,path
path should be valid where the script is run"""
import imagehash
from PIL import Image
import sys, re, os

with open(sys.argv[1]) as listh:
    for line in listh:
        g = re.match(r'([^,]+),(.*)', line.strip())
        if g is not None:
            where = g.group(1)
            path = g.group(2)
            if not os.path.isfile(path):
                print("-- cannot find ",path)
                continue
            try:
                phash = imagehash.phash(Image.open(path))
                print(f"update images set phash='{phash}' {where};")
            except Exception as e:
                print(f"-- error creating phash for {path}: {e}")

