Perceptual hash generation for images

The motivation for running perceptual 
hash (phash) is to make it possible to find
images that have slight transformations that
are basically the same image. This hashing
method is used by sites such as tineye.com 
to categorize images on the web. 
The phash is used to find where images are 
being reused by different advertisers.

See phash.py for more details.
A directory of images named using
the sha1 and size is used as a starting
point (to reduce the number of redundant 
images processed).

The simple-phash.py script does not attempt
to do multithreading but essentially does 
the same work as phash.py. This was found to be more
reliable in practice. Parallelization was
done externally by taking the input list of
files and splitting it with the linux "split"
command.

When either script is run the input is a 
list of file paths. The script opens each 
in turn and calculates the perceptual hash
and then generates a mysql command. These logged
commands are later run against the database to 
update the images table. 

