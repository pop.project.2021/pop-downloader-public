Validation of phashes

Over 1000 phashes with multiple images were
visually inspected. No phash showed false positives
despite the fact that many images were different sizes
or had small modifications (e.g. the face had been 
obscured). 

