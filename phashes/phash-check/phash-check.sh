#!/bin/bash
# quick script for human inspection of phashes
# to see if a phash actually represents the same image

# run in pp/2015/images

# generate a list of image stubs from phashes that represent multiple images
# these are limited to images from the 2015 data set
echo "select phash, group_concat(distinct concat(substring(sha1,1,2),'/',sha1,'-',lpad(size,7,'0')) limit 3) from images where iid < 20000018 group by phash having count(distinct sha1) > 1 order by rand(888) limit 2000" | ppall | tee phash-check.txt

# get the actual image paths
cat phash-check.txt | \
    perl -pe 's#(\w+?)\t(.*)#echo "\# $1"; ls $2#; s/,/\* /g; s#$#* 2>/dev/null#;' | \
    while read -r c; 
    do 
        bash -c "$c"; 
    done | tee valid-images.txt

# review each image and log whether the phash is valid or not
# this runs eog on the subset of images associated with the phash
perl -ne '
BEGIN { 
    open LOG, ">> phash-check.log" or die $! 
} 
chomp; 
if (/^# (.*)/) { 
    if (scalar(@buf) > 1) { 
        system qq{eog @buf}; 
        print "valid? [Yn] "; 
        $valid = <STDIN>; 
        print LOG "$1 is $valid" 
    } 
    @buf=(); 
    print "phash $1\n"; 
} else { 
    push @buf, $_ 
}' valid-images.txt

