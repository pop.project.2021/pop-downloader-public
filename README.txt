Source files for the "The Silent Majority" paper.

This repository contains code and data used for
processing online ads as described in the paper.

Directories:

- downloaders/{2016,2021} - example downloader code used for different phases of the project
- namelist - code and data for detecting names in ads
- restrictions - code for detecting restrictions in ads
- phashes - perceptual hash calculation code for images
- faces - face detection code for images
- pop - population estimation tools, also contains csv data with counts
- id-vars - builds ids table variables for analysis
- id-clusters - builds clusters for whole data sets (pop does this for smaller time periods)

General toolchain process for population counts is:
cd pop; ./pposf.sh

